import os
import sys
import ntpath
from shutil import copyfile
from subprocess import call
from tools import *

def experiment_compression(filename, working_folder):
    #rlz_budgets = [128, 256, 512]
    #rlz_budgets = [4, 8, 16, 32, 64, 128, 256, 512]
    rlz_budgets = [1, 2, 3]
    mem_range = [10000, 4000]
    D_range = [99]
    threads_range = [1]
    time_limit = -99 ## No timeout.
    
    brotli_levels = [5, 8, 11]

    lzma_levels = [9, 5, 2]
    
    for mem in mem_range:
        for threads in threads_range:
            for D in D_range:
                runReLZ(filename, working_folder, mem, threads, D, time_limit)
                runReLZ(filename, working_folder, mem, threads, D, time_limit)
                runReLZ(filename, working_folder, mem, threads, D, time_limit)
    
    for rlzstore_reflen in rlz_budgets:
        for threads in threads_range:
            runRLZSTORE(filename, working_folder, rlzstore_reflen, threads, time_limit)

    for level in brotli_levels:
        runBROTLI(filename, working_folder, level, time_limit)
    
    for level in lzma_levels:
        runLZMA(filename, working_folder, level, time_limit)
    
    ## SUMMARTY:::
    summary_file = working_folder + "/compression_results.txt"
    if os.path.isfile(summary_file):
        os.remove(summary_file)
    for mem in mem_range:
        for D in D_range:
            for threads in threads_range:
                curr_folder = working_folder+"/ReLZ_MEM" + str(mem) + "_THREADS_" + str(threads) + "_D_" + str(D) + "_files"
                logfile_name = curr_folder + "/" + "log.txt"
                curr_compression_time = getCompressionTimeFromLog(logfile_name)
                curr_decompression_time = getDecompressionTimeFromLog(logfile_name)
                curr_ratio = getRATIOfromLog(logfile_name)
                #curr_name = "ReLZ-M" + str(mem) + "-D" + str(D) + "-T" + str(threads)
                curr_name = "ReLZ_{" + str(mem/1000) + "GB}"
                curr_line = curr_name + "\n" + str(curr_ratio) + " " + str(curr_compression_time) + " " + str(curr_decompression_time) +"\n"
                with open(summary_file, "a") as myfile:
                    myfile.write(curr_line)
                    myfile.write("\n\n")
    
    for level in brotli_levels:
        curr_folder = working_folder+"/BROTLI_LEVEL" + str(level)
        logfile_name = curr_folder + "/" + "log.txt"
        
        curr_compression_time = getCompressionTimeFromLog(logfile_name)
        curr_decompression_time = getDecompressionTimeFromLog(logfile_name)
        curr_ratio = getRATIOfromLog(logfile_name)
        curr_name = "BROTLI-" + str(level)
        
        curr_line = curr_name + "\n" + str(curr_ratio) + " " + str(curr_compression_time) + " " + str(curr_decompression_time) +"\n"
        with open(summary_file, "a") as myfile:
            myfile.write(curr_line)
            myfile.write("\n\n")
    
    for level in lzma_levels:
        curr_folder = working_folder+"/LZMA_LEVEL" + str(level)
        logfile_name = curr_folder + "/" + "log.txt"
        
        curr_compression_time = getCompressionTimeFromLog(logfile_name)
        curr_decompression_time = getDecompressionTimeFromLog(logfile_name)
        curr_ratio = getRATIOfromLog(logfile_name)
        curr_name = "LZMA-" + str(level)
        
        curr_line = curr_name + "\n" + str(curr_ratio) + " " + str(curr_compression_time) + " " + str(curr_decompression_time) +"\n"
        with open(summary_file, "a") as myfile:
            myfile.write(curr_line)
            myfile.write("\n\n")
    
    for budget_ref in rlz_budgets:
        for threads in threads_range:
            curr_folder = working_folder+"/RLZ_STORE_BUDGET_" + str(budget_ref) + "_THREADS" + str(threads) 
            logfile_name = curr_folder + "/" + "log.txt"
            curr_compression_time = getCompressionTimeFromLog(logfile_name)
            curr_decompression_time = getDecompressionTimeFromLog(logfile_name)
            curr_ratio = getRATIOfromLog(logfile_name)
            #curr_name = "RLZ-STORE-Budget" + str(budget_ref) + "-T" + str(threads)
            curr_name = "RLZ-STORE-Budget" + str(budget_ref)
            curr_line = curr_name + "\n" + str(curr_ratio) + " " + str(curr_compression_time) + " " + str(curr_decompression_time) +"\n"
            with open(summary_file, "a") as myfile:
                myfile.write(curr_line)
                myfile.write("\n\n")
    
    PlotCompression(filename, working_folder)

def PlotCompression(input_filename, working_folder):
    basename = os.path.basename(input_filename)
    for plotfile in os.listdir("./plotters/compression"):
        if plotfile.endswith(".p"):
            sed_operation = " sed -i s/COLNAME/" + basename + "/ " + plotfile + " ;"
            command_plot = "cp ./plotters/compression/" + plotfile+ " "+ working_folder + ";";
            command_plot +="cd "+working_folder+"; " + sed_operation + " gnuplot "+ plotfile
            run_with_timeout(command_plot, -99)
