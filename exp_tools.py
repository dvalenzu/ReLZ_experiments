import os
import sys
from subprocess import call
from tools import *
from exp_tools import *

N_CHUNKS=32
N_THREADS=16

#N_SAMPLE=8
#N_MICROSAMPLE=4
N_SAMPLE=10
N_MICROSAMPLE=1

#SMALL_THRESHOLD=3000000
#LARGE_THRESHOLD=6000000
SMALL_THRESHOLD=1000000000
LARGE_THRESHOLD=2147000000
MAX_MEM_MB=30000
known_size = {}
known_size["english"]=2210395553
known_size["influenza"]=154808555
known_size["sources"]=210866607
known_size["einstein.en.txt"]=467626544

#t_values = [1,16,32,64,96]
t_values = [1,32]
mem_values = [8000, 16000]
depth_values = [1, 4]

def experiment2(input_filename, working_folder):
    runRLZ_PLUS_2(input_filename, working_folder)

def runRLZ_PLUS_2(input_filename, working_folder):
    for max_depth in depth_values:
        for max_mem_mb in mem_values:
            for t  in t_values:
                chunks = 2*int(t)
                #print "ref size:" , str(ref_size
                curr_folder=working_folder+"/RLZ_PLUS_THREADS_" + str(t) + "_DEPTH_" + str(max_depth) + "_MEM_" + str(max_mem_mb) + "_files"
                ensure_dir(curr_folder)

                logfile_name = curr_folder + "/" + "log.txt"
                output_name = curr_folder + "/" + "compressed_file"
                command = "/usr/bin/time -v ./ext/ReLZ/ReLZ " + input_filename + " -o " + output_name +" -s -t" + str(t) + " -c" + str(chunks) + " -m" + str(max_mem_mb) + " -d"+ str(max_depth) + " -v4 > " + logfile_name + " 2>&1"
                print "Will call: " + command
                call([command], shell=True)
                cleanupFolder(curr_folder)
    summarizeTIME_ReLZ_THREADS(working_folder)
    summarizePHRASES_ReLZ_THREADS(working_folder)

def summarizeTIME_ReLZ_THREADS(working_folder):
    for max_depth in depth_values:
        for max_mem_mb in mem_values:
            out = open(working_folder+'/time_summaryMEM_'+str(max_mem_mb)+'_DEPTH_'+str(max_depth)+'_THREADS.txt', 'w+')
            for t  in t_values:
                curr_folder=working_folder+"/RLZ_PLUS_THREADS_" + str(t) + "_DEPTH_" + str(max_depth) + "_MEM_" + str(max_mem_mb) + "_files"
                logfile_name = curr_folder + "/" + "log.txt"
                time = getCompressionTimeFromLog(logfile_name)
                out.write(time)
                out.write(" ")
            out.write("\n")
            out.close()

def summarizePHRASES_ReLZ_THREADS(working_folder):
    for max_depth in depth_values:
        for max_mem_mb in mem_values:
            out = open(working_folder+'/phrases_summaryMEM_'+str(max_mem_mb)+'_DEPTH_'+str(max_depth)+'_THREADS.txt', 'w+')
            for t  in t_values:
                curr_folder=working_folder+"/RLZ_PLUS_THREADS_" + str(t) + "_DEPTH_" + str(max_depth) + "_MEM_" + str(max_mem_mb) + "_files"
                logfile_name = curr_folder + "/" + "log.txt"
                phrases = getNPHRASESfromLog(logfile_name)
                out.write(phrases)
                out.write(" ")
            out.write("\n")
            out.close()

def experiment1(input_filename, working_folder):
    size = os.path.getsize(input_filename)
    if (size <= SMALL_THRESHOLD):
        #small
        #runKKP3(input_filename, working_folder)
        #runLZscan(input_filename, working_folder)
        runEMLZscan(input_filename, working_folder, MAX_MEM_MB)
        pass
    elif (size <= LARGE_THRESHOLD):
        #med
        #runLZscan(input_filename, working_folder)
        runEMLZscan(input_filename, working_folder, MAX_MEM_MB)
    else:
        #large
        runEMLZscan(input_filename, working_folder, MAX_MEM_MB)
    
    runRLZ_PLUS(input_filename, working_folder)
    #runRLZ_PLUS_REC(input_filename, working_folder)
    runRLZ_pre(input_filename, working_folder)

def GetFileLen(input_filename):
    size = os.path.getsize(input_filename)
    if (size == 0):
        basename = os.path.basename(input_filename)
        return known_size[basename]
    return size

def runLZscan(input_filename, working_folder):
    curr_folder = working_folder+"/IMLZscan_files"
    ensure_dir(curr_folder)
    logfile_name = curr_folder + "/" + "log.txt"
    output_name = curr_folder + "/" + "compressed.lz"
    #MAX_MEM_MB = FILE_SIZE_MB + BLOCK_SIZE * 27
    size = os.path.getsize(input_filename)
    file_size_MB = size / 1024
    BLOCK_SIZE = (MAX_MEM_MB - file_size_MB)/27
    command = "/usr/bin/time -v ./ext/LZscan/examples/count " + input_filename + " 10 > " + logfile_name + " 2>&1"
    print "Will call: " + command
    call([command], shell=True)
   
    phrases_file = curr_folder + "/" + "nphrases.txt"
    time_file = curr_folder + "/" + "time.txt"
    mem_file = curr_folder + "/" + "mem.txt"
    
    nphrases = getNPHRASESfromLog(logfile_name)
    time = getCompressionTimeFromLog(logfile_name)
    mem = getMEMfromLog(logfile_name)
    
    out_p = open(phrases_file, 'w+')
    out_p.write(str(nphrases));
    out_p.close()
    
    out_t = open(time_file, 'w+')
    out_t.write(str(time));
    out_t.close()
    
    out_m = open(mem_file, 'w+')
    out_m.write(str(mem));
    out_m.close()
    cleanupFolder(curr_folder)

def runRLZ_pre(input_filename, working_folder):
    f = open(working_folder+'/refs_sizes.txt', 'r')
    counter = 1
    for line in f:
        ref_size = line.rstrip('\n')
        if int(ref_size) <= 100:
            continue
        curr_folder=working_folder+"/RLZ_PRE_ref" + str(ref_size) + "_files"
        ensure_dir(curr_folder)

        logfile_name = curr_folder + "/" + "log.txt"
        if is_valid_log_file(logfile_name):  ## I could add a conditional variable.
            continue 
        output_name = curr_folder + "/" + "compressed_file"
        command = "/usr/bin/time -v ./ext/ReLZ/ReLZ " + input_filename + " -o " + output_name +" -s -l " + ref_size + " -t" + str(N_THREADS) + " -c" + str(N_CHUNKS) + " -m" + str(MAX_MEM_MB) + " -d0 -v4 > " + logfile_name + " 2>&1"
        print "Will call: " + command
        call([command], shell=True)
        counter = counter + 1
        cleanupFolder(curr_folder)
    summarizeRLZPRE(input_filename, working_folder)
    f.close()
 
def runRLZ_PLUS(input_filename, working_folder):
    f = open(working_folder+'/refs_sizes.txt', 'r')
    counter = 1
    for line in f:
        ref_size = line.rstrip('\n')
        #print "ref size:" , str(ref_size
        curr_folder=working_folder+"/RLZ_PLUS_ref" + str(ref_size) + "_files"
        ensure_dir(curr_folder)

        logfile_name = curr_folder + "/" + "log.txt"
        if is_valid_log_file(logfile_name):  ## I could add a conditional variable.
            continue 
        output_name = curr_folder + "/" + "compressed_file"
        command = "/usr/bin/time -v ./ext/ReLZ/ReLZ " + input_filename + " -o " + output_name +" -s -l " + ref_size + " -t" + str(N_THREADS) + " -c" + str(N_CHUNKS) + " -m" + str(MAX_MEM_MB) + " -d1 -v4 > " + logfile_name + " 2>&1"
        print "Will call: " + command
        call([command], shell=True)
        counter = counter + 1
        cleanupFolder(curr_folder)
    summarizeRLZ_PLUS(input_filename, working_folder)
    summarizeRLZ_PLUS_TIME(working_folder)
    summarizeRLZ_PLUS_MEM(working_folder)
    f.close()

def runRLZ_PLUS_REC(input_filename, working_folder):
    f = open(working_folder+'/refs_sizes.txt', 'r')
    counter = 1
    for line in f:
        ref_size = line.rstrip('\n')
        file_size = GetFileLen(input_filename)
        if (ref_size > file_size/40):
            break
        MEM_LIMIT = file_size*0.5;
        #print "ref size:" , str(ref_size
        curr_folder=working_folder+"/RLZ_PLUS_REC_ref" + str(ref_size) + "_files"
        ensure_dir(curr_folder)

        logfile_name = curr_folder + "/" + "log.txt"
        output_name = curr_folder + "/" + "compressed_file"
        command = "/usr/bin/time -v ./ext/ReLZ/ReLZ " + input_filename + " -o " + output_name +" -s -l " + ref_size + " -t" + str(N_THREADS) + " -c" + str(N_CHUNKS) + " -m" + str(MEM_LIMIT) + " -d100 -v4 > " + logfile_name + " 2>&1"
        print "Will call: " + command
        call([command], shell=True)
        counter = counter + 1
        cleanupFolder(curr_folder)
    summarizeRLZ_PLUS_REC(working_folder)
    summarizeRLZ_PLUS_REC_TIME(working_folder)
    summarizeRLZ_PLUS_REC_MEM(working_folder)
    f.close()

def summarizeRLZPRE(input_filename, working_folder):
    size = os.path.getsize(input_filename)
    emlz_logfile = working_folder + "/EMLZscan_files/log.txt"
    real_z = getNPHRASESfromLog(emlz_logfile)
    out = open(working_folder+'/summaryRLZ.txt', 'w+')
    f1 = open(working_folder+'/refs_sizes.txt', 'r')
    for line in f1:
        ref_size = line.rstrip('\n')
        if int(ref_size) <= 100:
            continue
        logfile_name = working_folder+"/RLZ_PRE_ref" + str(ref_size) + "_files/log.txt"
        n_phrases = getNPHRASESfromLog(logfile_name)
        out.write(ref_size)
        out.write(" ")
        out.write(str(n_phrases))
        out.write(" ")
        out.write(str(round(float(ref_size)/float(size),2)))
        out.write(" ")
        out.write(str(round(float(n_phrases)/float(real_z),2)))
        out.write("\n")
    out.close()
    f1.close()

def summarizeRLZ_PLUS_REC(working_folder):
    out = open(working_folder+'/summaryRLZ_PLUS_REC.txt', 'w+')
    f1 = open(working_folder+'/refs_sizes.txt', 'r')
    for line in f1:
        ref_size = line.rstrip('\n')
        logfile_name = working_folder+"/RLZ_PLUS_REC_ref" + str(ref_size) + "_files/log.txt"
        #n_phrases = get_RLZ_PLUS_REC_phrases(ref_size,working_folder)
        n_phrases = getNPHRASESfromLog(logfile_name)
        out.write(ref_size)
        out.write(" ")
        out.write(str(n_phrases))
        out.write("\n")
    out.close()
    f1.close()

def summarizeRLZ_PLUS_REC_TIME(working_folder):
    out = open(working_folder+'/summaryRLZ_PLUS_REC_TIME.txt', 'w+')
    f1 = open(working_folder+'/refs_sizes.txt', 'r')
    for line in f1:
        ref_size = line.rstrip('\n')
        logfile_name = working_folder+"/RLZ_PLUS_REC_ref" + str(ref_size) + "_files/log.txt"
        time = getCompressionTimeFromLog(logfile_name)
        out.write(ref_size)
        out.write(" ")
        out.write(str(time))
        out.write("\n")
    out.close()
    f1.close()

def summarizeRLZ_PLUS_REC_MEM(working_folder):
    out = open(working_folder+'/summaryRLZ_PLUS_REC_MEM.txt', 'w+')
    f1 = open(working_folder+'/refs_sizes.txt', 'r')
    for line in f1:
        ref_size = line.rstrip('\n')
        logfile_name = working_folder+"/RLZ_PLUS_REC_ref" + str(ref_size) + "_files/log.txt"
        mem = getMEMfromLog(logfile_name)
        out.write(ref_size)
        out.write(" ")
        out.write(str(mem))
        out.write("\n")
    out.close()
    f1.close()

def summarizeRLZ_PLUS(input_filename, working_folder):
    size = os.path.getsize(input_filename)
    emlz_logfile = working_folder + "/EMLZscan_files/log.txt"
    real_z = getNPHRASESfromLog(emlz_logfile)
    out = open(working_folder+'/summaryRLZ_PLUS.txt', 'w+')
    f1 = open(working_folder+'/refs_sizes.txt', 'r')
    for line in f1:
        ref_size = line.rstrip('\n')
        logfile_name = working_folder+"/RLZ_PLUS_ref" + str(ref_size) + "_files/log.txt"
        #n_phrases = get_RLZ_PLUS_phrases(ref_size,working_folder)
        n_phrases = getNPHRASESfromLog(logfile_name)
        out.write(ref_size)
        out.write(" ")
        out.write(str(n_phrases))
        out.write(" ")
        out.write(str(round(float(ref_size)/float(size),2)))
        out.write(" ")
        out.write(str(round(float(n_phrases)/float(real_z),2)))
        out.write("\n")
    out.close()
    f1.close()

def summarizeRLZ_PLUS_TIME(working_folder):
    out = open(working_folder+'/summaryRLZ_PLUS_TIME.txt', 'w+')
    f1 = open(working_folder+'/refs_sizes.txt', 'r')
    for line in f1:
        ref_size = line.rstrip('\n')
        logfile_name = working_folder+"/RLZ_PLUS_ref" + str(ref_size) + "_files/log.txt"
        time = getCompressionTimeFromLog(logfile_name)
        out.write(ref_size)
        out.write(" ")
        out.write(str(time))
        out.write("\n")
    out.close()
    f1.close()

def summarizeRLZ_PLUS_MEM(working_folder):
    out = open(working_folder+'/summaryRLZ_PLUS_MEM.txt', 'w+')
    f1 = open(working_folder+'/refs_sizes.txt', 'r')
    for line in f1:
        ref_size = line.rstrip('\n')
        logfile_name = working_folder+"/RLZ_PLUS_ref" + str(ref_size) + "_files/log.txt"
        mem = getMEMfromLog(logfile_name)
        out.write(ref_size)
        out.write(" ")
        out.write(str(mem))
        out.write("\n")
    out.close()
    f1.close()

def stats_from_file(input_filename, working_folder):
    out1 = open(working_folder+"/refs_sizes.txt", 'w')
    out1.write("1\n")
    size = os.path.getsize(input_filename)
    step = size/N_SAMPLE
    microstep = step/N_MICROSAMPLE 
    for i in range(1,N_MICROSAMPLE-1):
        if (i*microstep <= SMALL_THRESHOLD):
            out1.write(str(i*microstep))
            out1.write("\n")
    
    # get file length
    out1.write(str(step/2))
    out1.write("\n")
    for i in range(1,N_SAMPLE):
        if (i*step <= SMALL_THRESHOLD):
            out1.write(str(i*step))
            out1.write("\n")
    if (size <= SMALL_THRESHOLD):
        out1.write(str(size))
        out1.write("\n")
    out1.close()
    
def doPrint(input_filename, working_folder):
    size = GetFileLen(input_filename)
    emlz_logfile = working_folder + "/EMLZscan_files/log.txt"
    #z = getNPHRASESfromLog(emlz_logfile)
    #max_y = 10*int(z);
    basename = os.path.basename(input_filename)
    for plotfile in os.listdir("./plotters/exp1/"):
        if plotfile.endswith(".p"):
            #print "Z is" + str(z)
            #print "max_y is " + str(max_y)
            sed_operation = " sed -i s/COLNAME/" + basename + "/ " + plotfile + " ;"
            if (size <= SMALL_THRESHOLD):
                #small
                #sed_operation += " sed -i /EMLZscan/d " + plotfile + " ;
                pass
            elif (size <= LARGE_THRESHOLD):
                #med
                sed_operation += " sed -i /KKP3/d " + plotfile + " ;"
                #sed_operation += " sed -i /EMLZscan/d " + plotfile + " ;"
            else:
                #large
                sed_operation += " sed -i /KKP3/d " + plotfile + " ;"
                sed_operation += " sed -i /IMLZscan/d " + plotfile + " ;"
            sed_operation += "sed -i s/FILESIZE/" + str(size)+ "/ " + plotfile + " ;"
            if (basename == "einstein.en.txt"):
                sed_operation += "sed -i s/#PLACEHOLDER/#PLACEHOLDER\rset\ logscale\ y/ " + plotfile + " ;"
                sed_operation += "tr '\r' '\n' < " + plotfile + " > tmp.plot;"
                sed_operation += "mv tmp.plot " + plotfile + " ;"

            #PLACEHOLDER
            command_plot = "cp ./plotters/exp1/" + plotfile+ " "+ working_folder + ";";
            command_plot +="cd "+working_folder+"; " + sed_operation + " gnuplot "+ plotfile+ "; cd ..;"
            #print "calling: "+ command_plot 
            call([command_plot], shell=True)

