import os
import sys
import time
import filecmp
from subprocess import call

def run_with_timeout(command, timeout):
    import subprocess, time
    timeout += 2
    new_command = "timeout -s9 " + str(timeout) +" " +command
    if (timeout > 0):
        print "Calling: " + new_command
        call([new_command], shell=True)
    else:
        #TODO: this is the no-time out option.
        # Here we'd like to capture any error from command.
        print "Calling: " + command
        ret_code = call([command], shell=True)
        if ret_code != 0:
            print "Return code not zero:" + str(ret_code)
            sys.exit(ret_code)

def is_valid_log_file(logfile_name):
    if not os.path.isfile(logfile_name):
        return False
    if os.path.getsize(logfile_name) == 0:
        return False
    try:
        nphrases = getNPHRASESfromLog(logfile_name)
        time = getCompressionTimeFromLog(logfile_name)
        return True
    except:
        return False

def is_valid_compressor_log_file(logfile_name):
    if not os.path.isfile(logfile_name):
        return False
    if os.path.getsize(logfile_name) == 0:
        return False
    try:
        time = getCompressionTimeFromLog(logfile_name)
        ratio = getRATIOfromLog(logfile_name)
        return True
    except:
        return False


## COMPRESSION RUNNERS
def runLZMA(input_filename, working_folder, level, timeout):
    curr_folder = working_folder+"/LZMA_LEVEL" + str(level)
    ensure_dir(curr_folder)
    logfile_name = curr_folder + "/" + "log.txt"
    if is_valid_compressor_log_file(logfile_name):
        cleanupFolder(curr_folder)
        return
    output_name = input_filename + ".lzma"
    compress_command = "lzma -z -k -v -f -"+str(level) + " "  + input_filename + " > " + logfile_name + " 2>&1 "
    start = time.time()
    run_with_timeout(compress_command, timeout)
    end = time.time()
    compression_wtime = end - start
    
    output_len = os.path.getsize(output_name)
    input_len = os.path.getsize(input_filename)

    ratio = float(output_len)/float(input_len)
    ratio_line = "Compression ratio : " + str(ratio) + "\n"

    time_line = "Final Wtime(s)    : " + str(compression_wtime) + "\n"
    
    decompressed_name = input_filename + ".decompressed"
    if os.path.isfile(decompressed_name):
        os.remove(decompressed_name)
    renamed_lzma = decompressed_name + ".lzma"
    os.rename(output_name, renamed_lzma)
    # Decompress, measuring time
    decompress_command = "lzma -d -v " + renamed_lzma + " >> " + logfile_name + " 2>&1 "
    start = time.time()
    run_with_timeout(decompress_command, timeout)
    end = time.time()
    decompression_wtime = end - start
    if not filecmp.cmp(decompressed_name, input_filename, shallow=False):
        print "Decompression gave a wrong file"
        sys.exit(-1)
    os.remove(decompressed_name)
    
    decompression_line = "Decompression t(s)    : " + str(decompression_wtime) + "\n"
    
    with open(logfile_name, "a") as myfile:
            myfile.write(ratio_line)
            myfile.write(time_line)
            myfile.write(decompression_line)

def runRLZSTORE(input_filename, working_folder, budget_ref, threads, timeout):
    curr_folder = working_folder+"/RLZ_STORE_BUDGET_" + str(budget_ref) + "_THREADS" + str(threads) 
    ensure_dir(curr_folder)
    logfile_name = curr_folder + "/" + "log.txt"
    
    if is_valid_compressor_log_file(logfile_name):
        cleanupFolder(curr_folder)
        return

    COL_FOLDER = curr_folder + "/" + "compressed_collection"
    command_pre = "./ext/rlz-store/build/create-collection.x -c " + COL_FOLDER + "  -i " + input_filename + " > " + logfile_name + " 2>&1 "
    run_with_timeout(command_pre, -99)
    command_store = "./ext/rlz-store/build/rlzs-create.x -c " + COL_FOLDER + "  -s " + str(budget_ref)  + " -t " + str(threads) +" >> " + logfile_name + " 2>&1 "
    run_with_timeout(command_store, -99)
    cleanupFolder(curr_folder)

def createFrescoListFile(input_filename):
    (base_dir, base_name) = os.path.split(os.path.abspath(input_filename))
    list_file = base_dir + "/list.txt"
    with open(list_file, "w") as myfile:
        myfile.write(base_name)
    print "Just created:" + list_file
    print "with content:" + base_name

def deleteFrescoListFile(input_filename):
    (base_dir, base_name) = os.path.split(os.path.abspath(input_filename))
    list_file = base_dir + "list.txt"
    os.remove(list_file)

def runFRESCO(input_filename, working_folder, level, timeout):
    curr_folder = working_folder+"/FRESCO_LEVEL" + str(level)
    ensure_dir(curr_folder)
    logfile_name = curr_folder + "/" + "log.txt"
    if is_valid_compressor_log_file(logfile_name):
        cleanupFolder(curr_folder)
        return
    output_name = curr_folder + "/" + "compressed_file"
    config_file = "./ext/FRESCO/data/config.ini"
    createFrescoListFile(input_filename)
    comp_command = "./ext/FRESCO/FRESCO " + config_file + " COMPRESS " + input_filename + " " + output_name + "> " + logfile_name + " 2>&1"
    start = time.time()
    run_with_timeout(comp_command, timeout)
    end = time.time()
    compression_wtime = end - start
    time_line = "Final Wtime(s)    : " + str(compression_wtime) + "\n"
    #deleteFrescoListFile(input_filename)
    
    output_len = os.path.getsize(output_name)
    input_len = os.path.getsize(input_filename)
    ratio = float(output_len)/float(input_len)
    ratio_line = "Compression ratio : " + str(ratio) + "\n"
    
    decompressed_name = output_name+".decompressed"
    decomp_command = "./ext/brotli/out/brotli -d "+ output_name +" -o " + decompressed_name
    decomp_command = "./ext/FRESCO/FRESCO " + config_file + " DECOMPRESS " + output_name + " " + decompressed_name + "> " + logfile_name + " 2>&1"
    start = time.time()
    run_with_timeout(decomp_command, timeout)
    end = time.time()
    decompression_wtime = end - start
    decompression_line = "Decompression t(s)    : " + str(decompression_wtime) + "\n"
    
    if not filecmp.cmp(decompressed_name, input_filename, shallow=False):
        print "Decompression gave a wrong file"
        sys.exit(-1)
    
    with open(logfile_name, "a") as myfile:
            myfile.write(ratio_line)
            myfile.write(time_line)
            myfile.write(decompression_line)
    
    cleanupFolder(curr_folder)

def runBROTLI(input_filename, working_folder, level, timeout):
    curr_folder = working_folder+"/BROTLI_LEVEL" + str(level)
    ensure_dir(curr_folder)
    logfile_name = curr_folder + "/" + "log.txt"
    if is_valid_compressor_log_file(logfile_name):
        cleanupFolder(curr_folder)
        return
    output_name = curr_folder + "/" + "compressed_file"
    
    comp_command = "./ext/brotli/out/brotli -o " + output_name + " -q " + str(level) + " -v -f " + input_filename + "> " + logfile_name + " 2>&1"
    start = time.time()
    run_with_timeout(comp_command, timeout)
    end = time.time()
    compression_wtime = end - start
    time_line = "Final Wtime(s)    : " + str(compression_wtime) + "\n"
    
    output_len = os.path.getsize(output_name)
    input_len = os.path.getsize(input_filename)
    ratio = float(output_len)/float(input_len)
    ratio_line = "Compression ratio : " + str(ratio) + "\n"
    
    decompressed_name = output_name+".decompressed"
    decomp_command = "./ext/brotli/out/brotli -d "+ output_name +" -o " +decompressed_name
    start = time.time()
    run_with_timeout(decomp_command, timeout)
    end = time.time()
    decompression_wtime = end - start
    decompression_line = "Decompression t(s)    : " + str(decompression_wtime) + "\n"
    
    if not filecmp.cmp(decompressed_name, input_filename, shallow=False):
        print "Decompression gave a wrong file"
        sys.exit(-1)
    
    with open(logfile_name, "a") as myfile:
            myfile.write(ratio_line)
            myfile.write(time_line)
            myfile.write(decompression_line)
    
    cleanupFolder(curr_folder)

## SCALE RUNNERS:
def runLZend(filename, working_folder, timeout):
    int_size = compute_int_size(filename)
    curr_folder = working_folder+"/LZend_files"
    ensure_dir(curr_folder)
    logfile_name = curr_folder + "/" + "log.txt"
    if is_valid_log_file(logfile_name):  ## I could add a conditional variable.
        cleanupFolder(curr_folder)
        return
    output_name = curr_folder + "/" + "compressed_file"
    command = "./ext/LZ-End-Toolkit-0.1.0/parse " + filename + " -o " + output_name + " -i" + str(int_size) + " -v > " + logfile_name + " 2>&1"
    run_with_timeout(command, timeout)
    cleanupFolder(curr_folder)
    if is_valid_log_file(logfile_name):
        return False
    else: 
        return True

def runORLBWT_LZ77(filename, working_folder, timeout):
    file_len = os.path.getsize(filename)
    #if (file_len > 524288):  # 262144 ?
    #    return
    curr_folder = working_folder+"/ORLBWT_LZ77_files"
    ensure_dir(curr_folder)
    logfile_name = curr_folder + "/" + "log.txt"
    if is_valid_log_file(logfile_name):  ## I could add a conditional variable.
        cleanupFolder(curr_folder)
        return
    output_name = curr_folder + "/" + "compressed_file"
    command = "./ext/OnlineRlbwt/build/OnlineLz77ViaRlbwt -i " + filename + " -o " + output_name + "  > " + logfile_name + " 2>&1"
    run_with_timeout(command, timeout)
    cleanupFolder(curr_folder)
    if is_valid_log_file(logfile_name):
        return False
    else: 
        return True

def runRLE_LZ77_2(filename, working_folder, timeout):
    # TOO SLOW, not being called... for now...
    file_len = os.path.getsize(filename)
    curr_folder = working_folder+"/RLE_LZ77_2_files"
    ensure_dir(curr_folder)
    logfile_name = curr_folder + "/" + "log.txt"
    if is_valid_log_file(logfile_name):  ## I could add a conditional variable.
        cleanupFolder(curr_folder)
        return
    output_name = curr_folder + "/" + "compressed_file"
    command = "./ext/dynamic/bin/rle_lz77_v2 " + filename + " " + output_name + "  > " + logfile_name + " 2>&1"
    run_with_timeout(command, timeout)
    cleanupFolder(curr_folder)
    if is_valid_log_file(logfile_name):
        return False
    else: 
        return True

#Returns TRUE if it timed out
def runReLZ(filename, working_folder, mem_limit, threads, maxD, timeout):
    curr_folder = working_folder+"/ReLZ_MEM" + str(mem_limit) + "_THREADS_" + str(threads) + "_D_" + str(maxD) + "_files"
    ensure_dir(curr_folder)
    logfile_name = curr_folder + "/" + "log.txt"
    if is_valid_log_file(logfile_name):  ## I could add a conditional variable.
        cleanupFolder(curr_folder)
        return
    output_name = curr_folder + "/" + "compressed_file"
    command = "./ext/ReLZ/ReLZ " + filename + " -o " + output_name +" -t" + str(threads) + " -m" + str(mem_limit) + " -d" + str(maxD) + " -v4 -e MAX > " + logfile_name + " 2>&1"
    run_with_timeout(command, timeout)
    if not is_valid_log_file(logfile_name):
        cleanupFolder(curr_folder)
        return True
    else:
        #valid log, did not timeout, let's decompress:
        decompressed_name = output_name + ".decompressed"
        decomp_command = "./ext/ReLZ/decode_max  "+ output_name +" " +decompressed_name + " >> " + logfile_name + " 2>&1"
        
        start = time.time()
        run_with_timeout(decomp_command, -99)
        end = time.time()
        decompression_wtime = end - start
        decompression_line = "Decompression t(s)    : " + str(decompression_wtime) + "\n"
        
        if not filecmp.cmp(decompressed_name, filename, shallow=False):
            print "Decompression gave a wrong file"
            print "Probably caused by EM-LZ-Decompressor bug."
            sys.exit(-1)
        
        with open(logfile_name, "a") as myfile:
            myfile.write(decompression_line)
        
        cleanupFolder(curr_folder)
        return False

### OTHER RUNNERS
def runEMLZscan(input_filename, working_folder, max_mem_mb, timeout = -100):
    curr_folder = working_folder+"/EMLZscan_files"
    ensure_dir(curr_folder)
    logfile_name = curr_folder + "/" + "log.txt"
    if is_valid_log_file(logfile_name):  ## I could add a conditional variable.
        cleanupFolder(curr_folder)
        if is_valid_log_file(logfile_name):
            return False  # not timedout yet
        else:
            return True
    output_name = curr_folder + "/" + "compressed.lz"
    command = "/usr/bin/time -v ./ext/EM-LZscan-0.2/src/emlz_parser -o " + output_name + " -m" + str(max_mem_mb) + " " + input_filename + " > " + logfile_name + " 2>&1"
    run_with_timeout(command, timeout)
   
    phrases_file = curr_folder + "/" + "nphrases.txt"
    time_file = curr_folder + "/" + "time.txt"
    #mem_file = curr_folder + "/" + "mem.txt"
    
    if not is_valid_log_file(logfile_name):
        cleanupFolder(curr_folder)
        return True  ## timed out
    
    nphrases = getNPHRASESfromLog(logfile_name)
    time = getCompressionTimeFromLog(logfile_name)
    #mem = getMEMfromLog(logfile_name)
    
    out_p = open(phrases_file, 'w+')
    out_p.write(str(nphrases));
    out_p.close()
    
    out_t = open(time_file, 'w+')
    out_t.write(str(time));
    out_t.close()
    
    #out_m = open(mem_file, 'w+')
    #out_m.write(str(mem));
    #out_m.close()
    cleanupFolder(curr_folder)
    return False


def cleanupFolder(working_folder):
    command_clean="rm -rf " + working_folder + "/compressed*"
    #print command_clean
    call([command_clean], shell=True)

def getNPHRASESfromLog(logfile_name):
    f = open(logfile_name, 'r')
    ans = []
    for line in f:
        if "N Phrases" in line:
            tokens = line.rstrip().split(":")
            ans.append(tokens[1])
    return ans[0]

def getCompressionTimeFromLog(logfile_name):
    f = open(logfile_name, 'r')
    ans = []
    for line in f:
        if "Final Wtime(s)" in line:
            tokens = line.rstrip().split(":")
            ans.append(tokens[1])
    return ans[0]


def getDecompressionTimeFromLog(logfile_name):
    f = open(logfile_name, 'r')
    ans = []
    for line in f:
        if "Decompression t(s)" in line:
            tokens = line.rstrip().split(":")
            ans.append(tokens[1])
    return ans[0]


def getRATIOfromLog(logfile_name):
    f = open(logfile_name, 'r')
    ans = []
    for line in f:
        if "Compression ratio" in line:
            tokens = line.rstrip().split(":")
            ans.append(tokens[1])
    return ans[0]


def getMEMfromLog(logfile_name):
    f = open(logfile_name, 'r')
    ans = []
    for line in f:
        if "Maximum resident set size" in line:
            tokens = line.rstrip().split(" ")
            ans.append(tokens[5])
    return ans[0]

def ensure_dir(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)

def bitcount(n):
    count = 0
    while n > 0:
        count += 1
        n >>= 1
    return count

def log2(n):
    return bitcount(n)-1

def compute_int_size(filename):
    file_len = os.path.getsize(filename)
    bits = bitcount(file_len)
    ans = (bits+7)/8
    ans = max(ans, 4)
    assert(ans <= 8)
    return ans

def len_to_human(n):
    if (n >= (1<<30)):
        factor = 1<<30
        prefix = "GiB"
        num = n/factor
    elif (n >= (1<<20)):
        factor = 1<<20
        prefix = "MiB"
        num = n/factor
    else:
        factor = 1
        prefix = "b"
        num = n/factor
        
    return (num,prefix)

def ensure_prefix_exists(filename, prefix_len):
    basename = os.path.basename(filename)
    dirname = os.path.dirname(filename)
    
    suffix = "".join(map(str,len_to_human(prefix_len))) 
    target_file = dirname+"/"+basename+"."+suffix
    #print "Target: " + target_file
    if os.path.isfile(target_file):
        pass
    else:
        #print "making it..."
        command = "head -c " + str(prefix_len) + " " + filename + " > " + target_file
        call([command], shell=True)
    
    real_size = os.path.getsize(target_file)
    assert(real_size == prefix_len)
    return target_file

