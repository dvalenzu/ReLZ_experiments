#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail 

DATA_DIR="./data/large"
for FILEPATH in ${DATA_DIR}/*; do
	FILENAME=$(basename -- "${FILEPATH}")
	if [[ ${FILENAME} == *.* ]]; then
		continue
	fi
	if [[ -d ${FILEPATH} ]]; then
		continue
	fi
	echo ${FILENAME}
	TARGET_DIR="./RESULTS_SCALABILITY/${FILENAME}"
	#rm -rf ${TARGET_DIR}; 
  ./experiment_scalability.py ${FILEPATH} ${TARGET_DIR}
done
