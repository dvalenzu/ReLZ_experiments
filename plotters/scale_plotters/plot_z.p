# este encoding permite: set title "a=\341 e=\351 i=\355 o=\363 u=\372" 
set encoding iso_8859_1
#set terminal png        # gnuplot recommends setting terminal before output
#set terminal postscript enhanced color        # gnuplot recommends setting terminal before output
set terminal postscript eps color colortext
set output "Phrases.eps" # The output filename; to be set after setting
                        # terminal
set   autoscale                        # scale axes automatically
#unset log                              # remove any log-scaling
#unset label                            # remove any previous labels
#set xtic auto                          # set xtics automatically
#set ytic auto                          # set ytics automatically
#set xlabel "Block Size"
#set ylabel "YUNIT"
set key top right Left box width 2
#FLAG_KEY_TR#set key top right box width 2
#FLAG_NOKEY#set nokey
#set label "Yield Point" at 0.003,260
#set arrow from 0.0028,250 to 0.003,280
#set xr [0:MAX_X*1.1]
#set yr [1:*]
#set zero 1e-40
#PLACEHOLDER
#set logscale y
set style line 1 lw 1 pt 5 lt 2 dashtype 1 lc "black"
set style line 2 lw 1 pt 2 lt 2 dashtype 1 lc "dark-magenta"
set style line 3 lw 1 pt 1 lt 2 dashtype 1 lc "red"
set style line 4 lw 1 pt 2 lt 2 dashtype 1 lc "dark-green"
set style line 5 lw 1 pt 8 lt 2 dashtype 1 lc "blue"
#
#
set xtics ("2^{29}" 29, "2^{30}" 30, "2^{31}" 31, "2^{32}" 32, "2^{33}" 33, "2^{34}" 34, "2^{35}" 35, "2^{36}" 36)
set title "COLNAME"
plot  "nphrases_LZend.txt" using 1:3 title "LZ-End" with linespoints ls 2,\
      "nphrases_ReLZ_MEMXNORMALMEMX_THREADS_1_D_0.txt" using 1:3 title "RLZ_{PRE}" with linespoints ls 4,\
      "nphrases_ReLZ_MEMXNORMALMEMX_THREADS_4_D_99.txt" using 1:3 title "ReLZ" with linespoints ls 5
