set encoding iso_8859_1
set terminal postscript eps color colortext
set output "Compression.eps"
set key right top Left;
set key font ",15"
set title "COLNAME"
#RLZ: blues
set style line 1 pt 11 lc rgb "#0055FF" ps 1.2
set style line 2 pt 8 lc rgb "#000099" ps 1.2
# Brotli:  violets
set style line 3 pt 49 lc rgb '#EE00EE' ps 1.2
set style line 4 pt 50 lc rgb '#8B008B' ps 1.2
set style line 5 pt 52 lc rgb '#4B0082' ps 1.2
#LZMA orange to brown
set style line 6 pt 17 lc rgb '#FFA500 ' ps 1.2
set style line 7 pt 18 lc rgb '#D2691E' ps 1.2
set style line 8 pt 20 lc rgb '#8B4513' ps 1.2
#RLZ-STORE greens
set style line 9 pt 34 lc rgb '#90EE90' ps 1.2
set style line 10 pt 36 lc rgb '#3CB371' ps 1.2
set style line 11 pt 40 lc rgb '#6B8E23' ps 1.2
set style line 12 pt 45 lc rgb '#808000' ps 1.2
set style line 13 pt 43 lc rgb '#808000' ps 1.2
set style line 14 pt 39 lc rgb '#808000' ps 1.2
plot for [IDX=0:*] 'compression_results.txt' i IDX u 1:2 w points ls (IDX+1) title columnheader(1)
