#!/usr/bin/env bash
set -o errexit
set -o nounset

if [ ! -f "./English" ]; then
	wget http://pizzachili.dcc.uchile.cl/texts/nlang/english.200MB.gz
	gunzip english.200MB.gz
  mv english.200MB English
fi

if [ ! -f "./Sources" ]; then
	wget http://pizzachili.dcc.uchile.cl/texts/code/sources.gz
	gunzip sources.gz
  mv sources Sources
fi

if [ ! -f "./Influenza" ]; then
	wget http://pizzachili.dcc.uchile.cl/repcorpus/real/influenza.7z
	p7zip -d influenza.7z
	rm -f influenza.7z
  mv influenza Influenza
fi

if [ ! -f "./Leaders" ]; then
	wget http://pizzachili.dcc.uchile.cl/repcorpus/real/world_leaders.7z
	p7zip -d world_leaders.7z
	rm -f world_leaders.7z
  mv world_leaders Leaders
fi

cp English large
