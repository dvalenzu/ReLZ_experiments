# Config parameters that work for validation test data (ie. relatively small)
# To reproduce the paper results replace this file with config.prod.py
EMLZ_MEM_MB=8000
ref_sizes = [1, 2, 3]
threads = 96
mem_limit_reference_methods = 8000
