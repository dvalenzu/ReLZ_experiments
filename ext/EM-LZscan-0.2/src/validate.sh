#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail 

source "./utils.sh"

DATA_DIR="../../utest_data/"
INPUT_FILE=${DATA_DIR}/einstein.en.sample.txt

MAX_MEM_MB=1000

CODER_BIN=./emlz_parser
DECODER_BIN=../../LZ-Decoder/decode
utils_assert_file_exists ${CODER_BIN} 
utils_assert_file_exists ${DECODER_BIN} 

COMPRESSED_FILE=tmp.rlz_compressed
DECOMPRESSED_FILE=tmp.reconstructed
rm -f ${COMPRESSED_FILE}
rm -f ${DECOMPRESSED_FILE}

echo "Encoding..."
${CODER_BIN} ${INPUT_FILE} --mem=${MAX_MEM_MB} --output=${COMPRESSED_FILE}
echo "Decoding..."
${DECODER_BIN}  ${COMPRESSED_FILE} ${DECOMPRESSED_FILE}
echo "Verifying (diff)..."
diff ${DECOMPRESSED_FILE} ${INPUT_FILE}

echo "Test succesfull"
