#!/usr/bin/env bash
set -o errexit
set -o nounset

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

TARGET_FOLDER="${DIR}/GDC2"
if [ ! -d "${TARGET_FOLDER}" ]; then

  echo "*********************************"
  echo "Compiling and installing GDC2"
  echo "*********************************"
  COMMIT="master"
	git clone https://github.com/dvalenzu/GDC2.git
	cd GDC2;
  git checkout ${COMMIT}
   
	cd gdc_2/Gdc2;
  make;
  cd ..; cd ..;
	
  cd vcf2fasta/src;
  make;
  cd ..;
  cd ..;
	
  cd ..;
else
  echo "GDC2 ALREADY INSTALLED"
fi

TARGET_FOLDER="${DIR}/FRESCO"
if [ ! -d "${TARGET_FOLDER}" ]; then

  echo "*********************************"
  echo "Compiling and installing FRESCO"
  echo "*********************************"
  COMMIT="master"
	git clone https://github.com/dvalenzu/FRESCO.git
	cd FRESCO;
  git checkout ${COMMIT}
  
	cd build;
  make;
  cd ..;
	cd ..;
else
  echo "FRESCO ALREADY INSTALLED"
fi

TARGET_FOLDER="${DIR}/brotli"
if [ ! -d "${TARGET_FOLDER}" ]; then

  echo "*********************************"
  echo "Compiling and installing brotli"
  echo "*********************************"
  COMMIT="master"
	git clone https://github.com/google/brotli.git
	cd brotli;
  git checkout ${COMMIT}
  
  mkdir out && cd out
  cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=./installed ..
  cmake --build . --config Release --target install
	cd ..;
	cd ..;
else
  echo "BROTLI ALREADY INSTALLED"
fi

TARGET_FOLDER="${DIR}/dynamic"
if [ ! -d "${TARGET_FOLDER}" ]; then

  echo "*********************************"
  echo "Compiling and installing DYNAMIC"
  echo "*********************************"
  
  COMMIT="c04d8d4"
	git clone https://github.com/nicolaprezza/dynamic
	cd dynamic;
  git checkout ${COMMIT}
  mkdir bin; cd bin
  cmake ..
	make;
	cd ..;
	cd ..;
else
  echo "DYNAMIC ALREADY INSTALLED"
fi

TARGET_FOLDER="${DIR}/ReLZ"
if [ ! -d "${TARGET_FOLDER}" ]; then

  echo "*********************************"
  echo "Compiling and installing ReLZ"
  echo "*********************************"
  
  COMMIT="master"
  git clone https://gitlab.com/dvalenzu/ReLZ.git
	cd ReLZ;
  git checkout ${COMMIT}
	make;
	cd ..;
else
  echo "ReLZ ALREADY INSTALLED"
fi

TARGET_FOLDER="${DIR}/artificial_reference"
if [ ! -d "${TARGET_FOLDER}" ]; then

  echo "*********************************"
  echo "Compiling and installing artificial_reference"
  echo "*********************************"
  
	git clone https://gitlab.com/dvalenzu/artificial_reference.git
  cd artificial_reference;
	./compile.sh
  cd ..;
else
  echo "artificial_reference ALREADY INSTALLED"
fi

TARGET_FOLDER="${DIR}/rlz-store"
if [ ! -d "${TARGET_FOLDER}" ]; then

  echo "*********************************"
  echo "Compiling and installing rlz-store"
  echo "*********************************"
  git clone https://github.com/dvalenzu/rlz-store.git
  cd rlz-store
  git submodule update --init --recursive
  mkdir build
  cd build
  cmake ..
  make -j
	cd ..;
	cd ..;
else
  echo "rlz-store ALREADY INSTALLED"
fi

TARGET_FOLDER="${DIR}/OnlineRlbwt/build"
if [ ! -d "${TARGET_FOLDER}" ]; then

  echo "*********************************"
  echo "Compiling and installing OnlineRlbwt"
  echo "*********************************"
  cd OnlineRlbwt; mkdir build; cd build; cmake ..; make; cd ..;
  cd ..;
else
  echo "OnlineRlbwt ALREADY INSTALLED"
fi

cd EM-LZscan-0.2/src; make; cd ../..;
cd LZ-End-Toolkit-0.1.0/; make; cd ..;
