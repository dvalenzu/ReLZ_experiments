#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail 

source "./utils.sh"
#NTHREADS=128
NTHREADS=4
GDC2=./ext/GDC2/gdc_2/Gdc2/gdc2
FRESCO=./ext/FRESCO/FRESCO
FRESCO_CONFIG_1=./ext/FRESCO/data/configB.ini 
#FRESCO_CONFIG_1=./ext/FRESCO/data/config.ini 
RELZ=./ext/ReLZ/ReLZ
RELZ_DECODE=./ext/ReLZ/decode_max

DATA_DIR="/home/work/dvalenzu/data/gen_test/"
#DATA_DIR="/home/work/dvalenzu/data/genome/"
OUTPUT_DIR="/home/work/dvalenzu/data/gen_output/"

#DATA_DIR="/home/scratch-ssd/dvalenzu/Data/gen_tests/"
#DATA_DIR="/home/scratch-ssd/dvalenzu/Data/genomes/"
#OUTPUT_DIR="/home/work/dvalenzu/data/gen_output/"
#OUTPUT_DIR="/home/scratch-ssd/dvalenzu/Data/gen_output/"

LIST_FILE=${DATA_DIR}/list.txt

#FULL_FILE=${DATA_DIR}/genome.full
FULL_FILE=${DATA_DIR}/chr21.2001versions.withheaders
#FULL_FILE=${DATA_DIR}/all.genomes

rm -rf ${OUTPUT_DIR}
mkdir -p ${OUTPUT_DIR}

utils_assert_dir_exists ${DATA_DIR}
utils_assert_file_exists ${LIST_FILE}
utils_assert_file_exists ${FULL_FILE}
utils_assert_file_exists ${GDC2}
utils_assert_file_exists ${FRESCO}
utils_assert_file_exists ${FRESCO_CONFIG_1}

## must include list.txt for fresco, if we run FRESCO
GDC_OUTPUT=${OUTPUT_DIR}/compreesed_by_gdc
RELZ_OUTPUT=${OUTPUT_DIR}/compressed_by_relz
RELZ_RECONSTRUCTED=${OUTPUT_DIR}/DECOMP_by_relz
FRESCO_OUTPUT_1=${OUTPUT_DIR}/compressed_by_fresco_v1
FRESCO_OUTPUT_2=${OUTPUT_DIR}/compressed_by_fresco_SO
FRESCO_RECONSTRUCTED=${OUTPUT_DIR}/DECOMP_by_fresco

echo "GDC2"
time ${GDC2} c -t${NTHREADS} ${GDC_OUTPUT}  ${DATA_DIR}/*.fa
time ${GDC2} d  ${GDC_OUTPUT} 
rm -f ${DATA_DIR}/*.ori

echo "RELZ"
rm -f ${RELZ_RECONSTRUCTED}
time ${RELZ} -v3 ${FULL_FILE} -l 45 -e MAX -t ${NTHREADS} -o ${RELZ_OUTPUT}
time ${RELZ_DECODE} ${RELZ_OUTPUT}  ${RELZ_RECONSTRUCTED}

time ${FRESCO} ${FRESCO_CONFIG_1} COMPRESS  ${DATA_DIR} ${FRESCO_OUTPUT_1}
# Config file tells how many additional references.
time ${FRESCO} ${FRESCO_CONFIG_1} SOCOMPRESS  ${FRESCO_OUTPUT_1} ${FRESCO_OUTPUT_2}
time ${FRESCO} ${FRESCO_CONFIG_1} SODECOMPRESS  ${FRESCO_OUTPUT_2} ${FRESCO_RECONSTRUCTED}

utils_success_exit
