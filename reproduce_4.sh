#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail 

#DATA_DIR="./data/"
DATA_DIR="/home/work/dvalenzu/data/large"
for FILEPATH in ${DATA_DIR}/*; do
	FILENAME=$(basename -- "${FILEPATH}")
	if [[ ${FILENAME} == *.* ]]; then
		continue
	fi
	if [[ -d ${FILEPATH} ]]; then
		continue
	fi
	echo ${FILENAME}
	TARGET_DIR="./RESULTS_DECOMP/${FILENAME}"
	#rm -rf ${TARGET_DIR}; 
  ./experiment_compression.py ${FILEPATH} ${TARGET_DIR}
done
