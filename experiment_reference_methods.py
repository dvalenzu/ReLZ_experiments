#!/usr/bin/env python
import os
import sys
from subprocess import call
from tools import *
from reference_methods_tools import *

def main(argv):
    n_args = len(sys.argv);
    if(n_args != 3):
        print 'Got ',n_args-1,' arguments, this is incorrect.'
        print 'Usage:'
        print sys.argv[0] , 'input_filename working_folder'
        sys.exit();
    input_filename=sys.argv[1];
    working_folder=sys.argv[2];
    ensure_dir(working_folder)
    just_replot = False;
    experiment_references(input_filename, working_folder, just_replot)
    pass

if __name__ == "__main__":
    main(sys.argv)
