import os
import sys
import ntpath
from shutil import copyfile
from subprocess import call
from tools import *
import config

EMLZ_MEM_MB = config.EMLZ_MEM_MB
ref_sizes = config.ref_sizes
threads = config.threads
mem_limit = config.mem_limit_reference_methods

def experiment_references(input_filename, working_folder, just_replot=False):
    if not just_replot:
        RunTools(input_filename, working_folder)
        print "*****************"
        print "Compression Ready. Next create summaries and plot:"
        print "*****************"
    SummarizeData(input_filename, working_folder)
    PlotReferences(input_filename, working_folder)

def RunTools(input_filename, working_folder):
    print "*****************"
    print " Creating references with different methods."
    print "*****************"
    for REF_SIZE_MB in ref_sizes:
        refs_folder = working_folder + "/references"
        ensure_dir(refs_folder)
        create_reference_rlz_store(input_filename, refs_folder, REF_SIZE_MB)
        create_reference_random(input_filename, refs_folder, REF_SIZE_MB)
    
    print "*****************"
    print " References Ready. Next: compress using ReLZ..."
    print "*****************"
    maxD = 99 
    for REF_SIZE_MB in ref_sizes:
        files = dict()
        files['PREFIX']  = input_filename
        files['PRUNE']  = input_filename + '.RLZSTORE_REF_' + str(REF_SIZE_MB) + 'MB'
        files['RANDOM'] = input_filename + '.RANDOMSAMPLING_REF_' + str(REF_SIZE_MB) + 'MB'
        
        for METHOD in files:
            curr_folder = working_folder + "/" + METHOD + '_' + str(REF_SIZE_MB) + 'MB'
            ensure_dir(curr_folder)
            curr_filename = files[METHOD]
            logfile_name = curr_folder + "/log_relz_final.txt"
            if is_valid_log_file(logfile_name):  ## I could add a conditional variable.
                cleanupFolder(curr_folder)
                continue
            output_name = curr_folder + "/compressed_file"
            command = "./ext/ReLZ/ReLZ " + curr_filename + " -l " + str(REF_SIZE_MB) + " -e VBYTE -o " + output_name +" -t" + str(threads) + " -m" + str(mem_limit) + " -d" + str(maxD) + " -v4 > " + logfile_name + " 2>&1"
            run_with_timeout(command, -99)
            cleanupFolder(curr_folder)
    
    runEMLZscan(input_filename, working_folder, EMLZ_MEM_MB, -99)

def SummarizeData(input_filename, working_folder):
    ## Now create summary files:
    emlz_logfile = working_folder + "/EMLZscan_files/log.txt"
    real_z = getNPHRASESfromLog(emlz_logfile)
    for METHOD in ["PREFIX" , "PRUNE" , "RANDOM"]:
        summary_file = working_folder + "/" + METHOD + ".data"
        if os.path.isfile(summary_file):
            os.remove(summary_file)
        with open(summary_file, "a") as myfile:
            myfile.write("Title    RLZ    ReLZ\n")

        for REF_SIZE_MB in ref_sizes:
            curr_folder = working_folder + "/" + METHOD + '_' + str(REF_SIZE_MB) + 'MB'
            ensure_dir(curr_folder)
            logfile_name = curr_folder + "/log_relz_final.txt"

            (rlz_phrases, relz_phrases) = getBothPhrasesFromLog(logfile_name)
            rlz_ratio = round(float(rlz_phrases)/float(real_z),2)
            relz_ratio = round(float(relz_phrases)/float(real_z),2)
            unit = "MB" 
            size_units = REF_SIZE_MB
            if (size_units >= 1000):
                size_units = size_units/1000
                unit = "GB"
            #curr_line = '"' + str(size_units) + unit +'"' + " " + str(rlz_phrases) + " " + str(relz_phrases)
            curr_line = '"' + str(size_units) + unit +'"' + " " + str(rlz_ratio) + " " + str(relz_ratio)
            with open(summary_file, "a") as myfile:
                myfile.write(curr_line)
                myfile.write("\n")

def PlotReferences(input_filename, working_folder):
    basename = os.path.basename(input_filename)
    for plotfile in os.listdir("./plotters/references"):
        if plotfile.endswith(".p"):
            sed_operation = " sed -i s/COLNAME/" + basename + "/ " + plotfile + " ;"
            command_plot = "cp ./plotters/references/" + plotfile+ " "+ working_folder + ";";
            command_plot +="cd "+working_folder+"; " + sed_operation + " gnuplot "+ plotfile
            run_with_timeout(command_plot, -99)

def getBothPhrasesFromLog(logfile_name):
    f = open(logfile_name, 'r')
    ans = []
    print "Scanning: " + logfile_name
    for line in f:
        if "Factors Total" in line:
            tokens = line.rstrip().split(":")
            ans.append(tokens[1])
        if "N Phrases" in line:
            tokens = line.rstrip().split(":")
            relz = tokens[1]
    assert(len(ans) >= 1)
    if (len(ans) > 2):
        print "SUPER WARNING"
    assert(relz == ans[-1])
    return ans[0], relz

def create_reference_rlz_store(input_filename, working_folder, REF_SIZE_MB):
    FINAL_FILE = input_filename + '.RLZSTORE_REF_' + str(REF_SIZE_MB) + 'MB'
    if file_with_reference_exits(FINAL_FILE, input_filename, REF_SIZE_MB):
        return
    
    COL_FOLDER = working_folder+"/RLZ_STORE_REF_" + str(REF_SIZE_MB)
    logfile_name = working_folder + "/log_rlz_store.txt"
    command_pre = "./ext/rlz-store/build/create-collection.x -c " + COL_FOLDER + "  -i " + input_filename + " > " + logfile_name + " 2>&1 "
    run_with_timeout(command_pre, -99)
    command_store = "./ext/rlz-store/build/rlzs-create.x -c " + COL_FOLDER + "  -s " + str(REF_SIZE_MB)  + " >> " + logfile_name + " 2>&1 "
    run_with_timeout(command_store, -99)
    
    REF_FILE = COL_FOLDER + '/index/dict_uniform_sample_budget-1024-' + str(REF_SIZE_MB) + '.sdsl'
    command_create_file_part1 = 'cp ' + REF_FILE + ' ' + FINAL_FILE 
    run_with_timeout(command_create_file_part1, -99)
    command_create_file_part2 = 'cat ' + input_filename + ' >> ' + FINAL_FILE
    run_with_timeout(command_create_file_part2, -99)

def create_reference_random(input_filename, working_folder, REF_SIZE_MB):
    FINAL_FILE = input_filename + '.RANDOMSAMPLING_REF_' + str(REF_SIZE_MB) + 'MB'
    if file_with_reference_exits(FINAL_FILE, input_filename, REF_SIZE_MB):
        return

    block_len = 1024
    n_blocks = REF_SIZE_MB*1024
    
    CURR_FOLDER = working_folder+"/RANDOMSAMPLE_REF_" + str(REF_SIZE_MB)
    ensure_dir(CURR_FOLDER)
    logfile_name = CURR_FOLDER + "/log_randomsample.txt"
    REF_FILE = CURR_FOLDER + '/random_sample_ref-1024-' + str(REF_SIZE_MB) + '.plain'
    ## build dict.
    build_command = './ext/artificial_reference/do_dict/build_dictionary ' + input_filename + ' ' + str(n_blocks) + ' ' + str(block_len)  + ' ' + REF_FILE + ' 42  > ' + logfile_name + ' 2>&1 '
    run_with_timeout(build_command, -99)

    command_create_file_part1 = 'cp ' + REF_FILE + ' ' + FINAL_FILE 
    run_with_timeout(command_create_file_part1, -99)
    command_create_file_part2 = 'cat ' + input_filename + ' >> ' + FINAL_FILE
    run_with_timeout(command_create_file_part2, -99)

def file_with_reference_exits(FINAL_FILE, input_filename, REF_SIZE_MB):
    if os.path.isfile(FINAL_FILE):
        input_size_mb = os.path.getsize(input_filename)/(1024*1024.0)
        final_file_mb = os.path.getsize(FINAL_FILE)/(1024*1024.0)
        if abs(input_size_mb + REF_SIZE_MB - final_file_mb) < 0.01 :
            print "File with reference already exists. Skipping..."
            return True
        else:
            print "File with reference exists but the size is wrong"
            return False

