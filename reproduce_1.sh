#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail 

DATA_DIR="./data"
for FILEPATH in ${DATA_DIR}/*; do
	FILENAME=$(basename -- "${FILEPATH}")
	if [[ ${FILENAME} == *.* ]]; then
		continue
	fi
	if [[ -d ${FILEPATH} ]]; then
		continue
	fi
	echo ${FILENAME}
	TARGET_DIR="./RESULTS_SMALL_REF/${FILENAME}"
	#rm -rf ${TARGET_DIR}; 
  ./experiment_smallrefs.py ${FILEPATH} ${TARGET_DIR}
done
