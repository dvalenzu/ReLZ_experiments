
--- computing reference lenght based on memory limit:
--- With 4000MB of  memory, the reference length will be: 419430399 symbols
RLZ parser intitialized with:
Ignore     : 0
Ref        : 419430399
Partitions : 3
Memory(MB) : 4000
RLZ parsing...
Building SA...
We will read blocks of size: 536870912
Reading block 0/1
Block read in 0.152823 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
Reading block 1/1
Block read in 0.037546 (s)
Waiting for all tasks to be done...
Done.

*
Starting parallel parsing in block...Parallel parse in 3.20786 (s)
Writing phrases... 
Phrases written  in 0.001344 (s)
Input succesfully compressed, output written in: RESULTS_SCALABILITY/CEREHR//536870912/RLZPP_MEM4000_THREADS_1_D_0_files/compressed_file
Parsing performed in 1 passes

0-th level report: 

Factors skipped      : 0
Factors 77'ed in ref : 1658533
Factors RLZ          : 62258
Factors Total        : 1720791
--------------------------------------------
Time SA          : 40.09
Time LZ77 of ref : 10.1309
Time RLZ parse   : 3.24684
-------------------------------
Time RLZ total   : 53.4677


Final Wtime(s)    : 53.525
N Phrases         : 1720791
Compression ratio : 0.0157635
seconds per MiB   : 0.104541
seconds per GiB   : 107.05
MiB per second    : 9.56562
GiB per second    : 0.00934142
Input filename = /home/work/dvalenzu/Repos/RLZPP_experiments/RESULTS_SCALABILITY/CEREHR/536870912/RLZPP_MEM4000_THREADS_1_D_0_files/compressed_file
Output filename = /home/work/dvalenzu/Repos/RLZPP_experiments/RESULTS_SCALABILITY/CEREHR/536870912/RLZPP_MEM4000_THREADS_1_D_0_files/compressed_file.decompressed
Input size = 8462954 (8.1MiB)
Max disk use = 1152921504606846976 (1099511627776.0MiB)
RAM use = 3758096384 (3584.0MiB)
RAM use (excl. buffer) = 3724541952 (3552.0MiB)
Max segment size = 3724541952 (3552.0MiB)

Process part 1:
  Total parsing data decoded so far: 0.00%
  Total text decoded so far: 0 (0.00MiB)
  Permute phrases by source:   Permute phrases by source: time = 0.06s, I/O = 173.34MiB/s  Permute phrases by source: 100.0%, time = 0.08s, I/O = 214.57MiB/s
  Processing this part will decode 512.0MiB of text (100.00% of parsing)
  Last part = TRUE
  Decode segment 1/1 [0..536870912):
    Selfdecode segment:     Selfdecode segment: time = 0.04s, I/O = 126.65MiB/s    Selfdecode segment: time = 0.26s, I/O = 37.78MiB/s
    Write segment to disk: 0.20s, I/O = 2566.07MiB/s
    Decode the nearby phrases for next segment: 0.00s


Computation finished. Summary:
  I/O volume = 565704818 (1.05B/B)
  number of decoded phrases: 1720791
  average phrase length = 311.99
  length of decoded text: 536870912 (512.00MiB)
  elapsed time: 0.56s (0.0011s/MiB)
  speed: 916.18MiB/s
Decompression t(s)    : 0.572331905365
