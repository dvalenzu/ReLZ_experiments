
--- computing reference lenght based on memory limit:
--- With 4000MB of  memory, the reference length will be: 419430399 symbols
RLZ parser intitialized with:
Ignore     : 0
Ref        : 419430399
Partitions : 3
Memory(MB) : 4000
RLZ parsing...
Building SA...
We will read blocks of size: 734003201
Reading block 0/23
Block read in 4.55038 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
Reading block 1/23
Block read in 7.64667 (s)
Waiting for all tasks to be done...
Done.
Reading block 2/23
Block read in 7.83544 (s)

*
Starting parallel parsing in block...Parallel parse in 12.0439 (s)
Writing phrases... 
Phrases written  in 0.005691 (s)
Waiting for all tasks to be done...
Done.
Reading block 3/23
Block read in 7.8713 (s)

*
Starting parallel parsing in block...Parallel parse in 16.942 (s)
Writing phrases... 
Phrases written  in 0.00941 (s)
Waiting for all tasks to be done...
Done.
Reading block 4/23
Block read in 7.60952 (s)

*
Starting parallel parsing in block...Parallel parse in 21.7124 (s)
Writing phrases... 
Phrases written  in 0.012472 (s)
Waiting for all tasks to be done...
Done.
Reading block 5/23
Block read in 7.71448 (s)

*
Starting parallel parsing in block...Parallel parse in 27.7618 (s)
Writing phrases... 
Phrases written  in 0.018182 (s)
Waiting for all tasks to be done...
Done.
Reading block 6/23
Block read in 7.69281 (s)

*
Starting parallel parsing in block...Parallel parse in 31.3178 (s)
Writing phrases... 
Phrases written  in 0.02057 (s)
Waiting for all tasks to be done...
Done.
Reading block 7/23
Block read in 7.66249 (s)

*
Starting parallel parsing in block...Parallel parse in 37.8518 (s)
Writing phrases... 
Phrases written  in 0.025847 (s)
Waiting for all tasks to be done...
Done.
Reading block 8/23
Block read in 7.69298 (s)

*
Starting parallel parsing in block...Parallel parse in 41.0523 (s)
Writing phrases... 
Phrases written  in 0.029331 (s)
Waiting for all tasks to be done...
Done.
Reading block 9/23
Block read in 7.74673 (s)

*
Starting parallel parsing in block...Parallel parse in 46.1243 (s)
Writing phrases... 
Phrases written  in 0.033705 (s)
Waiting for all tasks to be done...
Done.
Reading block 10/23
Block read in 7.77571 (s)

*
Starting parallel parsing in block...Parallel parse in 49.7574 (s)
Writing phrases... 
Phrases written  in 0.037127 (s)
Waiting for all tasks to be done...
Done.
Reading block 11/23
Block read in 7.96733 (s)

*
Starting parallel parsing in block...Parallel parse in 53.4686 (s)
Writing phrases... 
Phrases written  in 0.041201 (s)
Waiting for all tasks to be done...
Done.
Reading block 12/23
Block read in 7.8004 (s)

*
Starting parallel parsing in block...Parallel parse in 59.0044 (s)
Writing phrases... 
Phrases written  in 0.046126 (s)
Waiting for all tasks to be done...
Done.
Reading block 13/23
Block read in 7.77741 (s)

*
Starting parallel parsing in block...Parallel parse in 61.3994 (s)
Writing phrases... 
Phrases written  in 0.048988 (s)
Waiting for all tasks to be done...
Done.
Reading block 14/23
Block read in 7.64223 (s)

*
Starting parallel parsing in block...Parallel parse in 67.1438 (s)
Writing phrases... 
Phrases written  in 0.054507 (s)
Waiting for all tasks to be done...
Done.
Reading block 15/23
Block read in 7.7384 (s)

*
Starting parallel parsing in block...Parallel parse in 69.4187 (s)
Writing phrases... 
Phrases written  in 0.057625 (s)
Waiting for all tasks to be done...
Done.
Reading block 16/23
Block read in 7.62176 (s)

*
Starting parallel parsing in block...Parallel parse in 73.9253 (s)
Writing phrases... 
Phrases written  in 0.059603 (s)
Waiting for all tasks to be done...
Done.
Reading block 17/23
Block read in 7.85604 (s)

*
Starting parallel parsing in block...Parallel parse in 78.0982 (s)
Writing phrases... 
Phrases written  in 0.067255 (s)
Waiting for all tasks to be done...
Done.
Reading block 18/23
Block read in 7.69456 (s)

*
Starting parallel parsing in block...Parallel parse in 80.2528 (s)
Writing phrases... 
Phrases written  in 0.066005 (s)
Waiting for all tasks to be done...
Done.
Reading block 19/23
Block read in 7.76395 (s)

*
Starting parallel parsing in block...Parallel parse in 86.665 (s)
Writing phrases... 
Phrases written  in 0.073256 (s)
Waiting for all tasks to be done...
Done.
Reading block 20/23
Block read in 7.72756 (s)

*
Starting parallel parsing in block...Parallel parse in 87.9537 (s)
Writing phrases... 
Phrases written  in 0.074579 (s)
Waiting for all tasks to be done...
Done.
Reading block 21/23
Block read in 7.78933 (s)

*
Starting parallel parsing in block...Parallel parse in 93.2811 (s)
Writing phrases... 
Phrases written  in 0.079058 (s)
Waiting for all tasks to be done...
Done.
Reading block 22/23
Block read in 7.78032 (s)

*
Starting parallel parsing in block...Parallel parse in 96.1495 (s)
Writing phrases... 
Phrases written  in 0.086713 (s)
Waiting for all tasks to be done...
Done.
Reading block 23/23
Block read in 6.55779 (s)

*
Starting parallel parsing in block...Parallel parse in 98.5786 (s)
Writing phrases... 
Phrases written  in 0.084159 (s)
Waiting for all tasks to be done...
Done.

*
Starting parallel parsing in block...Parallel parse in 86.9029 (s)
Writing phrases... 
Phrases written  in 0.073761 (s)
Packing phrases...
Recursive call to next depth...

--- computing reference lenght based on memory limit:
--- With 4000MB of  memory, the reference length will be: 157286399 symbols

--- reference_len is longer than the text are to be parsed: adjusting it.RLZ parser intitialized with:
Ignore     : 1658533
Ref        : 58788862
Partitions : 3
Memory(MB) : 4000

Starting recursion depth 1
RLZ Parsing...
Building SA...
We will read blocks of size: 60447395
Reading block 0/0
Block read in 0.142247 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
ReParsing...
Safe bound for bits per factor: 18
IM Random Access to sums file will use at most (MB): 129
 Random Access to Sum Files IM (delta compressed)RESULTS_SCALABILITY/CEREHR//17179869184/RLZPP_MEM4000_THREADS_1_D_99_files/compressed_file.TMP.rlztmp.sums
Input succesfully compressed, output written in: RESULTS_SCALABILITY/CEREHR//17179869184/RLZPP_MEM4000_THREADS_1_D_99_files/compressed_file
Parsing performed in 2 passes

0-th level report: 

Factors skipped      : 0
Factors 77'ed in ref : 1658533
Factors RLZ          : 58788862
Factors Total        : 60447395
--------------------------------------------
Time SA          : 44.4256
Time LZ77 of ref : 10.2401
Time RLZ parse   : 1554.93
-------------------------------
Time RLZ total   : 1609.6
--------------------------------------
Rlz TOTAL time 	: 1609.7
Pack      time 	: 3.28071
Recursion time 	: 59.8431
ReParse   time 	: 3.64305
Encode    time 	: 0
--------------------------------------
TOTAL     time 	: 1676.46

1-th level report: 

Factors skipped      : 1658533
Factors 77'ed in ref : 6223429
Factors RLZ          : 0
Factors Total        : 7881962
--------------------------------------------
Time SA          : 57.0062
Time LZ77 of ref : 2.77406
Time RLZ parse   : 1.00001e-06
-------------------------------
Time RLZ total   : 59.7802


Final Wtime(s)    : 1676.46
N Phrases         : 7881962
Compression ratio : 0.00270137
seconds per MiB   : 0.102323
seconds per GiB   : 104.779
MiB per second    : 9.77296
GiB per second    : 0.00954391
Input filename = /home/work/dvalenzu/Repos/RLZPP_experiments/RESULTS_SCALABILITY/CEREHR/17179869184/RLZPP_MEM4000_THREADS_1_D_99_files/compressed_file
Output filename = /home/work/dvalenzu/Repos/RLZPP_experiments/RESULTS_SCALABILITY/CEREHR/17179869184/RLZPP_MEM4000_THREADS_1_D_99_files/compressed_file.decompressed
Input size = 46409150 (44.3MiB)
Max disk use = 1152921504606846976 (1099511627776.0MiB)
RAM use = 3758096384 (3584.0MiB)
RAM use (excl. buffer) = 3724541952 (3552.0MiB)
Max segment size = 3724541952 (3552.0MiB)

Process part 1:
  Total parsing data decoded so far: 0.00%
  Total text decoded so far: 0 (0.00MiB)
  Permute phrases by source:   Permute phrases by source: time = 0.05s, I/O = 223.99MiB/s  Permute phrases by source: time = 0.09s, I/O = 262.71MiB/s  Permute phrases by source: time = 0.13s, I/O = 282.45MiB/s  Permute phrases by source: time = 0.17s, I/O = 295.74MiB/s  Permute phrases by source: time = 0.21s, I/O = 302.92MiB/s  Permute phrases by source: time = 0.25s, I/O = 307.08MiB/s  Permute phrases by source: time = 0.29s, I/O = 309.67MiB/s  Permute phrases by source: 100.0%, time = 0.32s, I/O = 310.47MiB/s
  Processing this part will decode 16384.0MiB of text (100.00% of parsing)
  Last part = TRUE
  Decode segment 1/5 [0..3724541952):
    Selfdecode segment:     Selfdecode segment: time = 0.04s, I/O = 125.85MiB/s    Selfdecode segment: time = 0.67s, I/O = 18.15MiB/s    Selfdecode segment: time = 1.28s, I/O = 13.08MiB/s
    Distribute chunks:     Distribute chunks: time = 0.26s, I/O = 1976.46MiB/s    Distribute chunks: time = 0.48s, I/O = 1849.69MiB/s    Distribute chunks: time = 0.66s, I/O = 1748.48MiB/s    Distribute chunks: time = 0.92s, I/O = 1445.50MiB/s
    Write segment to disk: 39.87s, I/O = 89.09MiB/s
    Decode the nearby phrases for next segment: 0.11s
  Decode segment 2/5 [3724541952..7449083904):
    Load segment chunks:     Load segment chunks: 1.9%, time = 0.00s, I/O = 3017.50MiB/s    Load segment chunks: 4.1%, time = 0.01s, I/O = 3443.42MiB/s    Load segment chunks: 4.9%, time = 0.01s, I/O = 2777.48MiB/s    Load segment chunks: 5.6%, time = 0.01s, I/O = 2424.05MiB/s    Load segment chunks: 6.3%, time = 0.02s, I/O = 2216.14MiB/s    Load segment chunks: 7.1%, time = 0.02s, I/O = 2084.13MiB/s    Load segment chunks: 7.8%, time = 0.02s, I/O = 1989.48MiB/s    Load segment chunks: 8.6%, time = 0.02s, I/O = 1916.67MiB/s    Load segment chunks: 9.3%, time = 0.03s, I/O = 1860.95MiB/s    Load segment chunks: 10.1%, time = 0.03s, I/O = 1814.52MiB/s    Load segment chunks: 10.8%, time = 0.03s, I/O = 1776.96MiB/s    Load segment chunks: 11.6%, time = 0.04s, I/O = 1744.22MiB/s    Load segment chunks: 12.3%, time = 0.04s, I/O = 1718.44MiB/s    Load segment chunks: 13.1%, time = 0.04s, I/O = 1696.35MiB/s    Load segment chunks: 13.8%, time = 0.04s, I/O = 1676.48MiB/s    Load segment chunks: 14.9%, time = 0.05s, I/O = 1701.62MiB/s    Load segment chunks: 15.7%, time = 0.05s, I/O = 1683.91MiB/s    Load segment chunks: 16.4%, time = 0.05s, I/O = 1667.58MiB/s    Load segment chunks: 17.2%, time = 0.06s, I/O = 1653.75MiB/s    Load segment chunks: 17.9%, time = 0.06s, I/O = 1641.53MiB/s    Load segment chunks: 18.7%, time = 0.06s, I/O = 1629.41MiB/s    Load segment chunks: 19.4%, time = 0.06s, I/O = 1619.86MiB/s    Load segment chunks: 20.2%, time = 0.07s, I/O = 1610.95MiB/s    Load segment chunks: 20.9%, time = 0.07s, I/O = 1602.86MiB/s    Load segment chunks: 21.7%, time = 0.07s, I/O = 1595.38MiB/s    Load segment chunks: 22.4%, time = 0.08s, I/O = 1587.41MiB/s    Load segment chunks: 23.1%, time = 0.08s, I/O = 1580.95MiB/s    Load segment chunks: 23.9%, time = 0.08s, I/O = 1573.91MiB/s    Load segment chunks: 24.6%, time = 0.08s, I/O = 1567.01MiB/s    Load segment chunks: 25.4%, time = 0.09s, I/O = 1561.39MiB/s    Load segment chunks: 26.1%, time = 0.09s, I/O = 1556.06MiB/s    Load segment chunks: 26.9%, time = 0.09s, I/O = 1551.19MiB/s    Load segment chunks: 27.6%, time = 0.10s, I/O = 1546.37MiB/s    Load segment chunks: 28.4%, time = 0.10s, I/O = 1541.58MiB/s    Load segment chunks: 29.1%, time = 0.10s, I/O = 1537.37MiB/s    Load segment chunks: 29.9%, time = 0.10s, I/O = 1533.52MiB/s    Load segment chunks: 30.6%, time = 0.11s, I/O = 1529.48MiB/s    Load segment chunks: 31.4%, time = 0.11s, I/O = 1526.14MiB/s    Load segment chunks: 32.1%, time = 0.11s, I/O = 1522.69MiB/s    Load segment chunks: 32.9%, time = 0.12s, I/O = 1519.48MiB/s    Load segment chunks: 33.6%, time = 0.12s, I/O = 1516.58MiB/s    Load segment chunks: 34.3%, time = 0.12s, I/O = 1513.61MiB/s    Load segment chunks: 35.1%, time = 0.12s, I/O = 1510.67MiB/s    Load segment chunks: 35.8%, time = 0.13s, I/O = 1507.93MiB/s    Load segment chunks: 36.6%, time = 0.13s, I/O = 1505.75MiB/s    Load segment chunks: 37.3%, time = 0.13s, I/O = 1502.91MiB/s    Load segment chunks: 38.1%, time = 0.14s, I/O = 1500.81MiB/s    Load segment chunks: 38.8%, time = 0.14s, I/O = 1498.73MiB/s    Load segment chunks: 39.6%, time = 0.14s, I/O = 1495.97MiB/s    Load segment chunks: 40.3%, time = 0.14s, I/O = 1493.79MiB/s    Load segment chunks: 41.1%, time = 0.15s, I/O = 1491.51MiB/s    Load segment chunks: 41.8%, time = 0.15s, I/O = 1489.54MiB/s    Load segment chunks: 42.6%, time = 0.15s, I/O = 1487.55MiB/s    Load segment chunks: 43.3%, time = 0.16s, I/O = 1485.58MiB/s    Load segment chunks: 44.1%, time = 0.16s, I/O = 1483.72MiB/s    Load segment chunks: 44.8%, time = 0.16s, I/O = 1481.67MiB/s    Load segment chunks: 45.5%, time = 0.16s, I/O = 1480.01MiB/s    Load segment chunks: 46.3%, time = 0.17s, I/O = 1478.37MiB/s    Load segment chunks: 47.0%, time = 0.17s, I/O = 1476.74MiB/s    Load segment chunks: 47.8%, time = 0.17s, I/O = 1475.00MiB/s    Load segment chunks: 48.9%, time = 0.18s, I/O = 1484.72MiB/s    Load segment chunks: 49.3%, time = 0.18s, I/O = 1472.09MiB/s    Load segment chunks: 50.4%, time = 0.18s, I/O = 1481.84MiB/s    Load segment chunks: 51.1%, time = 0.19s, I/O = 1480.65MiB/s    Load segment chunks: 51.9%, time = 0.19s, I/O = 1479.49MiB/s    Load segment chunks: 52.6%, time = 0.19s, I/O = 1478.29MiB/s    Load segment chunks: 53.4%, time = 0.19s, I/O = 1476.87MiB/s    Load segment chunks: 54.1%, time = 0.20s, I/O = 1475.14MiB/s    Load segment chunks: 54.9%, time = 0.20s, I/O = 1473.89MiB/s    Load segment chunks: 55.6%, time = 0.20s, I/O = 1472.55MiB/s    Load segment chunks: 56.4%, time = 0.21s, I/O = 1471.30MiB/s    Load segment chunks: 57.1%, time = 0.21s, I/O = 1470.12MiB/s    Load segment chunks: 57.9%, time = 0.21s, I/O = 1468.92MiB/s    Load segment chunks: 58.6%, time = 0.21s, I/O = 1467.74MiB/s    Load segment chunks: 59.4%, time = 0.22s, I/O = 1466.59MiB/s    Load segment chunks: 60.1%, time = 0.22s, I/O = 1465.37MiB/s    Load segment chunks: 60.9%, time = 0.22s, I/O = 1464.32MiB/s    Load segment chunks: 61.6%, time = 0.23s, I/O = 1463.26MiB/s    Load segment chunks: 62.3%, time = 0.23s, I/O = 1462.03MiB/s    Load segment chunks: 63.1%, time = 0.23s, I/O = 1461.06MiB/s    Load segment chunks: 63.8%, time = 0.23s, I/O = 1460.27MiB/s    Load segment chunks: 64.6%, time = 0.24s, I/O = 1459.47MiB/s    Load segment chunks: 65.3%, time = 0.24s, I/O = 1458.68MiB/s    Load segment chunks: 66.1%, time = 0.24s, I/O = 1457.82MiB/s    Load segment chunks: 66.8%, time = 0.25s, I/O = 1456.76MiB/s    Load segment chunks: 67.6%, time = 0.25s, I/O = 1455.83MiB/s    Load segment chunks: 68.3%, time = 0.25s, I/O = 1454.56MiB/s    Load segment chunks: 69.1%, time = 0.25s, I/O = 1453.69MiB/s    Load segment chunks: 69.8%, time = 0.26s, I/O = 1452.82MiB/s    Load segment chunks: 70.6%, time = 0.26s, I/O = 1451.85MiB/s    Load segment chunks: 71.3%, time = 0.26s, I/O = 1450.85MiB/s    Load segment chunks: 72.1%, time = 0.27s, I/O = 1450.08MiB/s    Load segment chunks: 72.8%, time = 0.27s, I/O = 1449.07MiB/s    Load segment chunks: 73.5%, time = 0.27s, I/O = 1448.22MiB/s    Load segment chunks: 74.3%, time = 0.27s, I/O = 1447.41MiB/s    Load segment chunks: 75.0%, time = 0.28s, I/O = 1446.09MiB/s    Load segment chunks: 75.8%, time = 0.28s, I/O = 1445.46MiB/s    Load segment chunks: 76.5%, time = 0.28s, I/O = 1444.85MiB/s    Load segment chunks: 77.3%, time = 0.29s, I/O = 1444.28MiB/s    Load segment chunks: 78.0%, time = 0.29s, I/O = 1443.65MiB/s    Load segment chunks: 78.8%, time = 0.29s, I/O = 1442.65MiB/s    Load segment chunks: 79.9%, time = 0.30s, I/O = 1448.63MiB/s    Load segment chunks: 80.6%, time = 0.30s, I/O = 1447.72MiB/s    Load segment chunks: 81.4%, time = 0.30s, I/O = 1446.84MiB/s    Load segment chunks: 82.1%, time = 0.30s, I/O = 1446.14MiB/s    Load segment chunks: 82.9%, time = 0.31s, I/O = 1445.37MiB/s    Load segment chunks: 83.6%, time = 0.31s, I/O = 1444.71MiB/s    Load segment chunks: 84.4%, time = 0.31s, I/O = 1443.96MiB/s    Load segment chunks: 85.1%, time = 0.32s, I/O = 1443.16MiB/s    Load segment chunks: 85.9%, time = 0.32s, I/O = 1442.49MiB/s    Load segment chunks: 86.6%, time = 0.32s, I/O = 1441.83MiB/s    Load segment chunks: 87.4%, time = 0.32s, I/O = 1441.10MiB/s    Load segment chunks: 88.1%, time = 0.33s, I/O = 1440.60MiB/s    Load segment chunks: 88.9%, time = 0.33s, I/O = 1440.11MiB/s    Load segment chunks: 89.6%, time = 0.33s, I/O = 1439.66MiB/s    Load segment chunks: 90.3%, time = 0.34s, I/O = 1438.97MiB/s    Load segment chunks: 91.1%, time = 0.34s, I/O = 1438.18MiB/s    Load segment chunks: 91.8%, time = 0.34s, I/O = 1437.57MiB/s    Load segment chunks: 92.6%, time = 0.35s, I/O = 1436.98MiB/s    Load segment chunks: 93.3%, time = 0.35s, I/O = 1436.25MiB/s    Load segment chunks: 94.1%, time = 0.35s, I/O = 1435.65MiB/s    Load segment chunks: 94.8%, time = 0.35s, I/O = 1435.05MiB/s    Load segment chunks: 95.6%, time = 0.36s, I/O = 1434.46MiB/s    Load segment chunks: 96.3%, time = 0.36s, I/O = 1433.88MiB/s    Load segment chunks: 97.1%, time = 0.36s, I/O = 1433.20MiB/s    Load segment chunks: 97.8%, time = 0.37s, I/O = 1432.58MiB/s    Load segment chunks: 98.6%, time = 0.37s, I/O = 1432.19MiB/s    Load segment chunks: 99.3%, time = 0.37s, I/O = 1431.63MiB/s    Load segment chunks: 100.0%, time = 0.37s, I/O = 1430.58MiB/s    Load segment chunks: 100.0%, time = 0.38s, I/O = 1419.81MiB/s    Load segment chunks: 100.0%, time = 0.38s, I/O = 1409.16MiB/s    Load segment chunks: 100.0%, time = 0.38s, I/O = 1398.80MiB/s    Load segment chunks: 100.0%, time = 0.38s, I/O = 1395.35MiB/s
    Selfdecode segment:     Selfdecode segment: time = 0.52s, I/O = 4.59MiB/s
    Distribute chunks:     Distribute chunks: time = 0.00s, I/O = 754.58MiB/s
    Write segment to disk: 40.77s, I/O = 87.12MiB/s
    Decode the nearby phrases for next segment: 0.54s
  Decode segment 3/5 [7449083904..11173625856):
    Load segment chunks:     Load segment chunks: 1.6%, time = 0.19s, I/O = 31.59MiB/s    Load segment chunks: 2.7%, time = 0.24s, I/O = 42.36MiB/s    Load segment chunks: 3.8%, time = 0.28s, I/O = 49.83MiB/s    Load segment chunks: 4.9%, time = 0.33s, I/O = 54.00MiB/s    Load segment chunks: 6.0%, time = 0.38s, I/O = 57.98MiB/s    Load segment chunks: 7.1%, time = 0.43s, I/O = 61.11MiB/s    Load segment chunks: 8.2%, time = 0.47s, I/O = 63.60MiB/s    Load segment chunks: 9.2%, time = 0.53s, I/O = 64.43MiB/s    Load segment chunks: 10.3%, time = 0.58s, I/O = 65.95MiB/s    Load segment chunks: 11.4%, time = 0.63s, I/O = 67.06MiB/s    Load segment chunks: 12.5%, time = 0.67s, I/O = 68.25MiB/s    Load segment chunks: 13.6%, time = 0.72s, I/O = 69.43MiB/s    Load segment chunks: 14.7%, time = 0.77s, I/O = 70.47MiB/s    Load segment chunks: 15.8%, time = 0.82s, I/O = 70.82MiB/s    Load segment chunks: 16.9%, time = 0.87s, I/O = 71.64MiB/s    Load segment chunks: 17.9%, time = 0.91s, I/O = 72.43MiB/s    Load segment chunks: 19.0%, time = 0.96s, I/O = 73.11MiB/s    Load segment chunks: 20.1%, time = 1.00s, I/O = 73.79MiB/s    Load segment chunks: 21.2%, time = 1.05s, I/O = 74.36MiB/s    Load segment chunks: 22.3%, time = 1.14s, I/O = 71.82MiB/s    Load segment chunks: 23.4%, time = 1.27s, I/O = 67.92MiB/s    Load segment chunks: 24.5%, time = 1.31s, I/O = 68.47MiB/s    Load segment chunks: 25.6%, time = 1.36s, I/O = 68.99MiB/s    Load segment chunks: 26.6%, time = 1.41s, I/O = 69.47MiB/s    Load segment chunks: 27.7%, time = 1.46s, I/O = 69.95MiB/s    Load segment chunks: 28.8%, time = 1.51s, I/O = 70.34MiB/s    Load segment chunks: 29.9%, time = 1.56s, I/O = 70.73MiB/s    Load segment chunks: 31.0%, time = 1.61s, I/O = 70.75MiB/s    Load segment chunks: 32.1%, time = 1.66s, I/O = 71.26MiB/s    Load segment chunks: 33.2%, time = 1.71s, I/O = 71.36MiB/s    Load segment chunks: 34.3%, time = 1.75s, I/O = 71.80MiB/s    Load segment chunks: 35.9%, time = 1.82s, I/O = 72.35MiB/s    Load segment chunks: 37.0%, time = 1.87s, I/O = 72.63MiB/s    Load segment chunks: 38.1%, time = 1.92s, I/O = 72.90MiB/s    Load segment chunks: 39.2%, time = 1.97s, I/O = 73.15MiB/s    Load segment chunks: 40.2%, time = 2.02s, I/O = 73.38MiB/s    Load segment chunks: 41.3%, time = 2.06s, I/O = 73.79MiB/s    Load segment chunks: 42.4%, time = 2.24s, I/O = 69.77MiB/s    Load segment chunks: 43.5%, time = 2.28s, I/O = 70.14MiB/s    Load segment chunks: 44.6%, time = 2.33s, I/O = 70.47MiB/s    Load segment chunks: 45.7%, time = 2.37s, I/O = 70.80MiB/s    Load segment chunks: 46.8%, time = 2.42s, I/O = 71.03MiB/s    Load segment chunks: 47.9%, time = 2.46s, I/O = 71.51MiB/s    Load segment chunks: 48.9%, time = 2.52s, I/O = 71.43MiB/s    Load segment chunks: 50.0%, time = 2.57s, I/O = 71.70MiB/s    Load segment chunks: 51.1%, time = 2.61s, I/O = 71.97MiB/s    Load segment chunks: 52.2%, time = 2.65s, I/O = 72.40MiB/s    Load segment chunks: 53.3%, time = 2.72s, I/O = 72.16MiB/s    Load segment chunks: 54.4%, time = 2.76s, I/O = 72.36MiB/s    Load segment chunks: 55.5%, time = 2.81s, I/O = 72.54MiB/s    Load segment chunks: 56.6%, time = 2.86s, I/O = 72.71MiB/s    Load segment chunks: 57.6%, time = 2.91s, I/O = 72.89MiB/s    Load segment chunks: 58.7%, time = 2.96s, I/O = 73.05MiB/s    Load segment chunks: 59.8%, time = 3.01s, I/O = 73.20MiB/s    Load segment chunks: 60.9%, time = 3.05s, I/O = 73.37MiB/s    Load segment chunks: 62.5%, time = 3.29s, I/O = 69.83MiB/s    Load segment chunks: 63.1%, time = 3.30s, I/O = 70.37MiB/s    Load segment chunks: 64.2%, time = 3.44s, I/O = 68.68MiB/s    Load segment chunks: 65.8%, time = 3.50s, I/O = 69.14MiB/s    Load segment chunks: 66.9%, time = 3.55s, I/O = 69.38MiB/s    Load segment chunks: 68.0%, time = 3.59s, I/O = 69.61MiB/s    Load segment chunks: 69.1%, time = 3.64s, I/O = 69.82MiB/s    Load segment chunks: 70.2%, time = 3.68s, I/O = 70.03MiB/s    Load segment chunks: 71.2%, time = 3.73s, I/O = 70.24MiB/s    Load segment chunks: 72.3%, time = 3.78s, I/O = 70.46MiB/s    Load segment chunks: 73.4%, time = 3.82s, I/O = 70.66MiB/s    Load segment chunks: 74.5%, time = 3.88s, I/O = 70.66MiB/s    Load segment chunks: 75.6%, time = 3.93s, I/O = 70.81MiB/s    Load segment chunks: 76.7%, time = 3.97s, I/O = 70.96MiB/s    Load segment chunks: 77.8%, time = 4.02s, I/O = 71.10MiB/s    Load segment chunks: 78.9%, time = 4.07s, I/O = 71.25MiB/s    Load segment chunks: 79.9%, time = 4.12s, I/O = 71.38MiB/s    Load segment chunks: 81.0%, time = 4.17s, I/O = 71.51MiB/s    Load segment chunks: 82.1%, time = 4.21s, I/O = 71.65MiB/s    Load segment chunks: 83.7%, time = 4.44s, I/O = 69.43MiB/s    Load segment chunks: 84.3%, time = 4.45s, I/O = 69.67MiB/s    Load segment chunks: 85.4%, time = 4.57s, I/O = 68.73MiB/s    Load segment chunks: 86.5%, time = 4.62s, I/O = 68.87MiB/s    Load segment chunks: 87.6%, time = 4.67s, I/O = 68.97MiB/s    Load segment chunks: 88.6%, time = 4.71s, I/O = 69.19MiB/s    Load segment chunks: 89.7%, time = 4.76s, I/O = 69.31MiB/s    Load segment chunks: 91.4%, time = 4.82s, I/O = 69.65MiB/s    Load segment chunks: 92.4%, time = 4.87s, I/O = 69.81MiB/s    Load segment chunks: 93.5%, time = 4.92s, I/O = 69.97MiB/s    Load segment chunks: 94.6%, time = 4.96s, I/O = 70.12MiB/s    Load segment chunks: 95.7%, time = 5.01s, I/O = 70.29MiB/s    Load segment chunks: 96.8%, time = 5.05s, I/O = 70.44MiB/s    Load segment chunks: 97.9%, time = 5.09s, I/O = 70.67MiB/s    Load segment chunks: 99.0%, time = 5.15s, I/O = 70.66MiB/s    Load segment chunks: 100.0%, time = 5.19s, I/O = 70.92MiB/s    Load segment chunks: 100.0%, time = 5.19s, I/O = 70.88MiB/s
    Selfdecode segment:     Selfdecode segment: time = 0.56s, I/O = 4.53MiB/s
    Distribute chunks:     Distribute chunks: time = 0.00s, I/O = 928.84MiB/s
    Write segment to disk: 40.24s, I/O = 88.27MiB/s
    Decode the nearby phrases for next segment: 0.09s
  Decode segment 4/5 [11173625856..14898167808):
    Load segment chunks:     Load segment chunks: 2.2%, time = 0.10s, I/O = 57.28MiB/s    Load segment chunks: 3.7%, time = 0.15s, I/O = 65.07MiB/s    Load segment chunks: 5.2%, time = 0.20s, I/O = 69.27MiB/s    Load segment chunks: 6.7%, time = 0.25s, I/O = 70.82MiB/s    Load segment chunks: 8.2%, time = 0.30s, I/O = 73.26MiB/s    Load segment chunks: 9.7%, time = 0.35s, I/O = 75.29MiB/s    Load segment chunks: 11.2%, time = 0.39s, I/O = 76.60MiB/s    Load segment chunks: 12.7%, time = 0.44s, I/O = 77.96MiB/s    Load segment chunks: 14.2%, time = 0.48s, I/O = 78.68MiB/s    Load segment chunks: 15.7%, time = 0.54s, I/O = 78.26MiB/s    Load segment chunks: 17.2%, time = 0.58s, I/O = 78.88MiB/s    Load segment chunks: 18.7%, time = 0.63s, I/O = 79.42MiB/s    Load segment chunks: 20.2%, time = 0.67s, I/O = 80.10MiB/s    Load segment chunks: 21.7%, time = 0.72s, I/O = 80.46MiB/s    Load segment chunks: 23.9%, time = 0.93s, I/O = 68.50MiB/s    Load segment chunks: 24.7%, time = 1.05s, I/O = 62.65MiB/s    Load segment chunks: 26.2%, time = 1.10s, I/O = 63.56MiB/s    Load segment chunks: 27.7%, time = 1.15s, I/O = 64.37MiB/s    Load segment chunks: 29.2%, time = 1.20s, I/O = 65.09MiB/s    Load segment chunks: 30.7%, time = 1.25s, I/O = 65.80MiB/s    Load segment chunks: 32.2%, time = 1.30s, I/O = 66.41MiB/s    Load segment chunks: 34.4%, time = 1.36s, I/O = 67.60MiB/s    Load segment chunks: 35.9%, time = 1.41s, I/O = 68.13MiB/s    Load segment chunks: 37.4%, time = 1.46s, I/O = 68.65MiB/s    Load segment chunks: 38.9%, time = 1.50s, I/O = 69.41MiB/s    Load segment chunks: 40.4%, time = 1.57s, I/O = 68.99MiB/s    Load segment chunks: 41.9%, time = 1.61s, I/O = 69.40MiB/s    Load segment chunks: 43.4%, time = 1.66s, I/O = 69.79MiB/s    Load segment chunks: 44.9%, time = 1.71s, I/O = 70.16MiB/s    Load segment chunks: 46.4%, time = 1.76s, I/O = 70.52MiB/s    Load segment chunks: 47.9%, time = 1.81s, I/O = 70.85MiB/s    Load segment chunks: 49.3%, time = 1.85s, I/O = 71.17MiB/s    Load segment chunks: 50.8%, time = 2.07s, I/O = 65.77MiB/s    Load segment chunks: 52.3%, time = 2.10s, I/O = 66.59MiB/s    Load segment chunks: 53.8%, time = 2.19s, I/O = 65.75MiB/s    Load segment chunks: 55.3%, time = 2.25s, I/O = 65.87MiB/s    Load segment chunks: 56.8%, time = 2.29s, I/O = 66.28MiB/s    Load segment chunks: 58.3%, time = 2.34s, I/O = 66.67MiB/s    Load segment chunks: 59.8%, time = 2.39s, I/O = 67.07MiB/s    Load segment chunks: 61.3%, time = 2.43s, I/O = 67.47MiB/s    Load segment chunks: 62.8%, time = 2.48s, I/O = 67.82MiB/s    Load segment chunks: 65.0%, time = 2.54s, I/O = 68.43MiB/s    Load segment chunks: 66.5%, time = 2.59s, I/O = 68.79MiB/s    Load segment chunks: 68.0%, time = 2.63s, I/O = 69.10MiB/s    Load segment chunks: 69.5%, time = 2.69s, I/O = 69.21MiB/s    Load segment chunks: 71.0%, time = 2.74s, I/O = 69.45MiB/s    Load segment chunks: 72.5%, time = 2.78s, I/O = 69.69MiB/s    Load segment chunks: 74.0%, time = 2.83s, I/O = 69.92MiB/s    Load segment chunks: 75.5%, time = 2.88s, I/O = 70.16MiB/s    Load segment chunks: 77.0%, time = 2.93s, I/O = 70.41MiB/s    Load segment chunks: 78.5%, time = 2.97s, I/O = 70.61MiB/s    Load segment chunks: 80.0%, time = 3.02s, I/O = 70.88MiB/s    Load segment chunks: 82.2%, time = 3.23s, I/O = 68.05MiB/s    Load segment chunks: 83.0%, time = 3.25s, I/O = 68.38MiB/s    Load segment chunks: 84.5%, time = 3.38s, I/O = 66.87MiB/s    Load segment chunks: 86.0%, time = 3.43s, I/O = 67.12MiB/s    Load segment chunks: 87.5%, time = 3.47s, I/O = 67.35MiB/s    Load segment chunks: 89.0%, time = 3.52s, I/O = 67.63MiB/s    Load segment chunks: 90.5%, time = 3.57s, I/O = 67.86MiB/s    Load segment chunks: 92.7%, time = 3.63s, I/O = 68.36MiB/s    Load segment chunks: 94.2%, time = 3.67s, I/O = 68.58MiB/s    Load segment chunks: 95.7%, time = 3.72s, I/O = 68.81MiB/s    Load segment chunks: 97.2%, time = 3.77s, I/O = 69.02MiB/s    Load segment chunks: 98.7%, time = 3.81s, I/O = 69.34MiB/s    Load segment chunks: 100.0%, time = 3.85s, I/O = 69.45MiB/s    Load segment chunks: 100.0%, time = 3.85s, I/O = 69.40MiB/s
    Selfdecode segment:     Selfdecode segment: time = 0.57s, I/O = 4.61MiB/s
    Distribute chunks:     Distribute chunks: time = 0.00s, I/O = 838.25MiB/s
    Write segment to disk: 40.45s, I/O = 87.82MiB/s
    Decode the nearby phrases for next segment: 0.09s
  Decode segment 5/5 [14898167808..17179869184):
    Load segment chunks:     Load segment chunks: 4.4%, time = 0.54s, I/O = 11.16MiB/s    Load segment chunks: 7.4%, time = 0.59s, I/O = 17.05MiB/s    Load segment chunks: 10.3%, time = 0.63s, I/O = 22.06MiB/s    Load segment chunks: 13.3%, time = 0.69s, I/O = 25.95MiB/s    Load segment chunks: 16.2%, time = 0.74s, I/O = 29.73MiB/s    Load segment chunks: 19.2%, time = 0.79s, I/O = 33.12MiB/s    Load segment chunks: 22.1%, time = 0.83s, I/O = 36.08MiB/s    Load segment chunks: 25.1%, time = 0.88s, I/O = 38.72MiB/s    Load segment chunks: 28.0%, time = 0.92s, I/O = 41.12MiB/s    Load segment chunks: 31.0%, time = 0.98s, I/O = 42.98MiB/s    Load segment chunks: 33.9%, time = 1.02s, I/O = 44.96MiB/s    Load segment chunks: 36.9%, time = 1.07s, I/O = 46.80MiB/s    Load segment chunks: 39.8%, time = 1.12s, I/O = 48.37MiB/s    Load segment chunks: 42.7%, time = 1.16s, I/O = 49.88MiB/s    Load segment chunks: 45.7%, time = 1.21s, I/O = 51.25MiB/s    Load segment chunks: 48.6%, time = 1.26s, I/O = 52.57MiB/s    Load segment chunks: 51.6%, time = 1.30s, I/O = 53.82MiB/s    Load segment chunks: 56.0%, time = 1.66s, I/O = 45.81MiB/s    Load segment chunks: 59.0%, time = 1.71s, I/O = 46.86MiB/s    Load segment chunks: 61.9%, time = 1.76s, I/O = 47.84MiB/s    Load segment chunks: 64.9%, time = 1.80s, I/O = 48.82MiB/s    Load segment chunks: 67.8%, time = 1.85s, I/O = 49.76MiB/s    Load segment chunks: 70.8%, time = 1.89s, I/O = 50.69MiB/s    Load segment chunks: 73.7%, time = 1.94s, I/O = 51.54MiB/s    Load segment chunks: 76.6%, time = 1.98s, I/O = 52.51MiB/s    Load segment chunks: 79.6%, time = 2.05s, I/O = 52.81MiB/s    Load segment chunks: 82.5%, time = 2.09s, I/O = 53.50MiB/s    Load segment chunks: 85.5%, time = 2.14s, I/O = 54.16MiB/s    Load segment chunks: 88.4%, time = 2.19s, I/O = 54.79MiB/s    Load segment chunks: 91.4%, time = 2.24s, I/O = 55.40MiB/s    Load segment chunks: 94.3%, time = 2.29s, I/O = 55.97MiB/s    Load segment chunks: 97.3%, time = 2.34s, I/O = 56.53MiB/s    Load segment chunks: 100.0%, time = 2.37s, I/O = 57.34MiB/s
    Selfdecode segment:     Selfdecode segment: time = 0.32s, I/O = 4.59MiB/s
    Write segment to disk: 24.86s, I/O = 87.54MiB/s
    Decode the nearby phrases for next segment: 0.00s


Computation finished. Summary:
  I/O volume = 20080142912 (1.17B/B)
  number of decoded phrases: 7881962
  average phrase length = 2179.64
  length of decoded text: 17179869184 (16384.00MiB)
  elapsed time: 204.86s (0.0125s/MiB)
  speed: 79.98MiB/s
Decompression t(s)    : 204.889205933
