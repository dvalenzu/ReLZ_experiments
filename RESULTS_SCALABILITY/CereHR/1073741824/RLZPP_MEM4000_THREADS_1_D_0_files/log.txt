
--- computing reference lenght based on memory limit:
--- With 4000MB of  memory, the reference length will be: 419430399 symbols
RLZ parser intitialized with:
Ignore     : 0
Ref        : 419430399
Partitions : 3
Memory(MB) : 4000
RLZ parsing...
Building SA...
We will read blocks of size: 734003201
Reading block 0/1
Block read in 0.133064 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
Reading block 1/1
Block read in 0.20317 (s)
Waiting for all tasks to be done...
Done.

*
Starting parallel parsing in block...Parallel parse in 10.7598 (s)
Writing phrases... 
Phrases written  in 0.004872 (s)
Input succesfully compressed, output written in: RESULTS_SCALABILITY/CEREHR//1073741824/RLZPP_MEM4000_THREADS_1_D_0_files/compressed_file
Parsing performed in 1 passes

0-th level report: 

Factors skipped      : 0
Factors 77'ed in ref : 1658533
Factors RLZ          : 245553
Factors Total        : 1904086
--------------------------------------------
Time SA          : 39.9493
Time LZ77 of ref : 10.1706
Time RLZ parse   : 10.968
-------------------------------
Time RLZ total   : 61.0879


Final Wtime(s)    : 61.1569
N Phrases         : 1904086
Compression ratio : 0.00888935
seconds per MiB   : 0.0597236
seconds per GiB   : 61.1569
MiB per second    : 16.7438
GiB per second    : 0.0163514
Input filename = /home/work/dvalenzu/Repos/RLZPP_experiments/RESULTS_SCALABILITY/CEREHR/1073741824/RLZPP_MEM4000_THREADS_1_D_0_files/compressed_file
Output filename = /home/work/dvalenzu/Repos/RLZPP_experiments/RESULTS_SCALABILITY/CEREHR/1073741824/RLZPP_MEM4000_THREADS_1_D_0_files/compressed_file.decompressed
Input size = 9544867 (9.1MiB)
Max disk use = 1152921504606846976 (1099511627776.0MiB)
RAM use = 3758096384 (3584.0MiB)
RAM use (excl. buffer) = 3724541952 (3552.0MiB)
Max segment size = 3724541952 (3552.0MiB)

Process part 1:
  Total parsing data decoded so far: 0.00%
  Total text decoded so far: 0 (0.00MiB)
  Permute phrases by source:   Permute phrases by source: time = 0.04s, I/O = 248.17MiB/s  Permute phrases by source: 100.0%, time = 0.07s, I/O = 273.06MiB/s
  Processing this part will decode 1024.0MiB of text (100.00% of parsing)
  Last part = TRUE
  Decode segment 1/1 [0..1073741824):
    Selfdecode segment:     Selfdecode segment: time = 0.04s, I/O = 127.72MiB/s    Selfdecode segment: time = 0.42s, I/O = 25.75MiB/s
    Write segment to disk: 0.39s, I/O = 2609.60MiB/s
    Decode the nearby phrases for next segment: 0.00s


Computation finished. Summary:
  I/O volume = 1106191661 (1.03B/B)
  number of decoded phrases: 1904086
  average phrase length = 563.91
  length of decoded text: 1073741824 (1024.00MiB)
  elapsed time: 0.92s (0.0009s/MiB)
  speed: 1114.11MiB/s
Decompression t(s)    : 0.922334909439
