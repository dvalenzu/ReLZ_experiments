
--- computing reference lenght based on memory limit:
--- With 10000MB of  memory, the reference length will be: 1048575999 symbols
RLZ parser intitialized with:
Ignore     : 0
Ref        : 1048575999
Partitions : 12
Memory(MB) : 10000
RLZ parsing...
Building SA...
We will read blocks of size: 1073741824
Reading block 0...Block read in 0.32046 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
Reading block 1...Block read in 0.007647 (s)
Waiting for all tasks to be done...
Done.
Starting parallel parsing in block...Parallel parse in 0.14923 (s)
Writing phrases... 
Phrases writed  in 0.000119 (s)
Packing phrases...
Recursive call to next depth...

--- computing reference lenght based on memory limit:
--- With 10000MB of  memory, the reference length will be: 393215999 symbols

--- reference_len is longer than the text are to be parsed: adjusting it.RLZ parser intitialized with:
Ignore     : 1817998
Ref        : 5239
Partitions : 12
Memory(MB) : 10000

Starting recursion depth 1
RLZ Parsing...
Building SA...
We will read blocks of size: 1823237
Reading block 0...Block read in 1.3e-05 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
ReParsing...
Safe bound for bits per factor: 19
IM Random Access to sums file will use at most (MB): 4
 Random Access to Sum Files IM (delta compressed)CEREHR/1073741824/RLZPP_MEM10000_THREADS_4_D_99_files/compressed_file.TMP.rlztmp.sums
Input succesfully compressed, output written in: CEREHR/1073741824/RLZPP_MEM10000_THREADS_4_D_99_files/compressed_file
Parsing performed in 2 passes

0-th level report: 

Factors skipped      : 0
Factors 77'ed in ref : 1817998
Factors RLZ          : 5239
Factors Total        : 1823237
--------------------------------------------
Time SA          : 111.462
Time LZ77 of ref : 32.8556
Time RLZ parse   : 0.157057
-------------------------------
Time RLZ total   : 144.475
--------------------------------------
Rlz TOTAL time 	: 144.605
Pack      time 	: 0.0619
Recursion time 	: 0.045573
ReParse   time 	: 0.118163
Encode    time 	: 0.109205
--------------------------------------
TOTAL     time 	: 144.94

1-th level report: 

Factors skipped      : 1817998
Factors 77'ed in ref : 5239
Factors RLZ          : 0
Factors Total        : 1823237
--------------------------------------------
Time SA          : 0.001105
Time LZ77 of ref : 0.000146
Time RLZ parse   : 9.99891e-07
-------------------------------
Time RLZ total   : 0.001252


Final Wtime(s)    : 144.94
N Phrases         : 1823237
Compression ratio : 0.0271683
seconds per MiB   : 0.141543
seconds per GiB   : 144.94
MiB per second    : 7.065
GiB per second    : 0.00689941
