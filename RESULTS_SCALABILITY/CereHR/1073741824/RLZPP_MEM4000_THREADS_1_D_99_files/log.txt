
--- computing reference lenght based on memory limit:
--- With 4000MB of  memory, the reference length will be: 419430399 symbols
RLZ parser intitialized with:
Ignore     : 0
Ref        : 419430399
Partitions : 3
Memory(MB) : 4000
RLZ parsing...
Building SA...
We will read blocks of size: 734003201
Reading block 0/1
Block read in 0.140781 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
Reading block 1/1
Block read in 0.214615 (s)
Waiting for all tasks to be done...
Done.

*
Starting parallel parsing in block...Parallel parse in 10.7523 (s)
Writing phrases... 
Phrases written  in 0.004873 (s)
Packing phrases...
Recursive call to next depth...

--- computing reference lenght based on memory limit:
--- With 4000MB of  memory, the reference length will be: 157286399 symbols

--- reference_len is longer than the text are to be parsed: adjusting it.RLZ parser intitialized with:
Ignore     : 1658533
Ref        : 245553
Partitions : 3
Memory(MB) : 4000

Starting recursion depth 1
RLZ Parsing...
Building SA...
We will read blocks of size: 1904086
Reading block 0/0
Block read in 0.000684 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
ReParsing...
Safe bound for bits per factor: 19
IM Random Access to sums file will use at most (MB): 4
 Random Access to Sum Files IM (delta compressed)RESULTS_SCALABILITY/CEREHR//1073741824/RLZPP_MEM4000_THREADS_1_D_99_files/compressed_file.TMP.rlztmp.sums
Input succesfully compressed, output written in: RESULTS_SCALABILITY/CEREHR//1073741824/RLZPP_MEM4000_THREADS_1_D_99_files/compressed_file
Parsing performed in 2 passes

0-th level report: 

Factors skipped      : 0
Factors 77'ed in ref : 1658533
Factors RLZ          : 245553
Factors Total        : 1904086
--------------------------------------------
Time SA          : 39.8247
Time LZ77 of ref : 10.1523
Time RLZ parse   : 10.9719
-------------------------------
Time RLZ total   : 60.9489
--------------------------------------
Rlz TOTAL time 	: 61.0176
Pack      time 	: 0.065086
Recursion time 	: 0.128523
ReParse   time 	: 0.127091
Encode    time 	: 0
--------------------------------------
TOTAL     time 	: 61.3383

1-th level report: 

Factors skipped      : 1658533
Factors 77'ed in ref : 187781
Factors RLZ          : 0
Factors Total        : 1846314
--------------------------------------------
Time SA          : 0.079177
Time LZ77 of ref : 0.008084
Time RLZ parse   : 1.00001e-06
-------------------------------
Time RLZ total   : 0.087262


Final Wtime(s)    : 61.3383
N Phrases         : 1846314
Compression ratio : 0.00858173
seconds per MiB   : 0.0599007
seconds per GiB   : 61.3383
MiB per second    : 16.6943
GiB per second    : 0.016303
Input filename = /home/work/dvalenzu/Repos/RLZPP_experiments/RESULTS_SCALABILITY/CEREHR/1073741824/RLZPP_MEM4000_THREADS_1_D_99_files/compressed_file
Output filename = /home/work/dvalenzu/Repos/RLZPP_experiments/RESULTS_SCALABILITY/CEREHR/1073741824/RLZPP_MEM4000_THREADS_1_D_99_files/compressed_file.decompressed
Input size = 9214558 (8.8MiB)
Max disk use = 1152921504606846976 (1099511627776.0MiB)
RAM use = 3758096384 (3584.0MiB)
RAM use (excl. buffer) = 3724541952 (3552.0MiB)
Max segment size = 3724541952 (3552.0MiB)

Process part 1:
  Total parsing data decoded so far: 0.00%
  Total text decoded so far: 0 (0.00MiB)
  Permute phrases by source:   Permute phrases by source: time = 0.04s, I/O = 248.38MiB/s  Permute phrases by source: 100.0%, time = 0.07s, I/O = 272.29MiB/s
  Processing this part will decode 1024.0MiB of text (100.00% of parsing)
  Last part = TRUE
  Decode segment 1/1 [0..1073741824):
    Selfdecode segment:     Selfdecode segment: time = 0.04s, I/O = 129.02MiB/s    Selfdecode segment: time = 0.42s, I/O = 25.33MiB/s
    Write segment to disk: 0.39s, I/O = 2595.51MiB/s
    Decode the nearby phrases for next segment: 0.00s


Computation finished. Summary:
  I/O volume = 1105085240 (1.03B/B)
  number of decoded phrases: 1846314
  average phrase length = 581.56
  length of decoded text: 1073741824 (1024.00MiB)
  elapsed time: 0.91s (0.0009s/MiB)
  speed: 1124.35MiB/s
Decompression t(s)    : 0.913902997971
