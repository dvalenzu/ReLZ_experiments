
--- computing reference lenght based on memory limit:
--- With 4000MB of  memory, the reference length will be: 419430399 symbols
RLZ parser intitialized with:
Ignore     : 0
Ref        : 419430399
Partitions : 12
Memory(MB) : 4000
RLZ parsing...
Building SA...
We will read blocks of size: 734003201
Reading block 0/12
Block read in 0.274076 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
Reading block 1/12
Block read in 7.5507 (s)
Waiting for all tasks to be done...
Done.
Reading block 2/12
Block read in 7.40057 (s)

*
Starting parallel parsing in block...Parallel parse in 5.95058 (s)
Writing phrases... 
Phrases written  in 0.006692 (s)
Waiting for all tasks to be done...
Done.
Reading block 3/12
Block read in 7.2544 (s)

*
Starting parallel parsing in block...Parallel parse in 8.4821 (s)
Writing phrases... 
Phrases written  in 0.010545 (s)
Waiting for all tasks to be done...
Done.
Reading block 4/12
Block read in 7.39198 (s)

*
Starting parallel parsing in block...Parallel parse in 11.1895 (s)
Writing phrases... 
Phrases written  in 0.015301 (s)
Waiting for all tasks to be done...
Done.
Reading block 5/12
Block read in 7.30369 (s)

*
Starting parallel parsing in block...Parallel parse in 14.3722 (s)
Writing phrases... 
Phrases written  in 0.019669 (s)
Waiting for all tasks to be done...
Done.
Reading block 6/12
Block read in 7.3112 (s)

*
Starting parallel parsing in block...Parallel parse in 15.5395 (s)
Writing phrases... 
Phrases written  in 0.021946 (s)
Waiting for all tasks to be done...
Done.
Reading block 7/12
Block read in 7.34149 (s)

*
Starting parallel parsing in block...Parallel parse in 19.0524 (s)
Writing phrases... 
Phrases written  in 0.02814 (s)
Waiting for all tasks to be done...
Done.
Reading block 8/12
Block read in 5.3406 (s)

*
Starting parallel parsing in block...Parallel parse in 20.962 (s)
Writing phrases... 
Phrases written  in 0.031392 (s)
Waiting for all tasks to be done...
Done.
Reading block 9/12
Block read in 6.77532 (s)

*
Starting parallel parsing in block...Parallel parse in 23.6063 (s)
Writing phrases... 
Phrases written  in 0.036458 (s)
Waiting for all tasks to be done...
Done.
Reading block 10/12
Block read in 6.01699 (s)

*
Starting parallel parsing in block...Parallel parse in 25.8303 (s)
Writing phrases... 
Phrases written  in 0.04003 (s)
Waiting for all tasks to be done...
Done.
Reading block 11/12
Block read in 1.45826 (s)

*
Starting parallel parsing in block...Parallel parse in 27.7277 (s)
Writing phrases... 
Phrases written  in 0.044255 (s)
Waiting for all tasks to be done...
Done.
Reading block 12/12
Block read in 0.016846 (s)

*
Starting parallel parsing in block...Parallel parse in 30.7563 (s)
Writing phrases... 
Phrases written  in 0.049588 (s)
Waiting for all tasks to be done...
Done.

*
Starting parallel parsing in block...Parallel parse in 4.17685 (s)
Writing phrases... 
Phrases written  in 0.007147 (s)
Packing phrases...
Recursive call to next depth...

--- computing reference lenght based on memory limit:
--- With 4000MB of  memory, the reference length will be: 157286399 symbols

--- reference_len is longer than the text are to be parsed: adjusting it.RLZ parser intitialized with:
Ignore     : 1658533
Ref        : 15105627
Partitions : 12
Memory(MB) : 4000

Starting recursion depth 1
RLZ Parsing...
Building SA...
We will read blocks of size: 16764160
Reading block 0/0
Block read in 0.040692 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
ReParsing...
Safe bound for bits per factor: 19
IM Random Access to sums file will use at most (MB): 37
 Random Access to Sum Files IM (delta compressed)RESULTS_SCALABILITY/CEREHR//8589934592/RLZPP_MEM4000_THREADS_4_D_99_files/compressed_file.TMP.rlztmp.sums
Input succesfully compressed, output written in: RESULTS_SCALABILITY/CEREHR//8589934592/RLZPP_MEM4000_THREADS_4_D_99_files/compressed_file
Parsing performed in 2 passes

0-th level report: 

Factors skipped      : 0
Factors 77'ed in ref : 1658533
Factors RLZ          : 15105627
Factors Total        : 16764160
--------------------------------------------
Time SA          : 40.0596
Time LZ77 of ref : 10.3277
Time RLZ parse   : 279.15
-------------------------------
Time RLZ total   : 329.537
--------------------------------------
Rlz TOTAL time 	: 329.639
Pack      time 	: 0.606235
Recursion time 	: 13.2053
ReParse   time 	: 1.12645
Encode    time 	: 0
--------------------------------------
TOTAL     time 	: 344.577

1-th level report: 

Factors skipped      : 1658533
Factors 77'ed in ref : 2934397
Factors RLZ          : 0
Factors Total        : 4592930
--------------------------------------------
Time SA          : 12.3682
Time LZ77 of ref : 0.788448
Time RLZ parse   : 1.00001e-06
-------------------------------
Time RLZ total   : 13.1567


Final Wtime(s)    : 344.577
N Phrases         : 4592930
Compression ratio : 0.00304691
seconds per MiB   : 0.0420626
seconds per GiB   : 43.0721
MiB per second    : 23.7741
GiB per second    : 0.0232169
Input filename = /home/work/dvalenzu/Repos/RLZPP_experiments/RESULTS_SCALABILITY/CEREHR/8589934592/RLZPP_MEM4000_THREADS_4_D_99_files/compressed_file
Output filename = /home/work/dvalenzu/Repos/RLZPP_experiments/RESULTS_SCALABILITY/CEREHR/8589934592/RLZPP_MEM4000_THREADS_4_D_99_files/compressed_file.decompressed
Input size = 26172770 (25.0MiB)
Max disk use = 1152921504606846976 (1099511627776.0MiB)
RAM use = 3758096384 (3584.0MiB)
RAM use (excl. buffer) = 3724541952 (3552.0MiB)
Max segment size = 3724541952 (3552.0MiB)

Process part 1:
  Total parsing data decoded so far: 0.00%
  Total text decoded so far: 0 (0.00MiB)
  Permute phrases by source:   Permute phrases by source: time = 0.04s, I/O = 248.98MiB/s  Permute phrases by source: time = 0.08s, I/O = 278.36MiB/s  Permute phrases by source: time = 0.12s, I/O = 293.73MiB/s  Permute phrases by source: time = 0.16s, I/O = 304.67MiB/s  Permute phrases by source: 100.0%, time = 0.18s, I/O = 307.08MiB/s
  Processing this part will decode 8192.0MiB of text (100.00% of parsing)
  Last part = TRUE
  Decode segment 1/3 [0..3724541952):
    Selfdecode segment:     Selfdecode segment: time = 0.05s, I/O = 109.37MiB/s    Selfdecode segment: time = 0.68s, I/O = 17.97MiB/s    Selfdecode segment: time = 1.29s, I/O = 12.97MiB/s
    Distribute chunks:     Distribute chunks: time = 0.27s, I/O = 1898.20MiB/s    Distribute chunks: time = 0.36s, I/O = 1847.67MiB/s
    Write segment to disk: 27.80s, I/O = 127.79MiB/s
    Decode the nearby phrases for next segment: 0.11s
  Decode segment 2/3 [3724541952..7449083904):
    Load segment chunks:     Load segment chunks: 1.9%, time = 0.00s, I/O = 2987.75MiB/s    Load segment chunks: 4.1%, time = 0.01s, I/O = 3425.19MiB/s    Load segment chunks: 4.9%, time = 0.01s, I/O = 2768.31MiB/s    Load segment chunks: 5.6%, time = 0.01s, I/O = 2431.32MiB/s    Load segment chunks: 6.3%, time = 0.02s, I/O = 2226.59MiB/s    Load segment chunks: 7.1%, time = 0.02s, I/O = 2088.49MiB/s    Load segment chunks: 7.8%, time = 0.02s, I/O = 1989.30MiB/s    Load segment chunks: 8.6%, time = 0.02s, I/O = 1919.07MiB/s    Load segment chunks: 9.3%, time = 0.03s, I/O = 1858.94MiB/s    Load segment chunks: 10.1%, time = 0.03s, I/O = 1814.52MiB/s    Load segment chunks: 10.8%, time = 0.03s, I/O = 1775.49MiB/s    Load segment chunks: 11.6%, time = 0.04s, I/O = 1745.05MiB/s    Load segment chunks: 12.3%, time = 0.04s, I/O = 1718.75MiB/s    Load segment chunks: 13.1%, time = 0.04s, I/O = 1696.23MiB/s    Load segment chunks: 13.8%, time = 0.04s, I/O = 1676.67MiB/s    Load segment chunks: 14.9%, time = 0.05s, I/O = 1702.09MiB/s    Load segment chunks: 15.7%, time = 0.05s, I/O = 1683.67MiB/s    Load segment chunks: 16.4%, time = 0.05s, I/O = 1668.15MiB/s    Load segment chunks: 17.2%, time = 0.06s, I/O = 1654.20MiB/s    Load segment chunks: 17.9%, time = 0.06s, I/O = 1641.92MiB/s    Load segment chunks: 18.7%, time = 0.06s, I/O = 1630.50MiB/s    Load segment chunks: 19.4%, time = 0.06s, I/O = 1620.80MiB/s    Load segment chunks: 20.2%, time = 0.07s, I/O = 1610.93MiB/s    Load segment chunks: 20.9%, time = 0.07s, I/O = 1602.66MiB/s    Load segment chunks: 21.7%, time = 0.07s, I/O = 1594.39MiB/s    Load segment chunks: 22.4%, time = 0.08s, I/O = 1587.32MiB/s    Load segment chunks: 23.1%, time = 0.08s, I/O = 1580.22MiB/s    Load segment chunks: 23.9%, time = 0.08s, I/O = 1573.16MiB/s    Load segment chunks: 24.6%, time = 0.08s, I/O = 1566.82MiB/s    Load segment chunks: 25.4%, time = 0.09s, I/O = 1561.28MiB/s    Load segment chunks: 26.1%, time = 0.09s, I/O = 1555.90MiB/s    Load segment chunks: 26.9%, time = 0.09s, I/O = 1550.50MiB/s    Load segment chunks: 27.6%, time = 0.10s, I/O = 1545.95MiB/s    Load segment chunks: 28.4%, time = 0.10s, I/O = 1541.58MiB/s    Load segment chunks: 29.1%, time = 0.10s, I/O = 1537.14MiB/s    Load segment chunks: 29.9%, time = 0.10s, I/O = 1533.18MiB/s    Load segment chunks: 30.6%, time = 0.11s, I/O = 1529.48MiB/s    Load segment chunks: 31.4%, time = 0.11s, I/O = 1526.01MiB/s    Load segment chunks: 32.1%, time = 0.11s, I/O = 1522.47MiB/s    Load segment chunks: 32.8%, time = 0.12s, I/O = 1519.23MiB/s    Load segment chunks: 33.6%, time = 0.12s, I/O = 1516.26MiB/s    Load segment chunks: 34.3%, time = 0.12s, I/O = 1513.41MiB/s    Load segment chunks: 35.1%, time = 0.12s, I/O = 1510.38MiB/s    Load segment chunks: 35.8%, time = 0.13s, I/O = 1508.09MiB/s    Load segment chunks: 36.6%, time = 0.13s, I/O = 1505.78MiB/s    Load segment chunks: 37.3%, time = 0.13s, I/O = 1503.64MiB/s    Load segment chunks: 38.1%, time = 0.14s, I/O = 1501.53MiB/s    Load segment chunks: 38.8%, time = 0.14s, I/O = 1499.02MiB/s    Load segment chunks: 39.6%, time = 0.14s, I/O = 1496.32MiB/s    Load segment chunks: 40.3%, time = 0.14s, I/O = 1493.81MiB/s    Load segment chunks: 41.1%, time = 0.15s, I/O = 1491.81MiB/s    Load segment chunks: 41.8%, time = 0.15s, I/O = 1489.79MiB/s    Load segment chunks: 42.6%, time = 0.15s, I/O = 1487.75MiB/s    Load segment chunks: 43.3%, time = 0.16s, I/O = 1485.89MiB/s    Load segment chunks: 44.0%, time = 0.16s, I/O = 1483.70MiB/s    Load segment chunks: 44.8%, time = 0.16s, I/O = 1481.87MiB/s    Load segment chunks: 45.5%, time = 0.16s, I/O = 1480.09MiB/s    Load segment chunks: 46.3%, time = 0.17s, I/O = 1478.36MiB/s    Load segment chunks: 47.0%, time = 0.17s, I/O = 1476.77MiB/s    Load segment chunks: 47.8%, time = 0.17s, I/O = 1475.31MiB/s    Load segment chunks: 48.9%, time = 0.18s, I/O = 1485.02MiB/s    Load segment chunks: 49.3%, time = 0.18s, I/O = 1472.48MiB/s    Load segment chunks: 50.4%, time = 0.18s, I/O = 1481.86MiB/s    Load segment chunks: 51.1%, time = 0.19s, I/O = 1480.39MiB/s    Load segment chunks: 51.9%, time = 0.19s, I/O = 1479.20MiB/s    Load segment chunks: 52.6%, time = 0.19s, I/O = 1477.96MiB/s    Load segment chunks: 53.4%, time = 0.19s, I/O = 1476.61MiB/s    Load segment chunks: 54.1%, time = 0.20s, I/O = 1475.10MiB/s    Load segment chunks: 54.9%, time = 0.20s, I/O = 1473.83MiB/s    Load segment chunks: 55.6%, time = 0.20s, I/O = 1472.47MiB/s    Load segment chunks: 56.4%, time = 0.21s, I/O = 1471.02MiB/s    Load segment chunks: 57.1%, time = 0.21s, I/O = 1469.83MiB/s    Load segment chunks: 57.9%, time = 0.21s, I/O = 1468.58MiB/s    Load segment chunks: 58.6%, time = 0.21s, I/O = 1467.26MiB/s    Load segment chunks: 59.4%, time = 0.22s, I/O = 1466.14MiB/s    Load segment chunks: 60.1%, time = 0.22s, I/O = 1465.05MiB/s    Load segment chunks: 60.8%, time = 0.22s, I/O = 1463.98MiB/s    Load segment chunks: 61.6%, time = 0.23s, I/O = 1462.94MiB/s    Load segment chunks: 62.3%, time = 0.23s, I/O = 1461.95MiB/s    Load segment chunks: 63.1%, time = 0.23s, I/O = 1461.10MiB/s    Load segment chunks: 63.8%, time = 0.23s, I/O = 1460.07MiB/s    Load segment chunks: 64.6%, time = 0.24s, I/O = 1459.28MiB/s    Load segment chunks: 65.3%, time = 0.24s, I/O = 1458.56MiB/s    Load segment chunks: 66.1%, time = 0.24s, I/O = 1457.78MiB/s    Load segment chunks: 66.8%, time = 0.25s, I/O = 1456.68MiB/s    Load segment chunks: 67.6%, time = 0.25s, I/O = 1455.74MiB/s    Load segment chunks: 68.3%, time = 0.25s, I/O = 1454.69MiB/s    Load segment chunks: 69.1%, time = 0.25s, I/O = 1453.82MiB/s    Load segment chunks: 69.8%, time = 0.26s, I/O = 1452.90MiB/s    Load segment chunks: 70.5%, time = 0.26s, I/O = 1452.01MiB/s    Load segment chunks: 71.3%, time = 0.26s, I/O = 1451.13MiB/s    Load segment chunks: 72.0%, time = 0.27s, I/O = 1450.10MiB/s    Load segment chunks: 72.8%, time = 0.27s, I/O = 1449.25MiB/s    Load segment chunks: 73.5%, time = 0.27s, I/O = 1448.48MiB/s    Load segment chunks: 74.3%, time = 0.27s, I/O = 1447.52MiB/s    Load segment chunks: 75.0%, time = 0.28s, I/O = 1446.79MiB/s    Load segment chunks: 75.8%, time = 0.28s, I/O = 1446.18MiB/s    Load segment chunks: 76.5%, time = 0.28s, I/O = 1445.61MiB/s    Load segment chunks: 77.3%, time = 0.29s, I/O = 1445.00MiB/s    Load segment chunks: 78.0%, time = 0.29s, I/O = 1444.27MiB/s    Load segment chunks: 78.8%, time = 0.29s, I/O = 1443.41MiB/s    Load segment chunks: 79.5%, time = 0.30s, I/O = 1442.62MiB/s    Load segment chunks: 80.6%, time = 0.30s, I/O = 1448.59MiB/s    Load segment chunks: 81.4%, time = 0.30s, I/O = 1447.80MiB/s    Load segment chunks: 82.1%, time = 0.30s, I/O = 1447.11MiB/s    Load segment chunks: 82.9%, time = 0.31s, I/O = 1446.37MiB/s    Load segment chunks: 83.6%, time = 0.31s, I/O = 1445.63MiB/s    Load segment chunks: 84.4%, time = 0.31s, I/O = 1444.92MiB/s    Load segment chunks: 85.1%, time = 0.32s, I/O = 1444.24MiB/s    Load segment chunks: 85.9%, time = 0.32s, I/O = 1443.53MiB/s    Load segment chunks: 86.6%, time = 0.32s, I/O = 1442.90MiB/s    Load segment chunks: 87.3%, time = 0.32s, I/O = 1442.36MiB/s    Load segment chunks: 88.1%, time = 0.33s, I/O = 1441.91MiB/s    Load segment chunks: 88.8%, time = 0.33s, I/O = 1441.40MiB/s    Load segment chunks: 89.6%, time = 0.33s, I/O = 1440.90MiB/s    Load segment chunks: 90.3%, time = 0.34s, I/O = 1440.24MiB/s    Load segment chunks: 91.1%, time = 0.34s, I/O = 1439.60MiB/s    Load segment chunks: 91.8%, time = 0.34s, I/O = 1438.84MiB/s    Load segment chunks: 92.6%, time = 0.34s, I/O = 1438.23MiB/s    Load segment chunks: 93.3%, time = 0.35s, I/O = 1437.65MiB/s    Load segment chunks: 94.1%, time = 0.35s, I/O = 1437.04MiB/s    Load segment chunks: 94.8%, time = 0.35s, I/O = 1436.42MiB/s    Load segment chunks: 95.6%, time = 0.36s, I/O = 1435.71MiB/s    Load segment chunks: 96.3%, time = 0.36s, I/O = 1435.13MiB/s    Load segment chunks: 97.1%, time = 0.36s, I/O = 1434.47MiB/s    Load segment chunks: 97.8%, time = 0.37s, I/O = 1433.64MiB/s    Load segment chunks: 98.5%, time = 0.37s, I/O = 1433.12MiB/s    Load segment chunks: 99.3%, time = 0.37s, I/O = 1432.72MiB/s    Load segment chunks: 100.0%, time = 0.37s, I/O = 1431.72MiB/s    Load segment chunks: 100.0%, time = 0.38s, I/O = 1420.96MiB/s    Load segment chunks: 100.0%, time = 0.38s, I/O = 1410.33MiB/s    Load segment chunks: 100.0%, time = 0.38s, I/O = 1399.83MiB/s    Load segment chunks: 100.0%, time = 0.38s, I/O = 1396.30MiB/s
    Selfdecode segment:     Selfdecode segment: time = 0.52s, I/O = 4.58MiB/s
    Distribute chunks:     Distribute chunks: time = 0.00s, I/O = 481.77MiB/s
    Write segment to disk: 37.68s, I/O = 94.27MiB/s
    Decode the nearby phrases for next segment: 0.19s
  Decode segment 3/3 [7449083904..8589934592):
    Load segment chunks:     Load segment chunks: 4.7%, time = 0.10s, I/O = 61.09MiB/s    Load segment chunks: 7.8%, time = 0.14s, I/O = 69.79MiB/s    Load segment chunks: 10.9%, time = 0.19s, I/O = 74.74MiB/s    Load segment chunks: 14.0%, time = 0.23s, I/O = 77.58MiB/s    Load segment chunks: 17.1%, time = 0.28s, I/O = 79.33MiB/s    Load segment chunks: 20.2%, time = 0.32s, I/O = 80.95MiB/s    Load segment chunks: 23.3%, time = 0.37s, I/O = 81.88MiB/s    Load segment chunks: 26.5%, time = 0.41s, I/O = 82.60MiB/s    Load segment chunks: 29.6%, time = 0.46s, I/O = 83.41MiB/s    Load segment chunks: 32.7%, time = 0.50s, I/O = 83.62MiB/s    Load segment chunks: 35.8%, time = 0.55s, I/O = 84.27MiB/s    Load segment chunks: 38.9%, time = 0.60s, I/O = 83.47MiB/s    Load segment chunks: 42.0%, time = 0.76s, I/O = 71.12MiB/s    Load segment chunks: 45.1%, time = 0.80s, I/O = 72.06MiB/s    Load segment chunks: 48.3%, time = 0.85s, I/O = 72.78MiB/s    Load segment chunks: 51.4%, time = 0.90s, I/O = 73.69MiB/s    Load segment chunks: 54.5%, time = 0.94s, I/O = 74.40MiB/s    Load segment chunks: 57.6%, time = 0.99s, I/O = 75.01MiB/s    Load segment chunks: 60.7%, time = 1.03s, I/O = 75.69MiB/s    Load segment chunks: 63.8%, time = 1.08s, I/O = 76.23MiB/s    Load segment chunks: 66.9%, time = 1.12s, I/O = 76.73MiB/s    Load segment chunks: 70.0%, time = 1.17s, I/O = 77.25MiB/s    Load segment chunks: 73.2%, time = 1.21s, I/O = 77.67MiB/s    Load segment chunks: 76.3%, time = 1.27s, I/O = 77.33MiB/s    Load segment chunks: 79.4%, time = 1.31s, I/O = 77.87MiB/s    Load segment chunks: 82.5%, time = 1.36s, I/O = 78.15MiB/s    Load segment chunks: 85.6%, time = 1.40s, I/O = 78.67MiB/s    Load segment chunks: 88.7%, time = 1.44s, I/O = 79.11MiB/s    Load segment chunks: 91.8%, time = 1.48s, I/O = 79.56MiB/s    Load segment chunks: 95.0%, time = 1.53s, I/O = 79.99MiB/s    Load segment chunks: 99.6%, time = 1.77s, I/O = 72.51MiB/s    Load segment chunks: 100.0%, time = 1.77s, I/O = 72.44MiB/s
    Selfdecode segment:     Selfdecode segment: time = 0.13s, I/O = 4.06MiB/s
    Write segment to disk: 11.47s, I/O = 94.86MiB/s
    Decode the nearby phrases for next segment: 0.00s


Computation finished. Summary:
  I/O volume = 10072316074 (1.17B/B)
  number of decoded phrases: 4592930
  average phrase length = 1870.25
  length of decoded text: 8589934592 (8192.00MiB)
  elapsed time: 82.32s (0.0100s/MiB)
  speed: 99.52MiB/s
Decompression t(s)    : 82.3344521523
