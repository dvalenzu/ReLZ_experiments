LZ77 Parsing ...
Final Wtime(s)    : 9786
N Phrases         : 53857054
OnlineLz77ViaRlbwt object (0x7ffd31a432c0) printStatistics(0) BEGIN
Len with endmarker = 4294967297
emPos_ = 3445561975, succSamplePos_ = 2585685919, em_ = 0
DynRleForRlbwt object (0x7ffd31a432c0) printStatistics(0) BEGIN
TotalLen = 4294967296, #Runs = 273823513, Alphabet Size = 210
BTreeNode arity kB = 32, BtmNode arity kBtmBM = 32, BtmNode arity kBtmBS = 8
MTree bottom array size = 10316960, capacity = 10317312
STree bottom array size = 41580976, capacity = 41581568
Total: 6125619096 bytes = 5.98205e+06 KiB = 5841.85 MiB
MTree: 215678048 bytes, OccuRate = 84.4445 (= 100*10713426/12686944)
ATree: 4896 bytes, OccuRate = 76.0417 (= 100*219/288)
STree: 827225984 bytes, OccuRate = 84.4236 (= 100*41080817/48660352)
BtmNodeM: 687390672 bytes = 671280 KiB = 655.547 MiB, OccuRate = 82.941 (= 100*273823514/330142720)
BtmNodeS: 665829400 bytes = 650224 KiB = 634.984 MiB, OccuRate = 82.3164 (= 100*273823723/332647808)
BtmNode: 1353220072 bytes = 1.3215e+06 KiB = 1290.53 MiB, OccuRate = 82.6275 (= 100*547647237/662790528)
Links: 2406817888 bytes = 2.35041e+06 KiB = 2295.32 MiB
Samples: 1322672184 bytes = 1.29167e+06 KiB = 1261.4 MiB
---------------- additional details ----------------
Memory for weights: 491099576 bytes = 479589 KiB = 468.349 MiB
Over reserved: 7116376 bytes = 6949.59 KiB = 6.78671 MiB
DynRleForRlbwt object (0x7ffd31a432c0) printStatistics(0) END
OnlineLz77ViaRlbwt object (0x7ffd31a432c0) printStatistics(0) END
