
--- computing reference lenght based on memory limit:
--- With 4000MB of  memory, the reference length will be: 419430399 symbols
RLZ parser intitialized with:
Ignore     : 0
Ref        : 419430399
Partitions : 12
Memory(MB) : 4000
RLZ parsing...
Building SA...
We will read blocks of size: 734003201
Reading block 0/1
Block read in 0.138328 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
Reading block 1/1
Block read in 0.201369 (s)
Waiting for all tasks to be done...
Done.

*
Starting parallel parsing in block...Parallel parse in 101.43 (s)
Writing phrases... 
Phrases written  in 0.290704 (s)
Packing phrases...
Recursive call to next depth...

--- computing reference lenght based on memory limit:
--- With 4000MB of  memory, the reference length will be: 157286399 symbols

--- reference_len is longer than the text are to be parsed: adjusting it.RLZ parser intitialized with:
Ignore     : 7088095
Ref        : 14960492
Partitions : 12
Memory(MB) : 4000

Starting recursion depth 1
RLZ Parsing...
Building SA...
We will read blocks of size: 22048587
Reading block 0/0
Block read in 0.039729 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
ReParsing...
Safe bound for bits per factor: 13
IM Random Access to sums file will use at most (MB): 34
 Random Access to Sum Files IM (delta compressed)RESULTS_SCALABILITY/WIKIPEDIA//1073741824/RLZPP_MEM4000_THREADS_4_D_99_files/compressed_file.TMP.rlztmp.sums
Input succesfully compressed, output written in: RESULTS_SCALABILITY/WIKIPEDIA//1073741824/RLZPP_MEM4000_THREADS_4_D_99_files/compressed_file
Parsing performed in 2 passes

0-th level report: 

Factors skipped      : 0
Factors 77'ed in ref : 7088095
Factors RLZ          : 14960492
Factors Total        : 22048587
--------------------------------------------
Time SA          : 36.1696
Time LZ77 of ref : 10.7871
Time RLZ parse   : 101.927
-------------------------------
Time RLZ total   : 148.884
--------------------------------------
Rlz TOTAL time 	: 148.967
Pack      time 	: 0.741323
Recursion time 	: 10.4241
ReParse   time 	: 1.64788
Encode    time 	: 1.00001e-06
--------------------------------------
TOTAL     time 	: 161.78

1-th level report: 

Factors skipped      : 7088095
Factors 77'ed in ref : 11113576
Factors RLZ          : 0
Factors Total        : 18201671
--------------------------------------------
Time SA          : 8.36299
Time LZ77 of ref : 1.88525
Time RLZ parse   : 1.00001e-06
-------------------------------
Time RLZ total   : 10.2482


Final Wtime(s)    : 161.78
N Phrases         : 18201671
Compression ratio : 0.0904942
seconds per MiB   : 0.157988
seconds per GiB   : 161.78
MiB per second    : 6.32959
GiB per second    : 0.00618124
Input filename = /home/work/dvalenzu/Repos/RLZPP_experiments/RESULTS_SCALABILITY/WIKIPEDIA/1073741824/RLZPP_MEM4000_THREADS_4_D_99_files/compressed_file
Output filename = /home/work/dvalenzu/Repos/RLZPP_experiments/RESULTS_SCALABILITY/WIKIPEDIA/1073741824/RLZPP_MEM4000_THREADS_4_D_99_files/compressed_file.decompressed
Input size = 97167427 (92.7MiB)
Max disk use = 1152921504606846976 (1099511627776.0MiB)
RAM use = 3758096384 (3584.0MiB)
RAM use (excl. buffer) = 3724541952 (3552.0MiB)
Max segment size = 3724541952 (3552.0MiB)

Process part 1:
  Total parsing data decoded so far: 0.00%
  Total text decoded so far: 0 (0.00MiB)
  Permute phrases by source:   Permute phrases by source: time = 0.14s, I/O = 79.42MiB/s  Permute phrases by source: time = 0.29s, I/O = 75.57MiB/s  Permute phrases by source: time = 0.33s, I/O = 100.29MiB/s  Permute phrases by source: time = 0.38s, I/O = 114.87MiB/s  Permute phrases by source: time = 0.44s, I/O = 125.37MiB/s  Permute phrases by source: time = 0.48s, I/O = 140.22MiB/s  Permute phrases by source: time = 0.52s, I/O = 153.01MiB/s  Permute phrases by source: time = 0.56s, I/O = 163.82MiB/s  Permute phrases by source: time = 0.59s, I/O = 173.26MiB/s  Permute phrases by source: time = 0.63s, I/O = 181.53MiB/s  Permute phrases by source: time = 0.67s, I/O = 188.90MiB/s  Permute phrases by source: time = 0.71s, I/O = 195.47MiB/s  Permute phrases by source: time = 0.75s, I/O = 201.35MiB/s  Permute phrases by source: time = 0.79s, I/O = 206.66MiB/s  Permute phrases by source: time = 0.83s, I/O = 211.51MiB/s  Permute phrases by source: time = 0.86s, I/O = 215.88MiB/s  Permute phrases by source: time = 0.90s, I/O = 219.91MiB/s  Permute phrases by source: 100.0%, time = 0.92s, I/O = 221.26MiB/s
  Processing this part will decode 1024.0MiB of text (100.00% of parsing)
  Last part = TRUE
  Decode segment 1/1 [0..1073741824):
    Selfdecode segment:     Selfdecode segment: time = 0.07s, I/O = 85.34MiB/s    Selfdecode segment: time = 0.15s, I/O = 77.76MiB/s    Selfdecode segment: time = 0.24s, I/O = 73.60MiB/s    Selfdecode segment: time = 0.34s, I/O = 70.61MiB/s    Selfdecode segment: time = 0.44s, I/O = 68.97MiB/s    Selfdecode segment: time = 0.54s, I/O = 67.77MiB/s    Selfdecode segment: time = 0.65s, I/O = 66.59MiB/s    Selfdecode segment: time = 0.75s, I/O = 65.80MiB/s    Selfdecode segment: time = 0.86s, I/O = 65.33MiB/s    Selfdecode segment: time = 0.97s, I/O = 64.66MiB/s    Selfdecode segment: time = 1.07s, I/O = 64.45MiB/s    Selfdecode segment: time = 1.17s, I/O = 64.38MiB/s    Selfdecode segment: time = 1.27s, I/O = 64.41MiB/s    Selfdecode segment: time = 1.38s, I/O = 64.19MiB/s    Selfdecode segment: time = 1.49s, I/O = 63.74MiB/s    Selfdecode segment: time = 1.59s, I/O = 63.52MiB/s    Selfdecode segment: time = 1.70s, I/O = 63.43MiB/s    Selfdecode segment: time = 1.73s, I/O = 63.50MiB/s
    Write segment to disk: 0.40s, I/O = 2589.22MiB/s
    Decode the nearby phrases for next segment: 0.00s


Computation finished. Summary:
  I/O volume = 1401653533 (1.31B/B)
  number of decoded phrases: 18201671
  average phrase length = 58.99
  length of decoded text: 1073741824 (1024.00MiB)
  elapsed time: 3.09s (0.0030s/MiB)
  speed: 331.45MiB/s
Decompression t(s)    : 3.48263192177
