
--- computing reference lenght based on memory limit:
--- With 4000MB of  memory, the reference length will be: 419430399 symbols
RLZ parser intitialized with:
Ignore     : 0
Ref        : 419430399
Partitions : 3
Memory(MB) : 4000
RLZ parsing...
Building SA...
We will read blocks of size: 734003201
Reading block 0/3
Block read in 0.13565 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
Reading block 1/3
Block read in 0.228487 (s)
Waiting for all tasks to be done...
Done.
Reading block 2/3
Block read in 0.226873 (s)

*
Starting parallel parsing in block...Parallel parse in 188.52 (s)
Writing phrases... 
Phrases written  in 0.344026 (s)
Waiting for all tasks to be done...
Done.
Reading block 3/3
Block read in 0.043897 (s)

*
Starting parallel parsing in block...Parallel parse in 219.658 (s)
Writing phrases... 
Phrases written  in 0.392218 (s)
Waiting for all tasks to be done...
Done.

*
Starting parallel parsing in block...Parallel parse in 69.9054 (s)
Writing phrases... 
Phrases written  in 0.128103 (s)
Packing phrases...
Recursive call to next depth...

--- computing reference lenght based on memory limit:
--- With 4000MB of  memory, the reference length will be: 157286399 symbols

--- reference_len is longer than the text are to be parsed: adjusting it.RLZ parser intitialized with:
Ignore     : 7088095
Ref        : 41562408
Partitions : 3
Memory(MB) : 4000

Starting recursion depth 1
RLZ Parsing...
Building SA...
We will read blocks of size: 48650503
Reading block 0/0
Block read in 0.108873 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
ReParsing...
Safe bound for bits per factor: 13
IM Random Access to sums file will use at most (MB): 75
 Random Access to Sum Files IM (delta compressed)RESULTS_SCALABILITY/WIKIPEDIA//2147483648/RLZPP_MEM4000_THREADS_1_D_99_files/compressed_file.TMP.rlztmp.sums
Input succesfully compressed, output written in: RESULTS_SCALABILITY/WIKIPEDIA//2147483648/RLZPP_MEM4000_THREADS_1_D_99_files/compressed_file
Parsing performed in 2 passes

0-th level report: 

Factors skipped      : 0
Factors 77'ed in ref : 7088095
Factors RLZ          : 41562408
Factors Total        : 48650503
--------------------------------------------
Time SA          : 36.331
Time LZ77 of ref : 10.8724
Time RLZ parse   : 479.461
-------------------------------
Time RLZ total   : 526.665
--------------------------------------
Rlz TOTAL time 	: 526.766
Pack      time 	: 1.66136
Recursion time 	: 33.554
ReParse   time 	: 3.83618
Encode    time 	: 0
--------------------------------------
TOTAL     time 	: 565.817

1-th level report: 

Factors skipped      : 7088095
Factors 77'ed in ref : 30918132
Factors RLZ          : 0
Factors Total        : 38006227
--------------------------------------------
Time SA          : 26.121
Time LZ77 of ref : 7.24469
Time RLZ parse   : 1.00001e-06
-------------------------------
Time RLZ total   : 33.3657


Final Wtime(s)    : 565.817
N Phrases         : 38006227
Compression ratio : 0.0955419
seconds per MiB   : 0.276278
seconds per GiB   : 282.909
MiB per second    : 3.61954
GiB per second    : 0.00353471
Input filename = /home/work/dvalenzu/Repos/RLZPP_experiments/RESULTS_SCALABILITY/WIKIPEDIA/2147483648/RLZPP_MEM4000_THREADS_1_D_99_files/compressed_file
Output filename = /home/work/dvalenzu/Repos/RLZPP_experiments/RESULTS_SCALABILITY/WIKIPEDIA/2147483648/RLZPP_MEM4000_THREADS_1_D_99_files/compressed_file.decompressed
Input size = 205174770 (195.7MiB)
Max disk use = 1152921504606846976 (1099511627776.0MiB)
RAM use = 3758096384 (3584.0MiB)
RAM use (excl. buffer) = 3724541952 (3552.0MiB)
Max segment size = 3724541952 (3552.0MiB)

Process part 1:
  Total parsing data decoded so far: 0.00%
  Total text decoded so far: 0 (0.00MiB)
  Permute phrases by source:   Permute phrases by source: time = 0.26s, I/O = 42.09MiB/s  Permute phrases by source: time = 0.29s, I/O = 75.11MiB/s  Permute phrases by source: time = 0.33s, I/O = 101.20MiB/s  Permute phrases by source: time = 0.38s, I/O = 116.92MiB/s  Permute phrases by source: time = 0.44s, I/O = 124.46MiB/s  Permute phrases by source: time = 0.48s, I/O = 138.98MiB/s  Permute phrases by source: time = 0.55s, I/O = 143.25MiB/s  Permute phrases by source: time = 0.62s, I/O = 147.85MiB/s  Permute phrases by source: time = 0.65s, I/O = 157.36MiB/s  Permute phrases by source: time = 0.72s, I/O = 158.72MiB/s  Permute phrases by source: time = 0.84s, I/O = 151.17MiB/s  Permute phrases by source: time = 0.89s, I/O = 156.63MiB/s  Permute phrases by source: time = 0.93s, I/O = 162.64MiB/s  Permute phrases by source: time = 0.96s, I/O = 168.53MiB/s  Permute phrases by source: time = 1.00s, I/O = 173.98MiB/s  Permute phrases by source: time = 1.04s, I/O = 179.02MiB/s  Permute phrases by source: time = 1.08s, I/O = 183.69MiB/s  Permute phrases by source: time = 1.12s, I/O = 188.04MiB/s  Permute phrases by source: time = 1.16s, I/O = 192.11MiB/s  Permute phrases by source: time = 1.20s, I/O = 195.91MiB/s  Permute phrases by source: time = 1.23s, I/O = 199.46MiB/s  Permute phrases by source: time = 1.27s, I/O = 202.78MiB/s  Permute phrases by source: time = 1.31s, I/O = 205.90MiB/s  Permute phrases by source: time = 1.35s, I/O = 208.85MiB/s  Permute phrases by source: time = 1.39s, I/O = 211.67MiB/s  Permute phrases by source: time = 1.43s, I/O = 214.31MiB/s  Permute phrases by source: time = 1.46s, I/O = 216.83MiB/s  Permute phrases by source: time = 1.50s, I/O = 219.22MiB/s  Permute phrases by source: time = 1.54s, I/O = 221.50MiB/s  Permute phrases by source: time = 1.58s, I/O = 223.67MiB/s  Permute phrases by source: time = 1.62s, I/O = 225.75MiB/s  Permute phrases by source: time = 1.66s, I/O = 227.70MiB/s  Permute phrases by source: time = 1.69s, I/O = 229.55MiB/s  Permute phrases by source: time = 1.73s, I/O = 231.34MiB/s  Permute phrases by source: time = 1.77s, I/O = 233.05MiB/s  Permute phrases by source: time = 1.81s, I/O = 234.68MiB/s  Permute phrases by source: 100.0%, time = 1.82s, I/O = 235.08MiB/s
  Processing this part will decode 2048.0MiB of text (100.00% of parsing)
  Last part = TRUE
  Decode segment 1/1 [0..2147483648):
    Selfdecode segment:     Selfdecode segment: time = 0.07s, I/O = 85.81MiB/s    Selfdecode segment: time = 0.15s, I/O = 78.03MiB/s    Selfdecode segment: time = 0.24s, I/O = 73.73MiB/s    Selfdecode segment: time = 0.34s, I/O = 70.76MiB/s    Selfdecode segment: time = 0.44s, I/O = 69.17MiB/s    Selfdecode segment: time = 0.54s, I/O = 68.00MiB/s    Selfdecode segment: time = 0.65s, I/O = 66.84MiB/s    Selfdecode segment: time = 0.75s, I/O = 66.06MiB/s    Selfdecode segment: time = 0.85s, I/O = 65.62MiB/s    Selfdecode segment: time = 0.96s, I/O = 64.99MiB/s    Selfdecode segment: time = 1.06s, I/O = 64.78MiB/s    Selfdecode segment: time = 1.17s, I/O = 64.72MiB/s    Selfdecode segment: time = 1.26s, I/O = 64.76MiB/s    Selfdecode segment: time = 1.37s, I/O = 64.54MiB/s    Selfdecode segment: time = 1.48s, I/O = 64.09MiB/s    Selfdecode segment: time = 1.59s, I/O = 63.86MiB/s    Selfdecode segment: time = 1.69s, I/O = 63.76MiB/s    Selfdecode segment: time = 1.79s, I/O = 63.62MiB/s    Selfdecode segment: time = 1.90s, I/O = 63.54MiB/s    Selfdecode segment: time = 2.01s, I/O = 63.33MiB/s    Selfdecode segment: time = 2.11s, I/O = 63.24MiB/s    Selfdecode segment: time = 2.21s, I/O = 63.30MiB/s    Selfdecode segment: time = 2.31s, I/O = 63.39MiB/s    Selfdecode segment: time = 2.41s, I/O = 63.48MiB/s    Selfdecode segment: time = 2.52s, I/O = 63.22MiB/s    Selfdecode segment: time = 2.62s, I/O = 63.18MiB/s    Selfdecode segment: time = 2.72s, I/O = 63.19MiB/s    Selfdecode segment: time = 2.82s, I/O = 63.26MiB/s    Selfdecode segment: time = 2.92s, I/O = 63.48MiB/s    Selfdecode segment: time = 3.01s, I/O = 63.65MiB/s    Selfdecode segment: time = 3.12s, I/O = 63.52MiB/s    Selfdecode segment: time = 3.22s, I/O = 63.52MiB/s    Selfdecode segment: time = 3.32s, I/O = 63.45MiB/s    Selfdecode segment: time = 3.43s, I/O = 63.37MiB/s    Selfdecode segment: time = 3.53s, I/O = 63.39MiB/s    Selfdecode segment: time = 3.63s, I/O = 63.45MiB/s    Selfdecode segment: time = 3.66s, I/O = 63.36MiB/s
    Write segment to disk: 6.12s, I/O = 334.46MiB/s
    Decode the nearby phrases for next segment: 0.00s


Computation finished. Summary:
  I/O volume = 2839032412 (1.32B/B)
  number of decoded phrases: 38006227
  average phrase length = 56.50
  length of decoded text: 2147483648 (2048.00MiB)
  elapsed time: 12.92s (0.0063s/MiB)
  speed: 158.48MiB/s
Decompression t(s)    : 13.6499409676
