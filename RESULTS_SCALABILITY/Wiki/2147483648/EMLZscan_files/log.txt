Text filename = /home/work/dvalenzu/data/large/Wiki.2GiB
Output filename = /home/work/dvalenzu/Repos/RLZPP_experiments/RESULTS_SCALABILITY/WIKIPEDIA/2147483648/EMLZscan_files/compressed.lz
RAM use = 4194304000 (4000.00MiB)
Max block size = 119837257 (114.29MiB)
sizeof(textoff_t) = 6
sizeof(blockoff_t) = 5

Process block [0..119837257):
  Read block: 0.04s
  Compute SA of block: 8.94s
  Parse block: 2.47s
  Current progress: 0.0%, elapsed = 12s, z = 2041854, total I/O vol = 0.07n
Process block [119836768..239674025):
  Read block: 0.04s
  Compute SA of block: 9.03s
  Compute LCP of block: 3.72s
  Compute MS-support: 2.33s
  Stream:   Stream: 100.0%, time = 16.43, I/O = 6.95MB/s
  Invert matching statistics: 0.84s
  Parse block: 2.57s
  Current progress: 5.6%, elapsed = 47s, z = 4294166, total I/O vol = 0.20n
Process block [239669898..359507155):
  Read block: 0.02s
  Compute SA of block: 8.94s
  Compute LCP of block: 3.71s
  Compute MS-support: 2.26s
  Stream:   Stream: 100.0%, time = 33.22, I/O = 6.88MB/s
  Invert matching statistics: 0.88s
  Parse block: 2.52s
  Current progress: 11.2%, elapsed = 99s, z = 6329334, total I/O vol = 0.39n
Process block [359507138..479344395):
  Read block: 0.02s
  Compute SA of block: 8.95s
  Compute LCP of block: 3.65s
  Compute MS-support: 2.25s
  Stream:   Stream: 100.0%, time = 49.05, I/O = 6.99MB/s
  Invert matching statistics: 0.83s
  Parse block: 2.40s
  Current progress: 16.7%, elapsed = 166s, z = 7955320, total I/O vol = 0.62n
Process block [479342902..599180159):
  Read block: 0.02s
  Compute SA of block: 9.07s
  Compute LCP of block: 3.70s
  Compute MS-support: 2.25s
  Stream:   Stream: 100.0%, time = 64.80, I/O = 7.05MB/s
  Invert matching statistics: 0.84s
  Parse block: 2.72s
  Current progress: 22.3%, elapsed = 250s, z = 9690156, total I/O vol = 0.92n
Process block [599180145..719017402):
  Read block: 0.02s
  Compute SA of block: 9.66s
  Compute LCP of block: 3.81s
  Compute MS-support: 2.26s
  Stream:   Stream: 100.0%, time = 79.14, I/O = 7.22MB/s
  Invert matching statistics: 0.76s
  Parse block: 2.47s
  Current progress: 27.9%, elapsed = 348s, z = 11445258, total I/O vol = 1.27n
Process block [719016336..838853593):
  Read block: 0.02s
  Compute SA of block: 8.82s
  Compute LCP of block: 3.73s
  Compute MS-support: 2.21s
  Stream:   Stream: 100.0%, time = 88.91, I/O = 7.71MB/s
  Invert matching statistics: 0.82s
  Parse block: 2.56s
  Current progress: 33.5%, elapsed = 456s, z = 13490658, total I/O vol = 1.68n
Process block [838850672..958687929):
  Read block: 0.02s
  Compute SA of block: 8.69s
  Compute LCP of block: 3.58s
  Compute MS-support: 2.20s
  Stream:   Stream: 100.0%, time = 109.03, I/O = 7.34MB/s
  Invert matching statistics: 0.76s
  Parse block: 2.33s
  Current progress: 39.1%, elapsed = 583s, z = 14874361, total I/O vol = 2.14n
Process block [958687924..1078525181):
  Read block: 0.02s
  Compute SA of block: 8.88s
  Compute LCP of block: 3.67s
  Compute MS-support: 2.23s
  Stream:   Stream: 100.0%, time = 120.79, I/O = 7.57MB/s
  Invert matching statistics: 0.74s
  Parse block: 2.43s
  Current progress: 44.6%, elapsed = 722s, z = 16525571, total I/O vol = 2.66n
Process block [1078525178..1198362435):
  Read block: 0.02s
  Compute SA of block: 8.69s
  Compute LCP of block: 3.66s
  Compute MS-support: 2.19s
  Stream:   Stream: 100.0%, time = 136.89, I/O = 7.51MB/s
  Invert matching statistics: 0.71s
  Parse block: 2.42s
  Current progress: 50.2%, elapsed = 877s, z = 18164158, total I/O vol = 3.24n
Process block [1198360961..1318198218):
  Read block: 0.02s
  Compute SA of block: 8.61s
  Compute LCP of block: 3.57s
  Compute MS-support: 2.19s
  Stream:   Stream: 100.0%, time = 152.08, I/O = 7.51MB/s
  Invert matching statistics: 0.69s
  Parse block: 2.31s
  Current progress: 55.8%, elapsed = 1046s, z = 19496769, total I/O vol = 3.86n
Process block [1318197722..1438034979):
  Read block: 0.02s
  Compute SA of block: 9.07s
  Compute LCP of block: 3.87s
  Compute MS-support: 2.24s
  Stream:   Stream: 100.0%, time = 144.99, I/O = 8.67MB/s
  Invert matching statistics: 0.79s
  Parse block: 2.71s
  Current progress: 61.4%, elapsed = 1210s, z = 21948490, total I/O vol = 4.56n
Process block [1438034924..1557872181):
  Read block: 0.02s
  Compute SA of block: 8.75s
  Compute LCP of block: 3.63s
  Compute MS-support: 2.19s
  Stream:   Stream: 100.0%, time = 175.77, I/O = 7.80MB/s
  Invert matching statistics: 0.72s
  Parse block: 2.39s
  Current progress: 67.0%, elapsed = 1404s, z = 23521794, total I/O vol = 5.30n
Process block [1557872136..1677709393):
  Read block: 0.02s
  Compute SA of block: 8.77s
  Compute LCP of block: 3.71s
  Compute MS-support: 2.20s
  Stream:   Stream: 100.0%, time = 191.41, I/O = 7.76MB/s
  Invert matching statistics: 0.71s
  Parse block: 2.47s
  Current progress: 72.5%, elapsed = 1614s, z = 25307297, total I/O vol = 6.10n
Process block [1677707868..1797545125):
  Read block: 0.02s
  Compute SA of block: 8.65s
  Compute LCP of block: 3.77s
  Compute MS-support: 2.21s
  Stream:   Stream: 100.0%, time = 195.26, I/O = 8.19MB/s
  Invert matching statistics: 0.74s
  Parse block: 2.61s
  Current progress: 78.1%, elapsed = 1827s, z = 27521900, total I/O vol = 6.96n
Process block [1797544427..1917381684):
  Read block: 0.02s
  Compute SA of block: 8.76s
  Compute LCP of block: 3.65s
  Compute MS-support: 2.20s
  Stream:   Stream: 100.0%, time = 217.25, I/O = 7.89MB/s
  Invert matching statistics: 0.70s
  Parse block: 2.41s
  Current progress: 83.7%, elapsed = 2063s, z = 29160177, total I/O vol = 7.87n
Process block [1917379461..2037216718):
  Read block: 0.02s
  Compute SA of block: 8.82s
  Compute LCP of block: 3.68s
  Compute MS-support: 2.23s
  Stream:   Stream: 100.0%, time = 238.67, I/O = 7.66MB/s
  Invert matching statistics: 0.69s
  Parse block: 2.42s
  Current progress: 89.3%, elapsed = 2320s, z = 30746095, total I/O vol = 8.84n
Process block [2037216716..2147483648):
  Read block: 0.02s
  Compute SA of block: 7.78s
  Compute LCP of block: 3.36s
  Compute MS-support: 2.03s
  Stream:   Stream: 100.0%, time = 235.94, I/O = 8.23MB/s
  Invert matching statistics: 0.65s
  Parse block: 2.23s
  Current progress: 94.9%, elapsed = 2572s, z = 32253649, total I/O vol = 9.86n


Computation finished. Summary:
  elapsed time = 2571.99s (1.256s/MiB of text)
  speed = 0.80MiB of text/s
  I/O volume = 21179017878bytes (9.86bytes/input symbol)
  Number of phrases = 32253649
  Average phrase length = 66.581
Final Wtime(s) : 2571.99
N Phrases      : 32253649
	Command being timed: "./ext/EM-LZscan-0.2/src/emlz_parser -o RESULTS_SCALABILITY/WIKIPEDIA//2147483648/EMLZscan_files/compressed.lz -m4000 /home/work/dvalenzu/data/large/Wiki.2GiB"
	User time (seconds): 2552.33
	System time (seconds): 19.87
	Percent of CPU this job got: 100%
	Elapsed (wall clock) time (h:mm:ss or m:ss): 42:51.98
	Average shared text size (kbytes): 0
	Average unshared data size (kbytes): 0
	Average stack size (kbytes): 0
	Average total size (kbytes): 0
	Maximum resident set size (kbytes): 3984924
	Average resident set size (kbytes): 0
	Major (requiring I/O) page faults: 0
	Minor (reclaiming a frame) page faults: 19478044
	Voluntary context switches: 312
	Involuntary context switches: 3297
	Swaps: 0
	File system inputs: 0
	File system outputs: 1157184
	Socket messages sent: 0
	Socket messages received: 0
	Signals delivered: 0
	Page size (bytes): 4096
	Exit status: 0
