
--- computing reference lenght based on memory limit:
--- With 10000MB of  memory, the reference length will be: 1048575999 symbols
RLZ parser intitialized with:
Ignore     : 0
Ref        : 1048575999
Partitions : 3
Memory(MB) : 10000
RLZ parsing...
Building SA...
We will read blocks of size: 1835008001
Reading block 0...Block read in 8.45288 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
Reading block 1...Block read in 13.2254 (s)
Waiting for all tasks to be done...
Done.
Reading block 2...Block read in 13.224 (s)
Starting parallel parsing in block...Parallel parse in 25.9477 (s)
Writing phrases... 
Phrases writed  in 0.026743 (s)
Waiting for all tasks to be done...
Done.
Reading block 3...Block read in 13.194 (s)
Starting parallel parsing in block...Parallel parse in 25.9372 (s)
Writing phrases... 
Phrases writed  in 0.02507 (s)
Waiting for all tasks to be done...
Done.
Reading block 4...Block read in 13.1814 (s)
Starting parallel parsing in block...Parallel parse in 68.0619 (s)
Writing phrases... 
Phrases writed  in 0.081024 (s)
Waiting for all tasks to be done...
Done.
Reading block 5...Block read in 13.1999 (s)
Starting parallel parsing in block...Parallel parse in 81.1087 (s)
Writing phrases... 
Phrases writed  in 0.097523 (s)
Waiting for all tasks to be done...
Done.
Reading block 6...Block read in 13.2136 (s)
Starting parallel parsing in block...Parallel parse in 97.5826 (s)
Writing phrases... 
Phrases writed  in 0.121009 (s)
Waiting for all tasks to be done...
Done.
Reading block 7...Block read in 13.2947 (s)
Starting parallel parsing in block...Parallel parse in 99.8658 (s)
Writing phrases... 
Phrases writed  in 0.122507 (s)
Waiting for all tasks to be done...
Done.
Reading block 8...Block read in 13.227 (s)
Starting parallel parsing in block...Parallel parse in 94.4874 (s)
Writing phrases... 
Phrases writed  in 0.114533 (s)
Waiting for all tasks to be done...
Done.
Reading block 9...Block read in 13.2864 (s)
Starting parallel parsing in block...Parallel parse in 115.236 (s)
Writing phrases... 
Phrases writed  in 0.149339 (s)
Waiting for all tasks to be done...
Done.
Reading block 10...Block read in 13.2568 (s)
Starting parallel parsing in block...Parallel parse in 119.656 (s)
Writing phrases... 
Phrases writed  in 0.155965 (s)
Waiting for all tasks to be done...
Done.
Reading block 11...Block read in 13.2504 (s)
Starting parallel parsing in block...Parallel parse in 119.832 (s)
Writing phrases... 
Phrases writed  in 0.156543 (s)
Waiting for all tasks to be done...
Done.
Reading block 12...Block read in 13.2412 (s)
Starting parallel parsing in block...Parallel parse in 116.935 (s)
Writing phrases... 
Phrases writed  in 0.15093 (s)
Waiting for all tasks to be done...
Done.
Reading block 13...Block read in 13.2267 (s)
Starting parallel parsing in block...Parallel parse in 110.681 (s)
Writing phrases... 
Phrases writed  in 0.143123 (s)
Waiting for all tasks to be done...
Done.
Reading block 14...Block read in 13.3126 (s)
Starting parallel parsing in block...Parallel parse in 119.342 (s)
Writing phrases... 
Phrases writed  in 0.155729 (s)
Waiting for all tasks to be done...
Done.
Reading block 15...Block read in 13.2412 (s)
Starting parallel parsing in block...Parallel parse in 120.852 (s)
Writing phrases... 
Phrases writed  in 0.1575 (s)
Waiting for all tasks to be done...
Done.
Reading block 16...Block read in 13.2633 (s)
Starting parallel parsing in block...Parallel parse in 127.01 (s)
Writing phrases... 
Phrases writed  in 0.163838 (s)
Waiting for all tasks to be done...
Done.
Reading block 17...Block read in 13.2846 (s)
Starting parallel parsing in block...Parallel parse in 135.189 (s)
Writing phrases... 
Phrases writed  in 0.17241 (s)
Waiting for all tasks to be done...
Done.
Reading block 18...Block read in 13.3374 (s)
Starting parallel parsing in block...Parallel parse in 135.769 (s)
Writing phrases... 
Phrases writed  in 0.173224 (s)
Waiting for all tasks to be done...
Done.
Reading block 19...Block read in 2.01826 (s)
Starting parallel parsing in block...Parallel parse in 136.553 (s)
Writing phrases... 
Phrases writed  in 0.173508 (s)
Waiting for all tasks to be done...
Done.
Starting parallel parsing in block...Parallel parse in 19.2034 (s)
Writing phrases... 
Phrases writed  in 0.022247 (s)
Input succesfully compressed, output written in: KERNEL/34359738368/RLZPP_MEM10000_THREADS_1_D_0_files/compressed_file
Parsing performed in 1 passes

0-th level report: 

Factors skipped      : 0
Factors 77'ed in ref : 25460592
Factors RLZ          : 120638462
Factors Total        : 146099054
--------------------------------------------
Time SA          : 135.153
Time LZ77 of ref : 29.3944
Time RLZ parse   : 2112.19
-------------------------------
Time RLZ total   : 2276.74


Final Wtime(s)    : 2284.93
N Phrases         : 146099054
Compression ratio : 0.0680327
seconds per MiB   : 0.0697305
seconds per GiB   : 71.404
MiB per second    : 14.3409
GiB per second    : 0.0140048
