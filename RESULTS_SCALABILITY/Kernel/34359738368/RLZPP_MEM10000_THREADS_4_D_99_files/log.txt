
--- computing reference lenght based on memory limit:
--- With 10000MB of  memory, the reference length will be: 1048575999 symbols
RLZ parser intitialized with:
Ignore     : 0
Ref        : 1048575999
Partitions : 12
Memory(MB) : 10000
RLZ parsing...
Building SA...
We will read blocks of size: 1835008001
Reading block 0...Block read in 7.61086 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
Reading block 1...Block read in 13.1642 (s)
Waiting for all tasks to be done...
Done.
Reading block 2...Block read in 13.2241 (s)
Starting parallel parsing in block...Parallel parse in 13.1939 (s)
Writing phrases... 
Phrases writed  in 0.027923 (s)
Waiting for all tasks to be done...
Done.
Reading block 3...Block read in 13.1963 (s)
Starting parallel parsing in block...Parallel parse in 13.2945 (s)
Writing phrases... 
Phrases writed  in 0.027636 (s)
Waiting for all tasks to be done...
Done.
Reading block 4...Block read in 13.18 (s)
Starting parallel parsing in block...Parallel parse in 42.5697 (s)
Writing phrases... 
Phrases writed  in 0.082996 (s)
Waiting for all tasks to be done...
Done.
Reading block 5...Block read in 13.2069 (s)
Starting parallel parsing in block...Parallel parse in 49.2141 (s)
Writing phrases... 
Phrases writed  in 0.108244 (s)
Waiting for all tasks to be done...
Done.
Reading block 6...Block read in 13.2142 (s)
Starting parallel parsing in block...Parallel parse in 59.7596 (s)
Writing phrases... 
Phrases writed  in 0.125926 (s)
Waiting for all tasks to be done...
Done.
Reading block 7...Block read in 13.2357 (s)
Starting parallel parsing in block...Parallel parse in 60.5167 (s)
Writing phrases... 
Phrases writed  in 0.121661 (s)
Waiting for all tasks to be done...
Done.
Reading block 8...Block read in 13.2236 (s)
Starting parallel parsing in block...Parallel parse in 57.9362 (s)
Writing phrases... 
Phrases writed  in 0.121073 (s)
Waiting for all tasks to be done...
Done.
Reading block 9...Block read in 13.281 (s)
Starting parallel parsing in block...Parallel parse in 71.2594 (s)
Writing phrases... 
Phrases writed  in 0.144063 (s)
Waiting for all tasks to be done...
Done.
Reading block 10...Block read in 13.2539 (s)
Starting parallel parsing in block...Parallel parse in 73.3924 (s)
Writing phrases... 
Phrases writed  in 0.150901 (s)
Waiting for all tasks to be done...
Done.
Reading block 11...Block read in 13.2565 (s)
Starting parallel parsing in block...Parallel parse in 73.5678 (s)
Writing phrases... 
Phrases writed  in 0.15441 (s)
Waiting for all tasks to be done...
Done.
Reading block 12...Block read in 13.2457 (s)
Starting parallel parsing in block...Parallel parse in 71.2177 (s)
Writing phrases... 
Phrases writed  in 0.144863 (s)
Waiting for all tasks to be done...
Done.
Reading block 13...Block read in 13.2301 (s)
Starting parallel parsing in block...Parallel parse in 68.9748 (s)
Writing phrases... 
Phrases writed  in 0.141569 (s)
Waiting for all tasks to be done...
Done.
Reading block 14...Block read in 13.303 (s)
Starting parallel parsing in block...Parallel parse in 73.0921 (s)
Writing phrases... 
Phrases writed  in 0.151021 (s)
Waiting for all tasks to be done...
Done.
Reading block 15...Block read in 13.2471 (s)
Starting parallel parsing in block...Parallel parse in 74.1988 (s)
Writing phrases... 
Phrases writed  in 0.156551 (s)
Waiting for all tasks to be done...
Done.
Reading block 16...Block read in 13.3143 (s)
Starting parallel parsing in block...Parallel parse in 78.827 (s)
Writing phrases... 
Phrases writed  in 0.159347 (s)
Waiting for all tasks to be done...
Done.
Reading block 17...Block read in 13.2865 (s)
Starting parallel parsing in block...Parallel parse in 83.0288 (s)
Writing phrases... 
Phrases writed  in 0.173817 (s)
Waiting for all tasks to be done...
Done.
Reading block 18...Block read in 13.3418 (s)
Starting parallel parsing in block...Parallel parse in 83.0964 (s)
Writing phrases... 
Phrases writed  in 0.168439 (s)
Waiting for all tasks to be done...
Done.
Reading block 19...Block read in 2.01765 (s)
Starting parallel parsing in block...Parallel parse in 86.9086 (s)
Writing phrases... 
Phrases writed  in 0.172727 (s)
Waiting for all tasks to be done...
Done.
Starting parallel parsing in block...Parallel parse in 16.0814 (s)
Writing phrases... 
Phrases writed  in 0.021747 (s)
Packing phrases...
Recursive call to next depth...

--- computing reference lenght based on memory limit:
--- With 10000MB of  memory, the reference length will be: 393215999 symbols

--- reference_len is longer than the text are to be parsed: adjusting it.RLZ parser intitialized with:
Ignore     : 25460592
Ref        : 120638630
Partitions : 12
Memory(MB) : 10000

Starting recursion depth 1
RLZ Parsing...
Building SA...
We will read blocks of size: 146099222
Reading block 0...Block read in 0.324987 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
ReParsing...
Safe bound for bits per factor: 17
IM Random Access to sums file will use at most (MB): 296
 Random Access to Sum Files IM (delta compressed)KERNEL/34359738368/RLZPP_MEM10000_THREADS_4_D_99_files/compressed_file.TMP.rlztmp.sums
Input succesfully compressed, output written in: KERNEL/34359738368/RLZPP_MEM10000_THREADS_4_D_99_files/compressed_file
Parsing performed in 2 passes

0-th level report: 

Factors skipped      : 0
Factors 77'ed in ref : 25460592
Factors RLZ          : 120638630
Factors Total        : 146099222
--------------------------------------------
Time SA          : 133.945
Time LZ77 of ref : 29.3383
Time RLZ parse   : 1393.03
-------------------------------
Time RLZ total   : 1556.31
--------------------------------------
Rlz TOTAL time 	: 1556.6
Pack      time 	: 4.93578
Recursion time 	: 204.255
ReParse   time 	: 6.65623
Encode    time 	: 1.94361
--------------------------------------
TOTAL     time 	: 1774.4

1-th level report: 

Factors skipped      : 25460592
Factors 77'ed in ref : 2536575
Factors RLZ          : 0
Factors Total        : 27997167
--------------------------------------------
Time SA          : 200.826
Time LZ77 of ref : 2.73575
Time RLZ parse   : 1.00001e-06
-------------------------------
Time RLZ total   : 203.562


Final Wtime(s)    : 1774.4
N Phrases         : 27997167
Compression ratio : 0.0130372
seconds per MiB   : 0.0541502
seconds per GiB   : 55.4499
MiB per second    : 18.4671
GiB per second    : 0.0180343
