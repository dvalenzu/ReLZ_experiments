
--- computing reference lenght based on memory limit:
--- With 10000MB of  memory, the reference length will be: 1048575999 symbols
RLZ parser intitialized with:
Ignore     : 0
Ref        : 1048575999
Partitions : 12
Memory(MB) : 10000
RLZ parsing...
Building SA...
We will read blocks of size: 1835008001
Reading block 0...Block read in 0.343972 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
Reading block 1...Block read in 0.345267 (s)
Waiting for all tasks to be done...
Done.
Starting parallel parsing in block...Parallel parse in 7.93149 (s)
Writing phrases... 
Phrases writed  in 0.015357 (s)
Packing phrases...
Recursive call to next depth...

--- computing reference lenght based on memory limit:
--- With 10000MB of  memory, the reference length will be: 393215999 symbols

--- reference_len is longer than the text are to be parsed: adjusting it.RLZ parser intitialized with:
Ignore     : 25460592
Ref        : 773145
Partitions : 12
Memory(MB) : 10000

Starting recursion depth 1
RLZ Parsing...
Building SA...
We will read blocks of size: 26233737
Reading block 0...Block read in 0.002042 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
ReParsing...
Safe bound for bits per factor: 14
IM Random Access to sums file will use at most (MB): 43
 Random Access to Sum Files IM (delta compressed)KERNEL/2147483648/RLZPP_MEM10000_THREADS_4_D_99_files/compressed_file.TMP.rlztmp.sums
Input succesfully compressed, output written in: KERNEL/2147483648/RLZPP_MEM10000_THREADS_4_D_99_files/compressed_file
Parsing performed in 2 passes

0-th level report: 

Factors skipped      : 0
Factors 77'ed in ref : 25460592
Factors RLZ          : 773145
Factors Total        : 26233737
--------------------------------------------
Time SA          : 127.057
Time LZ77 of ref : 29.3485
Time RLZ parse   : 8.29241
-------------------------------
Time RLZ total   : 164.698
--------------------------------------
Rlz TOTAL time 	: 164.904
Pack      time 	: 0.823761
Recursion time 	: 1.24575
ReParse   time 	: 1.69484
Encode    time 	: 1.52769
--------------------------------------
TOTAL     time 	: 170.196

1-th level report: 

Factors skipped      : 25460592
Factors 77'ed in ref : 293656
Factors RLZ          : 0
Factors Total        : 25754248
--------------------------------------------
Time SA          : 0.604178
Time LZ77 of ref : 0.01734
Time RLZ parse   : 1.00001e-06
-------------------------------
Time RLZ total   : 0.621519


Final Wtime(s)    : 170.196
N Phrases         : 25754248
Compression ratio : 0.191884
seconds per MiB   : 0.0831035
seconds per GiB   : 85.098
MiB per second    : 12.0332
GiB per second    : 0.0117512
