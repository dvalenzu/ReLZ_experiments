
--- computing reference lenght based on memory limit:
--- With 10000MB of  memory, the reference length will be: 1048575999 symbols
RLZ parser intitialized with:
Ignore     : 0
Ref        : 1048575999
Partitions : 3
Memory(MB) : 10000
RLZ parsing...
Building SA...
We will read blocks of size: 1835008001
Reading block 0...Block read in 7.90792 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
Reading block 1...Block read in 5.04489 (s)
Waiting for all tasks to be done...
Done.
Starting parallel parsing in block...Parallel parse in 14.5807 (s)
Writing phrases... 
Phrases writed  in 0.015005 (s)
Packing phrases...
Recursive call to next depth...

--- computing reference lenght based on memory limit:
--- With 10000MB of  memory, the reference length will be: 393215999 symbols

--- reference_len is longer than the text are to be parsed: adjusting it.RLZ parser intitialized with:
Ignore     : 25460592
Ref        : 773136
Partitions : 3
Memory(MB) : 10000

Starting recursion depth 1
RLZ Parsing...
Building SA...
We will read blocks of size: 26233728
Reading block 0...Block read in 0.001958 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
ReParsing...
Safe bound for bits per factor: 14
IM Random Access to sums file will use at most (MB): 43
 Random Access to Sum Files IM (delta compressed)KERNEL/2147483648/RLZPP_MEM10000_THREADS_1_D_99_files/compressed_file.TMP.rlztmp.sums
Input succesfully compressed, output written in: KERNEL/2147483648/RLZPP_MEM10000_THREADS_1_D_99_files/compressed_file
Parsing performed in 2 passes

0-th level report: 

Factors skipped      : 0
Factors 77'ed in ref : 25460592
Factors RLZ          : 773136
Factors Total        : 26233728
--------------------------------------------
Time SA          : 134.004
Time LZ77 of ref : 29.2852
Time RLZ parse   : 19.641
-------------------------------
Time RLZ total   : 182.931
--------------------------------------
Rlz TOTAL time 	: 183.131
Pack      time 	: 0.839324
Recursion time 	: 1.33468
ReParse   time 	: 1.68601
Encode    time 	: 1.53314
--------------------------------------
TOTAL     time 	: 188.524

1-th level report: 

Factors skipped      : 25460592
Factors 77'ed in ref : 293627
Factors RLZ          : 0
Factors Total        : 25754219
--------------------------------------------
Time SA          : 0.635073
Time LZ77 of ref : 0.016168
Time RLZ parse   : 1.00001e-06
-------------------------------
Time RLZ total   : 0.651242


Final Wtime(s)    : 188.524
N Phrases         : 25754219
Compression ratio : 0.191884
seconds per MiB   : 0.0920527
seconds per GiB   : 94.262
MiB per second    : 10.8633
GiB per second    : 0.0106087
