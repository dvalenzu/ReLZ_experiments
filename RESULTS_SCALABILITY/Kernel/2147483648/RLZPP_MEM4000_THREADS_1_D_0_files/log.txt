
--- computing reference lenght based on memory limit:
--- With 4000MB of  memory, the reference length will be: 419430399 symbols
RLZ parser intitialized with:
Ignore     : 0
Ref        : 419430399
Partitions : 3
Memory(MB) : 4000
RLZ parsing...
Building SA...
We will read blocks of size: 734003201
Reading block 0/3
Block read in 0.13251 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
Reading block 1/3
Block read in 0.226415 (s)
Waiting for all tasks to be done...
Done.
Reading block 2/3
Block read in 0.224397 (s)

*
Starting parallel parsing in block...Parallel parse in 147.988 (s)
Writing phrases... 
Phrases written  in 0.259526 (s)
Waiting for all tasks to be done...
Done.
Reading block 3/3
Block read in 0.043695 (s)

*
Starting parallel parsing in block...Parallel parse in 79.5209 (s)
Writing phrases... 
Phrases written  in 0.136792 (s)
Waiting for all tasks to be done...
Done.

*
Starting parallel parsing in block...Parallel parse in 72.9079 (s)
Writing phrases... 
Phrases written  in 0.127895 (s)
Input succesfully compressed, output written in: RESULTS_SCALABILITY/KERNEL//2147483648/RLZPP_MEM4000_THREADS_1_D_0_files/compressed_file
Parsing performed in 1 passes

0-th level report: 

Factors skipped      : 0
Factors 77'ed in ref : 21709237
Factors RLZ          : 27399888
Factors Total        : 49109125
--------------------------------------------
Time SA          : 31.4141
Time LZ77 of ref : 13.0709
Time RLZ parse   : 301.445
-------------------------------
Time RLZ total   : 345.93


Final Wtime(s)    : 346.018
N Phrases         : 49109125
Compression ratio : 0.120726
seconds per MiB   : 0.168954
seconds per GiB   : 173.009
MiB per second    : 5.91876
GiB per second    : 0.00578004
Input filename = /home/work/dvalenzu/Repos/RLZPP_experiments/RESULTS_SCALABILITY/KERNEL/2147483648/RLZPP_MEM4000_THREADS_1_D_0_files/compressed_file
Output filename = /home/work/dvalenzu/Repos/RLZPP_experiments/RESULTS_SCALABILITY/KERNEL/2147483648/RLZPP_MEM4000_THREADS_1_D_0_files/compressed_file.decompressed
Input size = 259256513 (247.2MiB)
Max disk use = 1152921504606846976 (1099511627776.0MiB)
RAM use = 3758096384 (3584.0MiB)
RAM use (excl. buffer) = 3724541952 (3552.0MiB)
Max segment size = 3724541952 (3552.0MiB)

Process part 1:
  Total parsing data decoded so far: 0.00%
  Total text decoded so far: 0 (0.00MiB)
  Permute phrases by source:   Permute phrases by source: time = 0.44s, I/O = 24.03MiB/s  Permute phrases by source: time = 0.47s, I/O = 45.43MiB/s  Permute phrases by source: time = 0.51s, I/O = 63.91MiB/s  Permute phrases by source: time = 0.54s, I/O = 80.08MiB/s  Permute phrases by source: time = 0.58s, I/O = 94.20MiB/s  Permute phrases by source: time = 0.61s, I/O = 106.74MiB/s  Permute phrases by source: time = 0.65s, I/O = 117.95MiB/s  Permute phrases by source: time = 0.68s, I/O = 128.06MiB/s  Permute phrases by source: time = 0.72s, I/O = 137.14MiB/s  Permute phrases by source: time = 0.75s, I/O = 145.38MiB/s  Permute phrases by source: time = 0.79s, I/O = 152.93MiB/s  Permute phrases by source: time = 0.82s, I/O = 159.38MiB/s  Permute phrases by source: time = 0.87s, I/O = 164.35MiB/s  Permute phrases by source: time = 0.90s, I/O = 170.26MiB/s  Permute phrases by source: time = 0.95s, I/O = 175.19MiB/s  Permute phrases by source: time = 0.99s, I/O = 179.36MiB/s  Permute phrases by source: time = 1.03s, I/O = 184.19MiB/s  Permute phrases by source: time = 1.07s, I/O = 188.14MiB/s  Permute phrases by source: time = 1.12s, I/O = 191.35MiB/s  Permute phrases by source: time = 1.16s, I/O = 195.43MiB/s  Permute phrases by source: time = 1.20s, I/O = 198.33MiB/s  Permute phrases by source: time = 1.25s, I/O = 200.62MiB/s  Permute phrases by source: time = 1.29s, I/O = 203.61MiB/s  Permute phrases by source: time = 1.33s, I/O = 206.09MiB/s  Permute phrases by source: time = 1.37s, I/O = 208.10MiB/s  Permute phrases by source: time = 1.41s, I/O = 210.69MiB/s  Permute phrases by source: time = 1.45s, I/O = 212.63MiB/s  Permute phrases by source: time = 1.50s, I/O = 214.29MiB/s  Permute phrases by source: time = 1.54s, I/O = 216.45MiB/s  Permute phrases by source: time = 1.58s, I/O = 218.18MiB/s  Permute phrases by source: time = 1.62s, I/O = 219.54MiB/s  Permute phrases by source: time = 1.66s, I/O = 221.42MiB/s  Permute phrases by source: time = 1.70s, I/O = 222.96MiB/s  Permute phrases by source: time = 1.74s, I/O = 224.12MiB/s  Permute phrases by source: time = 1.78s, I/O = 225.77MiB/s  Permute phrases by source: time = 1.83s, I/O = 227.00MiB/s  Permute phrases by source: time = 1.87s, I/O = 227.96MiB/s  Permute phrases by source: time = 1.91s, I/O = 229.42MiB/s  Permute phrases by source: time = 1.95s, I/O = 230.72MiB/s  Permute phrases by source: time = 1.99s, I/O = 232.04MiB/s  Permute phrases by source: time = 2.03s, I/O = 233.40MiB/s  Permute phrases by source: time = 2.07s, I/O = 234.65MiB/s  Permute phrases by source: time = 2.10s, I/O = 235.86MiB/s  Permute phrases by source: time = 2.14s, I/O = 237.03MiB/s  Permute phrases by source: time = 2.18s, I/O = 238.16MiB/s  Permute phrases by source: time = 2.22s, I/O = 239.28MiB/s  Permute phrases by source: 100.0%, time = 2.25s, I/O = 240.13MiB/s
  Processing this part will decode 2048.0MiB of text (100.00% of parsing)
  Last part = TRUE
  Decode segment 1/1 [0..2147483648):
    Selfdecode segment:     Selfdecode segment: time = 0.04s, I/O = 135.50MiB/s    Selfdecode segment: time = 0.10s, I/O = 117.42MiB/s    Selfdecode segment: time = 0.16s, I/O = 108.47MiB/s    Selfdecode segment: time = 0.23s, I/O = 105.07MiB/s    Selfdecode segment: time = 0.29s, I/O = 101.64MiB/s    Selfdecode segment: time = 0.36s, I/O = 98.62MiB/s    Selfdecode segment: time = 0.43s, I/O = 96.69MiB/s    Selfdecode segment: time = 0.50s, I/O = 96.22MiB/s    Selfdecode segment: time = 0.56s, I/O = 95.62MiB/s    Selfdecode segment: time = 0.63s, I/O = 94.15MiB/s    Selfdecode segment: time = 0.70s, I/O = 93.10MiB/s    Selfdecode segment: time = 0.77s, I/O = 92.79MiB/s    Selfdecode segment: time = 0.84s, I/O = 91.97MiB/s    Selfdecode segment: time = 0.92s, I/O = 91.29MiB/s    Selfdecode segment: time = 0.99s, I/O = 90.94MiB/s    Selfdecode segment: time = 1.07s, I/O = 90.57MiB/s    Selfdecode segment: time = 1.15s, I/O = 90.22MiB/s    Selfdecode segment: time = 1.22s, I/O = 89.93MiB/s    Selfdecode segment: time = 1.30s, I/O = 89.69MiB/s    Selfdecode segment: time = 1.38s, I/O = 89.34MiB/s    Selfdecode segment: time = 1.46s, I/O = 89.10MiB/s    Selfdecode segment: time = 1.51s, I/O = 89.99MiB/s    Selfdecode segment: time = 1.57s, I/O = 90.56MiB/s    Selfdecode segment: time = 1.63s, I/O = 91.20MiB/s    Selfdecode segment: time = 1.70s, I/O = 91.47MiB/s    Selfdecode segment: time = 1.76s, I/O = 92.16MiB/s    Selfdecode segment: time = 1.83s, I/O = 91.98MiB/s    Selfdecode segment: time = 2.01s, I/O = 86.79MiB/s    Selfdecode segment: time = 2.07s, I/O = 87.43MiB/s    Selfdecode segment: time = 2.13s, I/O = 88.00MiB/s    Selfdecode segment: time = 2.19s, I/O = 88.27MiB/s    Selfdecode segment: time = 2.25s, I/O = 88.65MiB/s    Selfdecode segment: time = 2.31s, I/O = 89.12MiB/s    Selfdecode segment: time = 2.50s, I/O = 84.93MiB/s    Selfdecode segment: time = 2.56s, I/O = 85.48MiB/s    Selfdecode segment: time = 2.62s, I/O = 85.94MiB/s    Selfdecode segment: time = 2.68s, I/O = 86.44MiB/s    Selfdecode segment: time = 2.75s, I/O = 86.68MiB/s    Selfdecode segment: time = 2.80s, I/O = 87.20MiB/s    Selfdecode segment: time = 2.88s, I/O = 87.16MiB/s    Selfdecode segment: time = 3.06s, I/O = 83.90MiB/s    Selfdecode segment: time = 3.12s, I/O = 84.42MiB/s    Selfdecode segment: time = 3.18s, I/O = 84.80MiB/s    Selfdecode segment: time = 3.24s, I/O = 85.16MiB/s    Selfdecode segment: time = 3.31s, I/O = 85.45MiB/s    Selfdecode segment: time = 3.36s, I/O = 85.92MiB/s    Selfdecode segment: time = 3.47s, I/O = 84.71MiB/s
    Write segment to disk: 2.97s, I/O = 690.20MiB/s
    Decode the nearby phrases for next segment: 0.00s


Computation finished. Summary:
  I/O volume = 3023485875 (1.41B/B)
  number of decoded phrases: 49109125
  average phrase length = 43.73
  length of decoded text: 2147483648 (2048.00MiB)
  elapsed time: 9.45s (0.0046s/MiB)
  speed: 216.69MiB/s
Decompression t(s)    : 9.48900580406
