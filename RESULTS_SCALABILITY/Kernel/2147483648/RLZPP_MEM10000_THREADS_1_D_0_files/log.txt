
--- computing reference lenght based on memory limit:
--- With 10000MB of  memory, the reference length will be: 1048575999 symbols
RLZ parser intitialized with:
Ignore     : 0
Ref        : 1048575999
Partitions : 3
Memory(MB) : 10000
RLZ parsing...
Building SA...
We will read blocks of size: 1835008001
Reading block 0...Block read in 0.358945 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
Reading block 1...Block read in 2.16969 (s)
Waiting for all tasks to be done...
Done.
Starting parallel parsing in block...Parallel parse in 14.5441 (s)
Writing phrases... 
Phrases writed  in 0.014868 (s)
Input succesfully compressed, output written in: KERNEL/2147483648/RLZPP_MEM10000_THREADS_1_D_0_files/compressed_file
Parsing performed in 1 passes

0-th level report: 

Factors skipped      : 0
Factors 77'ed in ref : 25460592
Factors RLZ          : 773136
Factors Total        : 26233728
--------------------------------------------
Time SA          : 126.984
Time LZ77 of ref : 29.4615
Time RLZ parse   : 16.729
-------------------------------
Time RLZ total   : 173.174


Final Wtime(s)    : 174.961
N Phrases         : 26233728
Compression ratio : 0.195457
seconds per MiB   : 0.08543
seconds per GiB   : 87.4803
MiB per second    : 11.7055
GiB per second    : 0.0114311
