
--- computing reference lenght based on memory limit:
--- With 4000MB of  memory, the reference length will be: 419430399 symbols
RLZ parser intitialized with:
Ignore     : 0
Ref        : 419430399
Partitions : 3
Memory(MB) : 4000
RLZ parsing...
Building SA...
We will read blocks of size: 734003201
Reading block 0/23
Block read in 3.17398 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
Reading block 1/23
Block read in 5.39345 (s)
Waiting for all tasks to be done...
Done.
Reading block 2/23
Block read in 5.50809 (s)

*
Starting parallel parsing in block...Parallel parse in 149.276 (s)
Writing phrases... 
Phrases written  in 0.260054 (s)
Waiting for all tasks to be done...
Done.
Reading block 3/23
Block read in 5.41368 (s)

*
Starting parallel parsing in block...Parallel parse in 80.1951 (s)
Writing phrases... 
Phrases written  in 0.13667 (s)
Waiting for all tasks to be done...
Done.
Reading block 4/23
Block read in 5.52081 (s)

*
Starting parallel parsing in block...Parallel parse in 150.162 (s)
Writing phrases... 
Phrases written  in 0.262031 (s)
Waiting for all tasks to be done...
Done.
Reading block 5/23
Block read in 5.47564 (s)

*
Starting parallel parsing in block...Parallel parse in 80.2159 (s)
Writing phrases... 
Phrases written  in 0.136686 (s)
Waiting for all tasks to be done...
Done.
Reading block 6/23
Block read in 5.50931 (s)

*
Starting parallel parsing in block...Parallel parse in 149.394 (s)
Writing phrases... 
Phrases written  in 0.260797 (s)
Waiting for all tasks to be done...
Done.
Reading block 7/23
Block read in 5.46607 (s)

*
Starting parallel parsing in block...Parallel parse in 80.3214 (s)
Writing phrases... 
Phrases written  in 0.136769 (s)
Waiting for all tasks to be done...
Done.
Reading block 8/23
Block read in 5.51048 (s)

*
Starting parallel parsing in block...Parallel parse in 160.853 (s)
Writing phrases... 
Phrases written  in 0.285922 (s)
Waiting for all tasks to be done...
Done.
Reading block 9/23
Block read in 5.4784 (s)

*
Starting parallel parsing in block...Parallel parse in 98.939 (s)
Writing phrases... 
Phrases written  in 0.169291 (s)
Waiting for all tasks to be done...
Done.
Reading block 10/23
Block read in 5.46103 (s)

*
Starting parallel parsing in block...Parallel parse in 159.142 (s)
Writing phrases... 
Phrases written  in 0.27451 (s)
Waiting for all tasks to be done...
Done.
Reading block 11/23
Block read in 5.44953 (s)

*
Starting parallel parsing in block...Parallel parse in 98.6286 (s)
Writing phrases... 
Phrases written  in 0.164171 (s)
Waiting for all tasks to be done...
Done.
Reading block 12/23
Block read in 5.45469 (s)

*
Starting parallel parsing in block...Parallel parse in 159.346 (s)
Writing phrases... 
Phrases written  in 0.274985 (s)
Waiting for all tasks to be done...
Done.
Reading block 13/23
Block read in 5.41609 (s)

*
Starting parallel parsing in block...Parallel parse in 102.011 (s)
Writing phrases... 
Phrases written  in 0.175633 (s)
Waiting for all tasks to be done...
Done.
Reading block 14/23
Block read in 5.49567 (s)

*
Starting parallel parsing in block...Parallel parse in 161.892 (s)
Writing phrases... 
Phrases written  in 0.278134 (s)
Waiting for all tasks to be done...
Done.
Reading block 15/23
Block read in 5.38873 (s)

*
Starting parallel parsing in block...Parallel parse in 103.813 (s)
Writing phrases... 
Phrases written  in 0.176171 (s)
Waiting for all tasks to be done...
Done.
Reading block 16/23
Block read in 5.52255 (s)

*
Starting parallel parsing in block...Parallel parse in 161.883 (s)
Writing phrases... 
Phrases written  in 0.277887 (s)
Waiting for all tasks to be done...
Done.
Reading block 17/23
Block read in 5.46101 (s)

*
Starting parallel parsing in block...Parallel parse in 103.916 (s)
Writing phrases... 
Phrases written  in 0.176381 (s)
Waiting for all tasks to be done...
Done.
Reading block 18/23
Block read in 5.44643 (s)

*
Starting parallel parsing in block...Parallel parse in 161.763 (s)
Writing phrases... 
Phrases written  in 0.277616 (s)
Waiting for all tasks to be done...
Done.
Reading block 19/23
Block read in 5.56503 (s)

*
Starting parallel parsing in block...Parallel parse in 103.941 (s)
Writing phrases... 
Phrases written  in 0.176057 (s)
Waiting for all tasks to be done...
Done.
Reading block 20/23
Block read in 5.3945 (s)

*
Starting parallel parsing in block...Parallel parse in 162.166 (s)
Writing phrases... 
Phrases written  in 0.277945 (s)
Waiting for all tasks to be done...
Done.
Reading block 21/23
Block read in 5.39947 (s)

*
Starting parallel parsing in block...Parallel parse in 106.572 (s)
Writing phrases... 
Phrases written  in 0.179006 (s)
Waiting for all tasks to be done...
Done.
Reading block 22/23
Block read in 5.425 (s)

*
Starting parallel parsing in block...Parallel parse in 162.673 (s)
Writing phrases... 
Phrases written  in 0.278787 (s)
Waiting for all tasks to be done...
Done.
Reading block 23/23
Block read in 4.5533 (s)

*
Starting parallel parsing in block...Parallel parse in 106.866 (s)
Writing phrases... 
Phrases written  in 0.180606 (s)
Waiting for all tasks to be done...
Done.

*
Starting parallel parsing in block...Parallel parse in 158.122 (s)
Writing phrases... 
Phrases written  in 0.270821 (s)
Packing phrases...
Recursive call to next depth...

--- computing reference lenght based on memory limit:
--- With 4000MB of  memory, the reference length will be: 157286399 symbols
RLZ parser intitialized with:
Ignore     : 21709237
Ref        : 157286399
Partitions : 3
Memory(MB) : 4000

Starting recursion depth 1
RLZ Parsing...
Building SA...
We will read blocks of size: 73400320
Reading block 0/2
Block read in 0.398402 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
Reading block 1/2
Block read in 0.187529 (s)
Waiting for all tasks to be done...
Done.
Reading block 2/2
Block read in 0.092886 (s)

*
Starting parallel parsing in block...Parallel parse in 1.72846 (s)
Writing phrases... 
Phrases written  in 0.01521 (s)
Waiting for all tasks to be done...
Done.

*
Starting parallel parsing in block...Parallel parse in 3.12041 (s)
Writing phrases... 
Phrases written  in 0.03596 (s)
Packing...
Recursive call...

--- computing reference lenght based on memory limit:
--- With 4000MB of  memory, the reference length will be: 157286399 symbols

--- reference_len is longer than the text are to be parsed: adjusting it.RLZ parser intitialized with:
Ignore     : 28080257
Ref        : 2742385
Partitions : 3
Memory(MB) : 4000

Starting recursion depth 2
RLZ Parsing...
Building SA...
We will read blocks of size: 30822642
Reading block 0/0
Block read in 0.006946 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
Reparsing...
Safe bound for bits per factor: 11
IM Random Access to sums file will use at most (MB): 40
 Random Access to Sum Files IM (delta compressed)RESULTS_SCALABILITY/KERNEL//17179869184/RLZPP_MEM4000_THREADS_1_D_99_files/compressed_file.TMP.rlz.packed.Depth1.rlztmp.sums
ReParsing...
Safe bound for bits per factor: 13
IM Random Access to sums file will use at most (MB): 449
 Random Access to Sum Files IM (delta compressed)RESULTS_SCALABILITY/KERNEL//17179869184/RLZPP_MEM4000_THREADS_1_D_99_files/compressed_file.TMP.rlztmp.sums
Input succesfully compressed, output written in: RESULTS_SCALABILITY/KERNEL//17179869184/RLZPP_MEM4000_THREADS_1_D_99_files/compressed_file
Parsing performed in 3 passes

0-th level report: 

Factors skipped      : 0
Factors 77'ed in ref : 21709237
Factors RLZ          : 268147906
Factors Total        : 289857143
--------------------------------------------
Time SA          : 34.2444
Time LZ77 of ref : 13.1197
Time RLZ parse   : 3092.35
-------------------------------
Time RLZ total   : 3139.71
--------------------------------------
Rlz TOTAL time 	: 3139.81
Pack      time 	: 16.0859
Recursion time 	: 344.933
ReParse   time 	: 13.5778
Encode    time 	: 0
--------------------------------------
TOTAL     time 	: 3514.41

1-th level report: 

Factors skipped      : 21709237
Factors 77'ed in ref : 6371020
Factors RLZ          : 2742385
Factors Total        : 30822642
--------------------------------------------
Time SA          : 327.97
Time LZ77 of ref : 4.50998
Time RLZ parse   : 5.182
-------------------------------
Time RLZ total   : 337.662
--------------------------------------
Rlz TOTAL time 	: 338.281
Pack      time 	: 1.02346
Recursion time 	: 3.68938
ReParse   time 	: 1.93875
--------------------------------------

2-th level report: 

Factors skipped      : 28080257
Factors 77'ed in ref : 401580
Factors RLZ          : 0
Factors Total        : 28481837
--------------------------------------------
Time SA          : 2.94345
Time LZ77 of ref : 0.05303
Time RLZ parse   : 0
-------------------------------
Time RLZ total   : 2.99648


Final Wtime(s)    : 3514.41
N Phrases         : 28481837
Compression ratio : 0.00870502
seconds per MiB   : 0.214502
seconds per GiB   : 219.651
MiB per second    : 4.66195
GiB per second    : 0.00455269
Input filename = /home/work/dvalenzu/Repos/RLZPP_experiments/RESULTS_SCALABILITY/KERNEL/17179869184/RLZPP_MEM4000_THREADS_1_D_99_files/compressed_file
Output filename = /home/work/dvalenzu/Repos/RLZPP_experiments/RESULTS_SCALABILITY/KERNEL/17179869184/RLZPP_MEM4000_THREADS_1_D_99_files/compressed_file.decompressed
Input size = 149551059 (142.6MiB)
Max disk use = 1152921504606846976 (1099511627776.0MiB)
RAM use = 3758096384 (3584.0MiB)
RAM use (excl. buffer) = 3724541952 (3552.0MiB)
Max segment size = 3724541952 (3552.0MiB)

Process part 1:
  Total parsing data decoded so far: 0.00%
  Total text decoded so far: 0 (0.00MiB)
  Permute phrases by source:   Permute phrases by source: time = 0.16s, I/O = 64.80MiB/s  Permute phrases by source: time = 0.20s, I/O = 108.64MiB/s  Permute phrases by source: time = 0.23s, I/O = 139.30MiB/s  Permute phrases by source: time = 0.38s, I/O = 115.51MiB/s  Permute phrases by source: time = 0.41s, I/O = 132.07MiB/s  Permute phrases by source: time = 0.45s, I/O = 146.29MiB/s  Permute phrases by source: time = 0.49s, I/O = 156.64MiB/s  Permute phrases by source: time = 0.52s, I/O = 167.25MiB/s  Permute phrases by source: time = 0.57s, I/O = 172.66MiB/s  Permute phrases by source: time = 0.61s, I/O = 177.68MiB/s  Permute phrases by source: time = 0.65s, I/O = 185.07MiB/s  Permute phrases by source: time = 0.68s, I/O = 191.69MiB/s  Permute phrases by source: time = 0.72s, I/O = 197.63MiB/s  Permute phrases by source: time = 0.76s, I/O = 203.00MiB/s  Permute phrases by source: time = 0.80s, I/O = 207.97MiB/s  Permute phrases by source: time = 0.84s, I/O = 212.60MiB/s  Permute phrases by source: time = 0.88s, I/O = 216.73MiB/s  Permute phrases by source: time = 0.92s, I/O = 220.61MiB/s  Permute phrases by source: time = 0.96s, I/O = 224.27MiB/s  Permute phrases by source: time = 1.00s, I/O = 227.71MiB/s  Permute phrases by source: time = 1.04s, I/O = 230.64MiB/s  Permute phrases by source: time = 1.24s, I/O = 202.34MiB/s  Permute phrases by source: time = 1.28s, I/O = 205.38MiB/s  Permute phrases by source: time = 1.32s, I/O = 208.24MiB/s  Permute phrases by source: time = 1.36s, I/O = 210.87MiB/s  Permute phrases by source: time = 1.40s, I/O = 213.45MiB/s  Permute phrases by source: time = 1.44s, I/O = 216.09MiB/s  Permute phrases by source: 100.0%, time = 1.44s, I/O = 216.53MiB/s
  Processing this part will decode 16384.0MiB of text (100.00% of parsing)
  Last part = TRUE
  Decode segment 1/5 [0..3724541952):
    Selfdecode segment:     Selfdecode segment: time = 0.04s, I/O = 135.43MiB/s    Selfdecode segment: time = 0.10s, I/O = 117.01MiB/s    Selfdecode segment: time = 0.16s, I/O = 107.89MiB/s    Selfdecode segment: time = 0.23s, I/O = 104.50MiB/s    Selfdecode segment: time = 0.29s, I/O = 101.14MiB/s    Selfdecode segment: time = 0.36s, I/O = 98.18MiB/s    Selfdecode segment: time = 0.43s, I/O = 96.23MiB/s    Selfdecode segment: time = 0.50s, I/O = 95.76MiB/s    Selfdecode segment: time = 0.56s, I/O = 95.19MiB/s    Selfdecode segment: time = 0.64s, I/O = 93.67MiB/s    Selfdecode segment: time = 0.71s, I/O = 92.64MiB/s    Selfdecode segment: time = 0.78s, I/O = 92.35MiB/s    Selfdecode segment: time = 0.85s, I/O = 91.57MiB/s    Selfdecode segment: time = 0.92s, I/O = 90.91MiB/s    Selfdecode segment: time = 1.00s, I/O = 90.56MiB/s    Selfdecode segment: time = 1.07s, I/O = 90.19MiB/s    Selfdecode segment: time = 1.15s, I/O = 89.86MiB/s    Selfdecode segment: time = 1.23s, I/O = 89.57MiB/s    Selfdecode segment: time = 1.31s, I/O = 89.34MiB/s    Selfdecode segment: time = 1.39s, I/O = 88.98MiB/s    Selfdecode segment: time = 1.47s, I/O = 88.56MiB/s    Selfdecode segment: time = 1.54s, I/O = 88.45MiB/s    Selfdecode segment: time = 1.62s, I/O = 88.22MiB/s    Selfdecode segment: time = 1.70s, I/O = 87.97MiB/s    Selfdecode segment: time = 1.78s, I/O = 87.65MiB/s    Selfdecode segment: time = 2.76s, I/O = 57.91MiB/s
    Distribute chunks:     Distribute chunks: time = 1.48s, I/O = 378.01MiB/s    Distribute chunks: time = 2.81s, I/O = 272.60MiB/s
    Write segment to disk: 31.07s, I/O = 114.34MiB/s
    Decode the nearby phrases for next segment: 0.18s
  Decode segment 2/5 [3724541952..7449083904):
    Load segment chunks:     Load segment chunks: 1.7%, time = 0.08s, I/O = 74.45MiB/s    Load segment chunks: 2.9%, time = 0.11s, I/O = 87.30MiB/s    Load segment chunks: 4.0%, time = 0.15s, I/O = 93.01MiB/s    Load segment chunks: 5.2%, time = 0.29s, I/O = 61.77MiB/s    Load segment chunks: 6.4%, time = 0.33s, I/O = 67.57MiB/s    Load segment chunks: 7.5%, time = 0.36s, I/O = 72.13MiB/s    Load segment chunks: 9.2%, time = 0.41s, I/O = 78.92MiB/s    Load segment chunks: 10.4%, time = 0.44s, I/O = 81.85MiB/s    Load segment chunks: 11.6%, time = 0.47s, I/O = 84.38MiB/s    Load segment chunks: 12.7%, time = 0.51s, I/O = 86.60MiB/s    Load segment chunks: 13.9%, time = 0.54s, I/O = 88.71MiB/s    Load segment chunks: 15.0%, time = 0.58s, I/O = 90.33MiB/s    Load segment chunks: 16.2%, time = 0.61s, I/O = 92.54MiB/s    Load segment chunks: 17.3%, time = 0.65s, I/O = 92.05MiB/s    Load segment chunks: 18.5%, time = 0.69s, I/O = 93.04MiB/s    Load segment chunks: 19.7%, time = 0.72s, I/O = 93.90MiB/s    Load segment chunks: 20.8%, time = 0.76s, I/O = 94.96MiB/s    Load segment chunks: 22.0%, time = 0.79s, I/O = 96.07MiB/s    Load segment chunks: 23.1%, time = 0.83s, I/O = 96.65MiB/s    Load segment chunks: 24.3%, time = 0.86s, I/O = 97.14MiB/s    Load segment chunks: 25.4%, time = 0.90s, I/O = 97.72MiB/s    Load segment chunks: 26.6%, time = 0.94s, I/O = 98.32MiB/s    Load segment chunks: 27.7%, time = 0.97s, I/O = 98.82MiB/s    Load segment chunks: 28.9%, time = 1.01s, I/O = 99.38MiB/s    Load segment chunks: 30.1%, time = 1.04s, I/O = 99.82MiB/s    Load segment chunks: 31.2%, time = 1.08s, I/O = 100.21MiB/s    Load segment chunks: 32.4%, time = 1.20s, I/O = 93.04MiB/s    Load segment chunks: 33.5%, time = 1.30s, I/O = 89.42MiB/s    Load segment chunks: 34.7%, time = 1.33s, I/O = 90.06MiB/s    Load segment chunks: 36.4%, time = 1.38s, I/O = 91.19MiB/s    Load segment chunks: 37.6%, time = 1.41s, I/O = 91.88MiB/s    Load segment chunks: 38.7%, time = 1.45s, I/O = 92.46MiB/s    Load segment chunks: 39.9%, time = 1.48s, I/O = 93.10MiB/s    Load segment chunks: 41.0%, time = 1.52s, I/O = 93.63MiB/s    Load segment chunks: 42.2%, time = 1.55s, I/O = 94.19MiB/s    Load segment chunks: 43.4%, time = 1.59s, I/O = 94.55MiB/s    Load segment chunks: 44.5%, time = 1.62s, I/O = 94.92MiB/s    Load segment chunks: 45.7%, time = 1.66s, I/O = 95.33MiB/s    Load segment chunks: 47.4%, time = 1.70s, I/O = 96.42MiB/s    Load segment chunks: 48.6%, time = 1.73s, I/O = 97.02MiB/s    Load segment chunks: 49.7%, time = 1.78s, I/O = 96.50MiB/s    Load segment chunks: 50.9%, time = 1.81s, I/O = 97.00MiB/s    Load segment chunks: 52.0%, time = 1.85s, I/O = 97.47MiB/s    Load segment chunks: 53.2%, time = 1.88s, I/O = 98.02MiB/s    Load segment chunks: 54.3%, time = 1.91s, I/O = 98.27MiB/s    Load segment chunks: 55.5%, time = 1.95s, I/O = 98.63MiB/s    Load segment chunks: 56.6%, time = 1.98s, I/O = 99.05MiB/s    Load segment chunks: 57.8%, time = 2.01s, I/O = 99.38MiB/s    Load segment chunks: 59.0%, time = 2.04s, I/O = 99.77MiB/s    Load segment chunks: 60.7%, time = 2.15s, I/O = 97.78MiB/s    Load segment chunks: 61.3%, time = 2.16s, I/O = 98.14MiB/s    Load segment chunks: 62.4%, time = 2.23s, I/O = 96.65MiB/s    Load segment chunks: 63.6%, time = 2.27s, I/O = 96.99MiB/s    Load segment chunks: 64.7%, time = 2.30s, I/O = 97.53MiB/s    Load segment chunks: 65.9%, time = 2.34s, I/O = 97.46MiB/s    Load segment chunks: 67.6%, time = 2.39s, I/O = 98.10MiB/s    Load segment chunks: 68.8%, time = 2.42s, I/O = 98.40MiB/s    Load segment chunks: 69.9%, time = 2.45s, I/O = 98.67MiB/s    Load segment chunks: 71.1%, time = 2.49s, I/O = 98.94MiB/s    Load segment chunks: 72.3%, time = 2.52s, I/O = 99.18MiB/s    Load segment chunks: 73.4%, time = 2.55s, I/O = 99.42MiB/s    Load segment chunks: 74.6%, time = 2.59s, I/O = 99.69MiB/s    Load segment chunks: 75.7%, time = 2.62s, I/O = 99.91MiB/s    Load segment chunks: 76.9%, time = 2.66s, I/O = 100.13MiB/s    Load segment chunks: 78.0%, time = 2.69s, I/O = 100.31MiB/s    Load segment chunks: 79.2%, time = 2.72s, I/O = 100.56MiB/s    Load segment chunks: 80.3%, time = 2.76s, I/O = 100.76MiB/s    Load segment chunks: 81.5%, time = 2.80s, I/O = 100.77MiB/s    Load segment chunks: 82.7%, time = 2.83s, I/O = 100.99MiB/s    Load segment chunks: 83.8%, time = 2.87s, I/O = 101.19MiB/s    Load segment chunks: 85.0%, time = 2.90s, I/O = 101.45MiB/s    Load segment chunks: 86.1%, time = 2.93s, I/O = 101.66MiB/s    Load segment chunks: 87.3%, time = 2.96s, I/O = 102.03MiB/s    Load segment chunks: 88.4%, time = 2.99s, I/O = 102.19MiB/s    Load segment chunks: 90.2%, time = 3.04s, I/O = 102.57MiB/s    Load segment chunks: 91.3%, time = 3.14s, I/O = 100.80MiB/s    Load segment chunks: 92.5%, time = 3.15s, I/O = 101.68MiB/s    Load segment chunks: 93.6%, time = 3.22s, I/O = 100.50MiB/s    Load segment chunks: 94.8%, time = 3.26s, I/O = 100.67MiB/s    Load segment chunks: 95.9%, time = 3.29s, I/O = 100.91MiB/s    Load segment chunks: 97.1%, time = 3.32s, I/O = 101.09MiB/s    Load segment chunks: 98.3%, time = 3.37s, I/O = 100.87MiB/s    Load segment chunks: 99.4%, time = 3.40s, I/O = 101.03MiB/s    Load segment chunks: 100.0%, time = 3.42s, I/O = 101.16MiB/s
    Selfdecode segment:     Selfdecode segment: time = 0.51s, I/O = 1.38MiB/s
    Distribute chunks:     Distribute chunks: time = 0.36s, I/O = 104.27MiB/s
    Write segment to disk: 29.31s, I/O = 121.20MiB/s
    Decode the nearby phrases for next segment: 0.10s
  Decode segment 3/5 [7449083904..11173625856):
    Load segment chunks:     Load segment chunks: 2.6%, time = 0.08s, I/O = 74.78MiB/s    Load segment chunks: 4.3%, time = 0.12s, I/O = 86.53MiB/s    Load segment chunks: 6.0%, time = 0.15s, I/O = 92.84MiB/s    Load segment chunks: 7.7%, time = 0.19s, I/O = 96.81MiB/s    Load segment chunks: 9.4%, time = 0.22s, I/O = 99.06MiB/s    Load segment chunks: 11.1%, time = 0.26s, I/O = 100.83MiB/s    Load segment chunks: 12.8%, time = 0.29s, I/O = 102.58MiB/s    Load segment chunks: 14.5%, time = 0.33s, I/O = 103.48MiB/s    Load segment chunks: 16.2%, time = 0.36s, I/O = 104.99MiB/s    Load segment chunks: 17.9%, time = 0.40s, I/O = 105.10MiB/s    Load segment chunks: 19.6%, time = 0.43s, I/O = 105.75MiB/s    Load segment chunks: 21.3%, time = 0.54s, I/O = 91.81MiB/s    Load segment chunks: 23.9%, time = 0.56s, I/O = 100.52MiB/s    Load segment chunks: 24.7%, time = 0.63s, I/O = 91.88MiB/s    Load segment chunks: 26.4%, time = 0.66s, I/O = 93.25MiB/s    Load segment chunks: 28.1%, time = 0.70s, I/O = 94.39MiB/s    Load segment chunks: 29.8%, time = 0.73s, I/O = 95.62MiB/s    Load segment chunks: 31.5%, time = 0.77s, I/O = 96.52MiB/s    Load segment chunks: 33.2%, time = 0.80s, I/O = 97.57MiB/s    Load segment chunks: 34.9%, time = 0.83s, I/O = 98.30MiB/s    Load segment chunks: 36.7%, time = 0.87s, I/O = 98.81MiB/s    Load segment chunks: 39.2%, time = 0.92s, I/O = 100.51MiB/s    Load segment chunks: 40.9%, time = 0.95s, I/O = 101.18MiB/s    Load segment chunks: 42.6%, time = 0.98s, I/O = 101.68MiB/s    Load segment chunks: 44.3%, time = 1.02s, I/O = 102.30MiB/s    Load segment chunks: 46.0%, time = 1.05s, I/O = 102.79MiB/s    Load segment chunks: 47.7%, time = 1.08s, I/O = 103.69MiB/s    Load segment chunks: 49.4%, time = 1.12s, I/O = 103.20MiB/s    Load segment chunks: 51.1%, time = 1.16s, I/O = 103.67MiB/s    Load segment chunks: 52.8%, time = 1.19s, I/O = 104.15MiB/s    Load segment chunks: 54.6%, time = 1.22s, I/O = 104.67MiB/s    Load segment chunks: 56.3%, time = 1.27s, I/O = 104.23MiB/s    Load segment chunks: 58.0%, time = 1.30s, I/O = 104.51MiB/s    Load segment chunks: 59.7%, time = 1.34s, I/O = 104.73MiB/s    Load segment chunks: 61.4%, time = 1.37s, I/O = 104.93MiB/s    Load segment chunks: 63.9%, time = 1.49s, I/O = 100.92MiB/s    Load segment chunks: 64.8%, time = 1.49s, I/O = 101.80MiB/s    Load segment chunks: 66.5%, time = 1.59s, I/O = 97.99MiB/s    Load segment chunks: 68.2%, time = 1.63s, I/O = 98.08MiB/s    Load segment chunks: 69.9%, time = 1.67s, I/O = 98.45MiB/s    Load segment chunks: 71.6%, time = 1.70s, I/O = 98.97MiB/s    Load segment chunks: 74.2%, time = 1.76s, I/O = 99.01MiB/s    Load segment chunks: 75.9%, time = 1.79s, I/O = 99.32MiB/s    Load segment chunks: 77.6%, time = 1.83s, I/O = 99.65MiB/s    Load segment chunks: 79.3%, time = 1.86s, I/O = 100.03MiB/s    Load segment chunks: 81.0%, time = 1.89s, I/O = 100.34MiB/s    Load segment chunks: 82.7%, time = 1.93s, I/O = 100.70MiB/s    Load segment chunks: 84.4%, time = 1.96s, I/O = 101.00MiB/s    Load segment chunks: 86.1%, time = 1.99s, I/O = 101.32MiB/s    Load segment chunks: 87.8%, time = 2.03s, I/O = 101.57MiB/s    Load segment chunks: 89.5%, time = 2.06s, I/O = 101.90MiB/s    Load segment chunks: 91.2%, time = 2.09s, I/O = 102.18MiB/s    Load segment chunks: 92.9%, time = 2.13s, I/O = 102.37MiB/s    Load segment chunks: 94.6%, time = 2.16s, I/O = 102.64MiB/s    Load segment chunks: 96.3%, time = 2.21s, I/O = 102.35MiB/s    Load segment chunks: 98.0%, time = 2.24s, I/O = 102.63MiB/s    Load segment chunks: 100.0%, time = 2.28s, I/O = 103.05MiB/s    Load segment chunks: 100.0%, time = 2.28s, I/O = 102.82MiB/s
    Selfdecode segment:     Selfdecode segment: time = 0.53s, I/O = 0.58MiB/s
    Distribute chunks:     Distribute chunks: time = 2.38s, I/O = 146.40MiB/s
    Write segment to disk: 34.19s, I/O = 103.89MiB/s
    Decode the nearby phrases for next segment: 0.26s
  Decode segment 4/5 [11173625856..14898167808):
    Load segment chunks:     Load segment chunks: 1.2%, time = 0.07s, I/O = 84.71MiB/s    Load segment chunks: 1.9%, time = 0.10s, I/O = 96.20MiB/s    Load segment chunks: 2.7%, time = 0.14s, I/O = 101.74MiB/s    Load segment chunks: 3.5%, time = 0.17s, I/O = 105.50MiB/s    Load segment chunks: 4.2%, time = 0.20s, I/O = 107.41MiB/s    Load segment chunks: 5.0%, time = 0.24s, I/O = 109.34MiB/s    Load segment chunks: 5.8%, time = 0.27s, I/O = 110.32MiB/s    Load segment chunks: 6.6%, time = 0.30s, I/O = 111.49MiB/s    Load segment chunks: 7.3%, time = 0.34s, I/O = 112.04MiB/s    Load segment chunks: 8.1%, time = 0.37s, I/O = 113.16MiB/s    Load segment chunks: 8.9%, time = 0.41s, I/O = 113.35MiB/s    Load segment chunks: 9.6%, time = 0.44s, I/O = 113.80MiB/s    Load segment chunks: 10.4%, time = 0.47s, I/O = 114.01MiB/s    Load segment chunks: 11.2%, time = 0.51s, I/O = 113.25MiB/s    Load segment chunks: 12.0%, time = 0.55s, I/O = 113.17MiB/s    Load segment chunks: 12.7%, time = 0.58s, I/O = 113.30MiB/s    Load segment chunks: 13.5%, time = 0.62s, I/O = 113.21MiB/s    Load segment chunks: 14.3%, time = 0.65s, I/O = 113.13MiB/s    Load segment chunks: 15.0%, time = 0.69s, I/O = 113.06MiB/s    Load segment chunks: 15.8%, time = 0.80s, I/O = 102.77MiB/s    Load segment chunks: 16.6%, time = 0.81s, I/O = 106.02MiB/s    Load segment chunks: 17.4%, time = 0.92s, I/O = 97.64MiB/s    Load segment chunks: 18.1%, time = 0.96s, I/O = 98.31MiB/s    Load segment chunks: 18.9%, time = 0.99s, I/O = 98.69MiB/s    Load segment chunks: 19.7%, time = 1.03s, I/O = 99.14MiB/s    Load segment chunks: 20.4%, time = 1.06s, I/O = 100.03MiB/s    Load segment chunks: 21.2%, time = 1.10s, I/O = 100.19MiB/s    Load segment chunks: 22.0%, time = 1.14s, I/O = 100.08MiB/s    Load segment chunks: 23.1%, time = 1.18s, I/O = 101.47MiB/s    Load segment chunks: 23.9%, time = 1.22s, I/O = 102.01MiB/s    Load segment chunks: 24.7%, time = 1.25s, I/O = 102.41MiB/s    Load segment chunks: 25.4%, time = 1.28s, I/O = 102.91MiB/s    Load segment chunks: 26.2%, time = 1.32s, I/O = 103.28MiB/s    Load segment chunks: 27.0%, time = 1.35s, I/O = 103.72MiB/s    Load segment chunks: 27.8%, time = 1.54s, I/O = 93.62MiB/s    Load segment chunks: 28.5%, time = 1.55s, I/O = 95.43MiB/s    Load segment chunks: 29.3%, time = 1.65s, I/O = 92.05MiB/s    Load segment chunks: 30.1%, time = 1.69s, I/O = 92.55MiB/s    Load segment chunks: 30.8%, time = 1.72s, I/O = 93.10MiB/s    Load segment chunks: 31.6%, time = 1.75s, I/O = 93.55MiB/s    Load segment chunks: 32.4%, time = 1.78s, I/O = 94.36MiB/s    Load segment chunks: 33.2%, time = 1.83s, I/O = 94.19MiB/s    Load segment chunks: 33.9%, time = 1.86s, I/O = 94.44MiB/s    Load segment chunks: 34.7%, time = 1.90s, I/O = 94.77MiB/s    Load segment chunks: 35.5%, time = 1.93s, I/O = 95.14MiB/s    Load segment chunks: 36.2%, time = 1.97s, I/O = 95.42MiB/s    Load segment chunks: 37.0%, time = 2.08s, I/O = 92.41MiB/s    Load segment chunks: 37.8%, time = 2.19s, I/O = 89.66MiB/s    Load segment chunks: 38.6%, time = 2.22s, I/O = 90.06MiB/s    Load segment chunks: 39.7%, time = 2.27s, I/O = 90.83MiB/s    Load segment chunks: 40.5%, time = 2.31s, I/O = 90.92MiB/s    Load segment chunks: 41.3%, time = 2.34s, I/O = 91.34MiB/s    Load segment chunks: 42.0%, time = 2.38s, I/O = 91.69MiB/s    Load segment chunks: 42.8%, time = 2.41s, I/O = 92.01MiB/s    Load segment chunks: 43.6%, time = 2.45s, I/O = 92.37MiB/s    Load segment chunks: 47.4%, time = 2.47s, I/O = 99.70MiB/s    Load segment chunks: 48.6%, time = 2.48s, I/O = 101.64MiB/s    Load segment chunks: 49.4%, time = 2.49s, I/O = 103.00MiB/s    Load segment chunks: 50.5%, time = 2.49s, I/O = 105.17MiB/s    Load segment chunks: 51.3%, time = 2.49s, I/O = 106.62MiB/s    Load segment chunks: 52.1%, time = 2.50s, I/O = 108.08MiB/s    Load segment chunks: 52.8%, time = 2.50s, I/O = 109.53MiB/s    Load segment chunks: 53.6%, time = 2.50s, I/O = 111.01MiB/s    Load segment chunks: 54.4%, time = 2.51s, I/O = 112.48MiB/s    Load segment chunks: 55.1%, time = 2.51s, I/O = 113.93MiB/s    Load segment chunks: 55.9%, time = 2.51s, I/O = 115.40MiB/s    Load segment chunks: 57.1%, time = 2.52s, I/O = 117.65MiB/s    Load segment chunks: 57.8%, time = 2.52s, I/O = 119.11MiB/s    Load segment chunks: 58.6%, time = 2.52s, I/O = 120.56MiB/s    Load segment chunks: 59.4%, time = 2.52s, I/O = 122.01MiB/s    Load segment chunks: 60.1%, time = 2.53s, I/O = 123.46MiB/s    Load segment chunks: 60.9%, time = 2.53s, I/O = 124.91MiB/s    Load segment chunks: 61.7%, time = 2.53s, I/O = 126.35MiB/s    Load segment chunks: 62.5%, time = 2.54s, I/O = 127.79MiB/s    Load segment chunks: 63.2%, time = 2.54s, I/O = 129.22MiB/s    Load segment chunks: 64.0%, time = 2.54s, I/O = 130.65MiB/s    Load segment chunks: 64.8%, time = 2.54s, I/O = 132.08MiB/s    Load segment chunks: 65.5%, time = 2.55s, I/O = 133.51MiB/s    Load segment chunks: 66.3%, time = 2.55s, I/O = 134.93MiB/s    Load segment chunks: 67.1%, time = 2.55s, I/O = 136.35MiB/s    Load segment chunks: 67.9%, time = 2.56s, I/O = 137.77MiB/s    Load segment chunks: 68.6%, time = 2.56s, I/O = 139.17MiB/s    Load segment chunks: 69.4%, time = 2.56s, I/O = 140.58MiB/s    Load segment chunks: 70.2%, time = 2.56s, I/O = 141.98MiB/s    Load segment chunks: 70.9%, time = 2.57s, I/O = 143.39MiB/s    Load segment chunks: 71.7%, time = 2.57s, I/O = 144.79MiB/s    Load segment chunks: 72.5%, time = 2.57s, I/O = 146.19MiB/s    Load segment chunks: 73.3%, time = 2.57s, I/O = 147.58MiB/s    Load segment chunks: 74.0%, time = 2.58s, I/O = 148.97MiB/s    Load segment chunks: 74.8%, time = 2.58s, I/O = 150.35MiB/s    Load segment chunks: 75.6%, time = 2.58s, I/O = 151.74MiB/s    Load segment chunks: 76.7%, time = 2.59s, I/O = 153.87MiB/s    Load segment chunks: 77.5%, time = 2.59s, I/O = 155.25MiB/s    Load segment chunks: 78.3%, time = 2.59s, I/O = 156.62MiB/s    Load segment chunks: 79.0%, time = 2.60s, I/O = 157.99MiB/s    Load segment chunks: 79.8%, time = 2.60s, I/O = 159.36MiB/s    Load segment chunks: 80.6%, time = 2.60s, I/O = 160.73MiB/s    Load segment chunks: 81.4%, time = 2.60s, I/O = 162.09MiB/s    Load segment chunks: 82.1%, time = 2.61s, I/O = 163.45MiB/s    Load segment chunks: 82.9%, time = 2.61s, I/O = 164.81MiB/s    Load segment chunks: 83.7%, time = 2.61s, I/O = 166.16MiB/s    Load segment chunks: 84.4%, time = 2.61s, I/O = 167.51MiB/s    Load segment chunks: 85.2%, time = 2.62s, I/O = 168.86MiB/s    Load segment chunks: 86.0%, time = 2.62s, I/O = 170.21MiB/s    Load segment chunks: 86.8%, time = 2.62s, I/O = 171.55MiB/s    Load segment chunks: 87.5%, time = 2.63s, I/O = 172.89MiB/s    Load segment chunks: 88.3%, time = 2.63s, I/O = 174.23MiB/s    Load segment chunks: 89.1%, time = 2.63s, I/O = 175.55MiB/s    Load segment chunks: 89.8%, time = 2.63s, I/O = 176.89MiB/s    Load segment chunks: 90.6%, time = 2.64s, I/O = 178.21MiB/s    Load segment chunks: 91.4%, time = 2.64s, I/O = 179.53MiB/s    Load segment chunks: 92.2%, time = 2.64s, I/O = 180.86MiB/s    Load segment chunks: 92.9%, time = 2.65s, I/O = 182.18MiB/s    Load segment chunks: 93.7%, time = 2.65s, I/O = 183.50MiB/s    Load segment chunks: 94.5%, time = 2.65s, I/O = 184.81MiB/s    Load segment chunks: 95.2%, time = 2.65s, I/O = 186.12MiB/s    Load segment chunks: 96.4%, time = 2.66s, I/O = 188.15MiB/s    Load segment chunks: 97.2%, time = 2.66s, I/O = 189.45MiB/s    Load segment chunks: 97.9%, time = 2.66s, I/O = 190.75MiB/s    Load segment chunks: 98.7%, time = 2.67s, I/O = 192.05MiB/s    Load segment chunks: 99.5%, time = 2.67s, I/O = 193.34MiB/s    Load segment chunks: 100.0%, time = 2.67s, I/O = 194.16MiB/s    Load segment chunks: 100.0%, time = 2.67s, I/O = 193.97MiB/s    Load segment chunks: 100.0%, time = 2.68s, I/O = 193.77MiB/s    Load segment chunks: 100.0%, time = 2.68s, I/O = 193.57MiB/s    Load segment chunks: 100.0%, time = 2.68s, I/O = 193.52MiB/s
    Selfdecode segment:     Selfdecode segment: time = 0.33s, I/O = 0.54MiB/s
    Distribute chunks:     Distribute chunks: time = 0.00s, I/O = 310.77MiB/s
    Write segment to disk: 33.74s, I/O = 105.28MiB/s
    Decode the nearby phrases for next segment: 0.12s
  Decode segment 5/5 [14898167808..17179869184):
    Load segment chunks:     Load segment chunks: 14.2%, time = 0.07s, I/O = 84.60MiB/s    Load segment chunks: 23.7%, time = 0.12s, I/O = 86.12MiB/s    Load segment chunks: 33.1%, time = 0.15s, I/O = 90.47MiB/s    Load segment chunks: 42.6%, time = 0.21s, I/O = 87.78MiB/s    Load segment chunks: 52.1%, time = 0.25s, I/O = 89.13MiB/s    Load segment chunks: 61.5%, time = 0.29s, I/O = 88.84MiB/s    Load segment chunks: 75.7%, time = 0.35s, I/O = 90.52MiB/s    Load segment chunks: 85.2%, time = 0.39s, I/O = 91.69MiB/s    Load segment chunks: 94.6%, time = 0.43s, I/O = 93.31MiB/s    Load segment chunks: 100.0%, time = 0.54s, I/O = 78.44MiB/s
    Selfdecode segment:     Selfdecode segment: time = 0.28s, I/O = 0.04MiB/s
    Write segment to disk: 20.76s, I/O = 104.82MiB/s
    Decode the nearby phrases for next segment: 0.00s


Computation finished. Summary:
  I/O volume = 20080081917 (1.17B/B)
  number of decoded phrases: 28481837
  average phrase length = 603.19
  length of decoded text: 17179869184 (16384.00MiB)
  elapsed time: 173.77s (0.0106s/MiB)
  speed: 94.29MiB/s
Decompression t(s)    : 173.800879002
