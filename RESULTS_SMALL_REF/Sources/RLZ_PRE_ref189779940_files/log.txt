RLZ parser intitialized with:
Ignore     : 0
Ref        : 189779940
Partitions : 32
Memory(MB) : 30000
RLZ parsing...
Building SA...
We will read blocks of size: 210866607
Reading block 0/1
Block read in 0.063959 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
Reading block 1/1
Block read in 0.006771 (s)
Waiting for all tasks to be done...
Done.

*
Starting parallel parsing in block...Parallel parse in 9.78662 (s)
Writing phrases... 
Phrases written  in 0.027517 (s)
Input succesfully compressed, output written in: ./RESULTS_SMALL_REF/Sources/RLZ_PRE_ref189779940_files/compressed_file
Parsing performed in 1 passes

0-th level report: 

Factors skipped      : 0
Factors 77'ed in ref : 10541591
Factors RLZ          : 1593950
Factors Total        : 12135541
--------------------------------------------
Time SA          : 12.9436
Time LZ77 of ref : 5.28089
Time RLZ parse   : 9.82184
-------------------------------
Time RLZ total   : 28.0464


Final Wtime(s)    : 29.1841
N Phrases         : 12135541
Compression ratio : 0.920813
seconds per MiB   : 0.145124
seconds per GiB   : 148.607
MiB per second    : 6.89066
GiB per second    : 0.00672916
	Command being timed: "./ext/RLZPP/rlz_plusplus /home/work/dvalenzu/data//Sources -o ./RESULTS_SMALL_REF/Sources/RLZ_PRE_ref189779940_files/compressed_file -s -l 189779940 -t16 -c32 -m30000 -d0 -v4"
	User time (seconds): 57.34
	System time (seconds): 0.69
	Percent of CPU this job got: 198%
	Elapsed (wall clock) time (h:mm:ss or m:ss): 0:29.19
	Average shared text size (kbytes): 0
	Average unshared data size (kbytes): 0
	Average stack size (kbytes): 0
	Average total size (kbytes): 0
	Maximum resident set size (kbytes): 1680364
	Average resident set size (kbytes): 0
	Major (requiring I/O) page faults: 0
	Minor (reclaiming a frame) page faults: 443498
	Voluntary context switches: 187
	Involuntary context switches: 4848
	Swaps: 0
	File system inputs: 8
	File system outputs: 496832
	Socket messages sent: 0
	Socket messages received: 0
	Signals delivered: 0
	Page size (bytes): 4096
	Exit status: 0
