RLZ parser intitialized with:
Ignore     : 0
Ref        : 42173320
Partitions : 32
Memory(MB) : 30000
RLZ parsing...
Building SA...
We will read blocks of size: 210866607
Reading block 0/1
Block read in 0.01472 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
Reading block 1/1
Block read in 0.052593 (s)
Waiting for all tasks to be done...
Done.

*
Starting parallel parsing in block...Parallel parse in 69.9651 (s)
Writing phrases... 
Phrases written  in 0.337766 (s)
Input succesfully compressed, output written in: ./RESULTS_SMALL_REF/Sources/RLZ_PRE_ref42173320_files/compressed_file
Parsing performed in 1 passes

0-th level report: 

Factors skipped      : 0
Factors 77'ed in ref : 2690028
Factors RLZ          : 19148515
Factors Total        : 21838543
--------------------------------------------
Time SA          : 2.47788
Time LZ77 of ref : 1.06862
Time RLZ parse   : 70.3636
-------------------------------
Time RLZ total   : 73.9101


Final Wtime(s)    : 76.457
N Phrases         : 21838543
Compression ratio : 1.65705
seconds per MiB   : 0.380198
seconds per GiB   : 389.322
MiB per second    : 2.63021
GiB per second    : 0.00256857
	Command being timed: "./ext/RLZPP/rlz_plusplus /home/work/dvalenzu/data//Sources -o ./RESULTS_SMALL_REF/Sources/RLZ_PRE_ref42173320_files/compressed_file -s -l 42173320 -t16 -c32 -m30000 -d0 -v4"
	User time (seconds): 283.09
	System time (seconds): 1.41
	Percent of CPU this job got: 372%
	Elapsed (wall clock) time (h:mm:ss or m:ss): 1:16.47
	Average shared text size (kbytes): 0
	Average unshared data size (kbytes): 0
	Average stack size (kbytes): 0
	Average total size (kbytes): 0
	Maximum resident set size (kbytes): 1176416
	Average resident set size (kbytes): 0
	Major (requiring I/O) page faults: 0
	Minor (reclaiming a frame) page faults: 386122
	Voluntary context switches: 506
	Involuntary context switches: 33645
	Swaps: 0
	File system inputs: 16
	File system outputs: 893176
	Socket messages sent: 0
	Socket messages received: 0
	Signals delivered: 0
	Page size (bytes): 4096
	Exit status: 0
