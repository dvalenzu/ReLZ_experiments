set encoding iso_8859_1
#set terminal postscript enhanced color        # gnuplot recommends setting terminal before output
set terminal postscript eps color colortext
set key font ",18"
set title font ",18"
set output "Mem.eps" # The output filename; to be set after setting
set key right top Left
#RLZ++
set style line 1 lt 2 pt 8 ps 2 lw 2 lc "blue"
#RLZ
set style line 2 lt 2 pt 2 ps 2 lw 2 lc "dark-green"
#KKP3
set style line 3 lw 3 lt 2 dashtype 2 lc "magenta"
#IMLZ
set style line 4 lw 3 lt 2 dashtype 3 lc "orange"
#EMLZ
set style line 5 lw 3 lt 2 dashtype 5 lc "brown"
#
#
set title "Sources"
plot  "summaryRLZ_PLUS_MEM.txt" with points ls 1 title "RLZ++",\
      "./KKP3_files/mem.txt" using (0):($1):(210866607):(0)     with vectors nohead ls 3 title "KKP3",\
      "./IMLZscan_files/mem.txt" using (0):($1):(210866607):(0) with vectors nohead ls 4 title "LZscan",\
      "./EMLZscan_files/mem.txt" using (0):($1):(210866607):(0) with vectors nohead ls 5 title "EMLZscan",\
