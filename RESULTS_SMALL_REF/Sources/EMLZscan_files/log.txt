Text filename = /home/work/dvalenzu/data/Sources
Output filename = /home/work/dvalenzu/Repos/RLZPP_experiments/RESULTS_SMALL_REF/Sources/EMLZscan_files/compressed.lz
RAM use = 31457280000 (30000.00MiB)
Max block size = 898779428 (857.14MiB)
sizeof(textoff_t) = 6
sizeof(blockoff_t) = 5

Process block [0..210866607):
  Read block: 1.62s
  Compute SA of block: 14.80s
  Parse block: 6.52s
  Current progress: 0.0%, elapsed = 23s, z = 11598459, total I/O vol = 1.88n


Computation finished. Summary:
  elapsed time = 23.43s (0.116s/MiB of text)
  speed = 8.58MiB of text/s
  I/O volume = 401707433bytes (1.91bytes/input symbol)
  Number of phrases = 11598459
  Average phrase length = 18.181
Final Wtime(s) : 23.425
N Phrases      : 11598459
	Command being timed: "./ext/EM-LZscan-0.2/src/emlz_parser -o ./RESULTS_SMALL_REF/Sources/EMLZscan_files/compressed.lz -m30000 /home/work/dvalenzu/data//Sources"
	User time (seconds): 20.84
	System time (seconds): 1.29
	Percent of CPU this job got: 94%
	Elapsed (wall clock) time (h:mm:ss or m:ss): 0:23.42
	Average shared text size (kbytes): 0
	Average unshared data size (kbytes): 0
	Average stack size (kbytes): 0
	Average total size (kbytes): 0
	Maximum resident set size (kbytes): 3540588
	Average resident set size (kbytes): 0
	Major (requiring I/O) page faults: 0
	Minor (reclaiming a frame) page faults: 1091485
	Voluntary context switches: 260
	Involuntary context switches: 46
	Swaps: 0
	File system inputs: 411856
	File system outputs: 378168
	Socket messages sent: 0
	Socket messages received: 0
	Signals delivered: 0
	Page size (bytes): 4096
	Exit status: 0
