RLZ parser intitialized with:
Ignore     : 0
Ref        : 147606620
Partitions : 32
Memory(MB) : 30000
RLZ parsing...
Building SA...
We will read blocks of size: 210866607
Reading block 0/1
Block read in 0.050379 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
Reading block 1/1
Block read in 0.020127 (s)
Waiting for all tasks to be done...
Done.

*
Starting parallel parsing in block...Parallel parse in 33.71 (s)
Writing phrases... 
Phrases written  in 0.109183 (s)
Input succesfully compressed, output written in: ./RESULTS_SMALL_REF/Sources/RLZ_PRE_ref147606620_files/compressed_file
Parsing performed in 1 passes

0-th level report: 

Factors skipped      : 0
Factors 77'ed in ref : 8524066
Factors RLZ          : 6356270
Factors Total        : 14880336
--------------------------------------------
Time SA          : 9.86423
Time LZ77 of ref : 4.04219
Time RLZ parse   : 33.8422
-------------------------------
Time RLZ total   : 47.7487


Final Wtime(s)    : 49.2319
N Phrases         : 14880336
Compression ratio : 1.12908
seconds per MiB   : 0.244816
seconds per GiB   : 250.691
MiB per second    : 4.08471
GiB per second    : 0.00398897
	Command being timed: "./ext/RLZPP/rlz_plusplus /home/work/dvalenzu/data//Sources -o ./RESULTS_SMALL_REF/Sources/RLZ_PRE_ref147606620_files/compressed_file -s -l 147606620 -t16 -c32 -m30000 -d0 -v4"
	User time (seconds): 147.91
	System time (seconds): 0.81
	Percent of CPU this job got: 302%
	Elapsed (wall clock) time (h:mm:ss or m:ss): 0:49.24
	Average shared text size (kbytes): 0
	Average unshared data size (kbytes): 0
	Average stack size (kbytes): 0
	Average total size (kbytes): 0
	Maximum resident set size (kbytes): 1309780
	Average resident set size (kbytes): 0
	Major (requiring I/O) page faults: 0
	Minor (reclaiming a frame) page faults: 413758
	Voluntary context switches: 247
	Involuntary context switches: 16413
	Swaps: 0
	File system inputs: 16
	File system outputs: 609280
	Socket messages sent: 0
	Socket messages received: 0
	Signals delivered: 0
	Page size (bytes): 4096
	Exit status: 0
