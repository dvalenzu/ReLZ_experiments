RLZ parser intitialized with:
Ignore     : 0
Ref        : 10543330
Partitions : 32
Memory(MB) : 30000
RLZ parsing...
Building SA...
We will read blocks of size: 210866607
Reading block 0/1
Block read in 0.003712 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
Reading block 1/1
Block read in 0.063874 (s)
Waiting for all tasks to be done...
Done.

*
Starting parallel parsing in block...Parallel parse in 49.1811 (s)
Writing phrases... 
Phrases written  in 0.495852 (s)
Packing phrases...
Recursive call to next depth...

--- computing reference lenght based on memory limit:
--- With 30000MB of  memory, the reference length will be: 1179648000 symbols

--- reference_len is longer than the text are to be parsed: adjusting it.RLZ parser intitialized with:
Ignore     : 727856
Ref        : 27234018
Partitions : 32
Memory(MB) : 30000

Starting recursion depth 1
RLZ Parsing...
Building SA...
We will read blocks of size: 27961874
Reading block 0/0
Block read in 0.069759 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
ReParsing...
Safe bound for bits per factor: 8
IM Random Access to sums file will use at most (MB): 26
 Random Access to Sum Files IM (delta compressed)./RESULTS_SMALL_REF/Sources/RLZ_PLUS_ref10543330_files/compressed_file.TMP.rlztmp.sums
Input succesfully compressed, output written in: ./RESULTS_SMALL_REF/Sources/RLZ_PLUS_ref10543330_files/compressed_file
Parsing performed in 2 passes

0-th level report: 

Factors skipped      : 0
Factors 77'ed in ref : 727856
Factors RLZ          : 27234018
Factors Total        : 27961874
--------------------------------------------
Time SA          : 0.495072
Time LZ77 of ref : 0.226783
Time RLZ parse   : 49.7554
-------------------------------
Time RLZ total   : 50.4772
--------------------------------------
Rlz TOTAL time 	: 50.4872
Pack      time 	: 0.855599
Recursion time 	: 18.6723
ReParse   time 	: 2.05095
Encode    time 	: 1.56186
--------------------------------------
TOTAL     time 	: 73.6279

1-th level report: 

Factors skipped      : 727856
Factors 77'ed in ref : 15445215
Factors RLZ          : 0
Factors Total        : 16173071
--------------------------------------------
Time SA          : 16.066
Time LZ77 of ref : 2.57521
Time RLZ parse   : 1.00001e-06
-------------------------------
Time RLZ total   : 18.6412


Final Wtime(s)    : 73.6279
N Phrases         : 16173071
Compression ratio : 1.22717
seconds per MiB   : 0.366129
seconds per GiB   : 374.916
MiB per second    : 2.73128
GiB per second    : 0.00266726
	Command being timed: "./ext/RLZPP/rlz_plusplus /home/work/dvalenzu/data//Sources -o ./RESULTS_SMALL_REF/Sources/RLZ_PLUS_ref10543330_files/compressed_file -s -l 10543330 -t16 -c32 -m30000 -d1 -v4"
	User time (seconds): 217.15
	System time (seconds): 2.35
	Percent of CPU this job got: 298%
	Elapsed (wall clock) time (h:mm:ss or m:ss): 1:13.64
	Average shared text size (kbytes): 0
	Average unshared data size (kbytes): 0
	Average stack size (kbytes): 0
	Average total size (kbytes): 0
	Maximum resident set size (kbytes): 1354952
	Average resident set size (kbytes): 0
	Major (requiring I/O) page faults: 0
	Minor (reclaiming a frame) page faults: 790589
	Voluntary context switches: 1056
	Involuntary context switches: 24263
	Swaps: 0
	File system inputs: 16
	File system outputs: 1951384
	Socket messages sent: 0
	Socket messages received: 0
	Signals delivered: 0
	Page size (bytes): 4096
	Exit status: 0
