RLZ parser intitialized with:
Ignore     : 0
Ref        : 28180908
Partitions : 32
Memory(MB) : 30000
RLZ parsing...
Building SA...
We will read blocks of size: 46968181
Reading block 0/1
Block read in 0.009811 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
Reading block 1/1
Block read in 0.006174 (s)
Waiting for all tasks to be done...
Done.

*
Starting parallel parsing in block...Parallel parse in 0.976853 (s)
Writing phrases... 
Phrases written  in 0.004754 (s)
Packing phrases...
Recursive call to next depth...

--- computing reference lenght based on memory limit:
--- With 30000MB of  memory, the reference length will be: 1179648000 symbols

--- reference_len is longer than the text are to be parsed: adjusting it.RLZ parser intitialized with:
Ignore     : 144495
Ref        : 244499
Partitions : 32
Memory(MB) : 30000

Starting recursion depth 1
RLZ Parsing...
Building SA...
We will read blocks of size: 388994
Reading block 0/0
Block read in 0.000633 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
ReParsing...
Safe bound for bits per factor: 14
IM Random Access to sums file will use at most (MB): 0
 Random Access to Sum Files IM (delta compressed)./RESULTS_SMALL_REF/Leaders/RLZ_PLUS_ref28180908_files/compressed_file.TMP.rlztmp.sums
Input succesfully compressed, output written in: ./RESULTS_SMALL_REF/Leaders/RLZ_PLUS_ref28180908_files/compressed_file
Parsing performed in 2 passes

0-th level report: 

Factors skipped      : 0
Factors 77'ed in ref : 144495
Factors RLZ          : 244499
Factors Total        : 388994
--------------------------------------------
Time SA          : 1.14775
Time LZ77 of ref : 0.471224
Time RLZ parse   : 0.988019
-------------------------------
Time RLZ total   : 2.60699
--------------------------------------
Rlz TOTAL time 	: 2.61393
Pack      time 	: 0.013784
Recursion time 	: 0.106314
ReParse   time 	: 0.02417
Encode    time 	: 0.009941
--------------------------------------
TOTAL     time 	: 2.76816

1-th level report: 

Factors skipped      : 144495
Factors 77'ed in ref : 41097
Factors RLZ          : 0
Factors Total        : 185592
--------------------------------------------
Time SA          : 0.098642
Time LZ77 of ref : 0.003783
Time RLZ parse   : 9.99891e-07
-------------------------------
Time RLZ total   : 0.102426


Final Wtime(s)    : 2.76816
N Phrases         : 185592
Compression ratio : 0.0632231
seconds per MiB   : 0.0617998
seconds per GiB   : 63.283
MiB per second    : 16.1813
GiB per second    : 0.015802
	Command being timed: "./ext/RLZPP/rlz_plusplus /home/work/dvalenzu/data//Leaders -o ./RESULTS_SMALL_REF/Leaders/RLZ_PLUS_ref28180908_files/compressed_file -s -l 28180908 -t16 -c32 -m30000 -d1 -v4"
	User time (seconds): 5.55
	System time (seconds): 0.07
	Percent of CPU this job got: 203%
	Elapsed (wall clock) time (h:mm:ss or m:ss): 0:02.77
	Average shared text size (kbytes): 0
	Average unshared data size (kbytes): 0
	Average stack size (kbytes): 0
	Average total size (kbytes): 0
	Maximum resident set size (kbytes): 252492
	Average resident set size (kbytes): 0
	Major (requiring I/O) page faults: 0
	Minor (reclaiming a frame) page faults: 74829
	Voluntary context switches: 114
	Involuntary context switches: 437
	Swaps: 0
	File system inputs: 0
	File system outputs: 25440
	Socket messages sent: 0
	Socket messages received: 0
	Signals delivered: 0
	Page size (bytes): 4096
	Exit status: 0
