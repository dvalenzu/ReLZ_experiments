RLZ parser intitialized with:
Ignore     : 0
Ref        : 9393636
Partitions : 32
Memory(MB) : 30000
RLZ parsing...
Building SA...
We will read blocks of size: 46968181
Reading block 0/1
Block read in 0.0033 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
Reading block 1/1
Block read in 0.012374 (s)
Waiting for all tasks to be done...
Done.

*
Starting parallel parsing in block...Parallel parse in 1.48628 (s)
Writing phrases... 
Phrases written  in 0.013905 (s)
Packing phrases...
Recursive call to next depth...

--- computing reference lenght based on memory limit:
--- With 30000MB of  memory, the reference length will be: 1179648000 symbols

--- reference_len is longer than the text are to be parsed: adjusting it.RLZ parser intitialized with:
Ignore     : 112648
Ref        : 703413
Partitions : 32
Memory(MB) : 30000

Starting recursion depth 1
RLZ Parsing...
Building SA...
We will read blocks of size: 816061
Reading block 0/0
Block read in 0.001934 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
ReParsing...
Safe bound for bits per factor: 13
IM Random Access to sums file will use at most (MB): 1
 Random Access to Sum Files IM (delta compressed)./RESULTS_SMALL_REF/Leaders/RLZ_PLUS_ref9393636_files/compressed_file.TMP.rlztmp.sums
Input succesfully compressed, output written in: ./RESULTS_SMALL_REF/Leaders/RLZ_PLUS_ref9393636_files/compressed_file
Parsing performed in 2 passes

0-th level report: 

Factors skipped      : 0
Factors 77'ed in ref : 112648
Factors RLZ          : 703413
Factors Total        : 816061
--------------------------------------------
Time SA          : 0.440176
Time LZ77 of ref : 0.14913
Time RLZ parse   : 1.51291
-------------------------------
Time RLZ total   : 2.10222
--------------------------------------
Rlz TOTAL time 	: 2.10639
Pack      time 	: 0.028169
Recursion time 	: 0.336086
ReParse   time 	: 0.045824
Encode    time 	: 0.010526
--------------------------------------
TOTAL     time 	: 2.52701

1-th level report: 

Factors skipped      : 112648
Factors 77'ed in ref : 86648
Factors RLZ          : 0
Factors Total        : 199296
--------------------------------------------
Time SA          : 0.321186
Time LZ77 of ref : 0.01063
Time RLZ parse   : 1.00001e-06
-------------------------------
Time RLZ total   : 0.331817


Final Wtime(s)    : 2.52701
N Phrases         : 199296
Compression ratio : 0.0678914
seconds per MiB   : 0.0564161
seconds per GiB   : 57.7701
MiB per second    : 17.7254
GiB per second    : 0.01731
	Command being timed: "./ext/RLZPP/rlz_plusplus /home/work/dvalenzu/data//Leaders -o ./RESULTS_SMALL_REF/Leaders/RLZ_PLUS_ref9393636_files/compressed_file -s -l 9393636 -t16 -c32 -m30000 -d1 -v4"
	User time (seconds): 6.65
	System time (seconds): 0.11
	Percent of CPU this job got: 267%
	Elapsed (wall clock) time (h:mm:ss or m:ss): 0:02.53
	Average shared text size (kbytes): 0
	Average unshared data size (kbytes): 0
	Average stack size (kbytes): 0
	Average total size (kbytes): 0
	Maximum resident set size (kbytes): 119052
	Average resident set size (kbytes): 0
	Major (requiring I/O) page faults: 0
	Minor (reclaiming a frame) page faults: 50361
	Voluntary context switches: 203
	Involuntary context switches: 623
	Swaps: 0
	File system inputs: 0
	File system outputs: 43224
	Socket messages sent: 0
	Socket messages received: 0
	Signals delivered: 0
	Page size (bytes): 4096
	Exit status: 0
