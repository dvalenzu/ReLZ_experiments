RLZ parser intitialized with:
Ignore     : 0
Ref        : 188743680
Partitions : 32
Memory(MB) : 30000
RLZ parsing...
Building SA...
We will read blocks of size: 209715200
Reading block 0/1
Block read in 0.065684 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
Reading block 1/1
Block read in 0.007105 (s)
Waiting for all tasks to be done...
Done.

*
Starting parallel parsing in block...Parallel parse in 9.96146 (s)
Writing phrases... 
Phrases written  in 0.021278 (s)
Packing phrases...
Recursive call to next depth...

--- computing reference lenght based on memory limit:
--- With 30000MB of  memory, the reference length will be: 1179648000 symbols

--- reference_len is longer than the text are to be parsed: adjusting it.RLZ parser intitialized with:
Ignore     : 12930841
Ref        : 1181660
Partitions : 32
Memory(MB) : 30000

Starting recursion depth 1
RLZ Parsing...
Building SA...
We will read blocks of size: 14112501
Reading block 0/0
Block read in 0.003148 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
ReParsing...
Safe bound for bits per factor: 11
IM Random Access to sums file will use at most (MB): 18
 Random Access to Sum Files IM (delta compressed)./RESULTS_SMALL_REF/English/RLZ_PLUS_ref188743680_files/compressed_file.TMP.rlztmp.sums
Input succesfully compressed, output written in: ./RESULTS_SMALL_REF/English/RLZ_PLUS_ref188743680_files/compressed_file
Parsing performed in 2 passes

0-th level report: 

Factors skipped      : 0
Factors 77'ed in ref : 12930841
Factors RLZ          : 1181660
Factors Total        : 14112501
--------------------------------------------
Time SA          : 18.7417
Time LZ77 of ref : 6.82867
Time RLZ parse   : 9.99058
-------------------------------
Time RLZ total   : 35.561
--------------------------------------
Rlz TOTAL time 	: 35.5954
Pack      time 	: 0.402193
Recursion time 	: 0.832039
ReParse   time 	: 0.828472
Encode    time 	: 2.24353
--------------------------------------
TOTAL     time 	: 39.9017

1-th level report: 

Factors skipped      : 12930841
Factors 77'ed in ref : 1108499
Factors RLZ          : 0
Factors Total        : 14039340
--------------------------------------------
Time SA          : 0.427739
Time LZ77 of ref : 0.097719
Time RLZ parse   : 1.00001e-06
-------------------------------
Time RLZ total   : 0.525459


Final Wtime(s)    : 39.9017
N Phrases         : 14039340
Compression ratio : 1.07112
seconds per MiB   : 0.199509
seconds per GiB   : 204.297
MiB per second    : 5.01232
GiB per second    : 0.00489484
	Command being timed: "./ext/RLZPP/rlz_plusplus /home/work/dvalenzu/data//English -o ./RESULTS_SMALL_REF/English/RLZ_PLUS_ref188743680_files/compressed_file -s -l 188743680 -t16 -c32 -m30000 -d1 -v4"
	User time (seconds): 67.24
	System time (seconds): 1.04
	Percent of CPU this job got: 171%
	Elapsed (wall clock) time (h:mm:ss or m:ss): 0:39.90
	Average shared text size (kbytes): 0
	Average unshared data size (kbytes): 0
	Average stack size (kbytes): 0
	Average total size (kbytes): 0
	Maximum resident set size (kbytes): 1671084
	Average resident set size (kbytes): 0
	Major (requiring I/O) page faults: 0
	Minor (reclaiming a frame) page faults: 537937
	Voluntary context switches: 459
	Involuntary context switches: 4517
	Swaps: 0
	File system inputs: 8
	File system outputs: 1315288
	Socket messages sent: 0
	Socket messages received: 0
	Signals delivered: 0
	Page size (bytes): 4096
	Exit status: 0
