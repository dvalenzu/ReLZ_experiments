RLZ parser intitialized with:
Ignore     : 0
Ref        : 30961710
Partitions : 32
Memory(MB) : 30000
RLZ parsing...
Building SA...
We will read blocks of size: 154808555
Reading block 0/1
Block read in 0.010985 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
Reading block 1/1
Block read in 0.03725 (s)
Waiting for all tasks to be done...
Done.

*
Starting parallel parsing in block...Parallel parse in 9.06395 (s)
Writing phrases... 
Phrases written  in 0.030959 (s)
Packing phrases...
Recursive call to next depth...

--- computing reference lenght based on memory limit:
--- With 30000MB of  memory, the reference length will be: 1179648000 symbols

--- reference_len is longer than the text are to be parsed: adjusting it.RLZ parser intitialized with:
Ignore     : 273181
Ref        : 1631103
Partitions : 32
Memory(MB) : 30000

Starting recursion depth 1
RLZ Parsing...
Building SA...
We will read blocks of size: 1904284
Reading block 0/0
Block read in 0.00434 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
ReParsing...
Safe bound for bits per factor: 14
IM Random Access to sums file will use at most (MB): 3
 Random Access to Sum Files IM (delta compressed)./RESULTS_SMALL_REF/Influenza/RLZ_PLUS_ref30961710_files/compressed_file.TMP.rlztmp.sums
Input succesfully compressed, output written in: ./RESULTS_SMALL_REF/Influenza/RLZ_PLUS_ref30961710_files/compressed_file
Parsing performed in 2 passes

0-th level report: 

Factors skipped      : 0
Factors 77'ed in ref : 273181
Factors RLZ          : 1631103
Factors Total        : 1904284
--------------------------------------------
Time SA          : 2.07495
Time LZ77 of ref : 0.570198
Time RLZ parse   : 9.13285
-------------------------------
Time RLZ total   : 11.778
--------------------------------------
Rlz TOTAL time 	: 11.7891
Pack      time 	: 0.069134
Recursion time 	: 0.802083
ReParse   time 	: 0.138384
Encode    time 	: 0.049379
--------------------------------------
TOTAL     time 	: 12.8481

1-th level report: 

Factors skipped      : 273181
Factors 77'ed in ref : 671699
Factors RLZ          : 0
Factors Total        : 944880
--------------------------------------------
Time SA          : 0.72842
Time LZ77 of ref : 0.064428
Time RLZ parse   : 1.00001e-06
-------------------------------
Time RLZ total   : 0.792849


Final Wtime(s)    : 12.8481
N Phrases         : 944880
Compression ratio : 0.0976566
seconds per MiB   : 0.0870252
seconds per GiB   : 89.1139
MiB per second    : 11.4909
GiB per second    : 0.0112216
	Command being timed: "./ext/RLZPP/rlz_plusplus /home/work/dvalenzu/data//Influenza -o ./RESULTS_SMALL_REF/Influenza/RLZ_PLUS_ref30961710_files/compressed_file -s -l 30961710 -t16 -c32 -m30000 -d1 -v4"
	User time (seconds): 38.86
	System time (seconds): 0.27
	Percent of CPU this job got: 304%
	Elapsed (wall clock) time (h:mm:ss or m:ss): 0:12.85
	Average shared text size (kbytes): 0
	Average unshared data size (kbytes): 0
	Average stack size (kbytes): 0
	Average total size (kbytes): 0
	Maximum resident set size (kbytes): 352796
	Average resident set size (kbytes): 0
	Major (requiring I/O) page faults: 0
	Minor (reclaiming a frame) page faults: 145279
	Voluntary context switches: 197
	Involuntary context switches: 4128
	Swaps: 0
	File system inputs: 0
	File system outputs: 127744
	Socket messages sent: 0
	Socket messages received: 0
	Signals delivered: 0
	Page size (bytes): 4096
	Exit status: 0
