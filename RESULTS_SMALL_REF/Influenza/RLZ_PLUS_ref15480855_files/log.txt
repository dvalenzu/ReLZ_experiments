RLZ parser intitialized with:
Ignore     : 0
Ref        : 15480855
Partitions : 32
Memory(MB) : 30000
RLZ parsing...
Building SA...
We will read blocks of size: 154808555
Reading block 0/1
Block read in 0.005421 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
Reading block 1/1
Block read in 0.046295 (s)
Waiting for all tasks to be done...
Done.

*
Starting parallel parsing in block...Parallel parse in 9.24491 (s)
Writing phrases... 
Phrases written  in 0.041661 (s)
Packing phrases...
Recursive call to next depth...

--- computing reference lenght based on memory limit:
--- With 30000MB of  memory, the reference length will be: 1179648000 symbols

--- reference_len is longer than the text are to be parsed: adjusting it.RLZ parser intitialized with:
Ignore     : 185655
Ref        : 2225800
Partitions : 32
Memory(MB) : 30000

Starting recursion depth 1
RLZ Parsing...
Building SA...
We will read blocks of size: 2411455
Reading block 0/0
Block read in 0.006031 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
ReParsing...
Safe bound for bits per factor: 14
IM Random Access to sums file will use at most (MB): 4
 Random Access to Sum Files IM (delta compressed)./RESULTS_SMALL_REF/Influenza/RLZ_PLUS_ref15480855_files/compressed_file.TMP.rlztmp.sums
Input succesfully compressed, output written in: ./RESULTS_SMALL_REF/Influenza/RLZ_PLUS_ref15480855_files/compressed_file
Parsing performed in 2 passes

0-th level report: 

Factors skipped      : 0
Factors 77'ed in ref : 185655
Factors RLZ          : 2225800
Factors Total        : 2411455
--------------------------------------------
Time SA          : 0.908048
Time LZ77 of ref : 0.272661
Time RLZ parse   : 9.33397
-------------------------------
Time RLZ total   : 10.5147
--------------------------------------
Rlz TOTAL time 	: 10.5238
Pack      time 	: 0.076383
Recursion time 	: 1.20129
ReParse   time 	: 0.174414
Encode    time 	: 0.052948
--------------------------------------
TOTAL     time 	: 12.0289

1-th level report: 

Factors skipped      : 185655
Factors 77'ed in ref : 832253
Factors RLZ          : 0
Factors Total        : 1017908
--------------------------------------------
Time SA          : 1.09986
Time LZ77 of ref : 0.095398
Time RLZ parse   : 1.00001e-06
-------------------------------
Time RLZ total   : 1.19526


Final Wtime(s)    : 12.0289
N Phrases         : 1017908
Compression ratio : 0.105204
seconds per MiB   : 0.0814761
seconds per GiB   : 83.4315
MiB per second    : 12.2735
GiB per second    : 0.0119859
	Command being timed: "./ext/RLZPP/rlz_plusplus /home/work/dvalenzu/data//Influenza -o ./RESULTS_SMALL_REF/Influenza/RLZ_PLUS_ref15480855_files/compressed_file -s -l 15480855 -t16 -c32 -m30000 -d1 -v4"
	User time (seconds): 38.86
	System time (seconds): 0.29
	Percent of CPU this job got: 325%
	Elapsed (wall clock) time (h:mm:ss or m:ss): 0:12.03
	Average shared text size (kbytes): 0
	Average unshared data size (kbytes): 0
	Average stack size (kbytes): 0
	Average total size (kbytes): 0
	Maximum resident set size (kbytes): 317040
	Average resident set size (kbytes): 0
	Major (requiring I/O) page faults: 0
	Minor (reclaiming a frame) page faults: 130850
	Voluntary context switches: 393
	Involuntary context switches: 4346
	Swaps: 0
	File system inputs: 0
	File system outputs: 151672
	Socket messages sent: 0
	Socket messages received: 0
	Signals delivered: 0
	Page size (bytes): 4096
	Exit status: 0
