RLZ parser intitialized with:
Ignore     : 0
Ref        : 139327695
Partitions : 32
Memory(MB) : 30000
RLZ parsing...
Building SA...
We will read blocks of size: 154808555
Reading block 0/1
Block read in 0.066035 (s)
Computing SA(ref)...
Computing LZ77(ref)...
Starting RLZ...
Reading block 1/1
Block read in 0.005025 (s)
Waiting for all tasks to be done...
Done.

*
Starting parallel parsing in block...Parallel parse in 0.700728 (s)
Writing phrases... 
Phrases written  in 0.001571 (s)
Input succesfully compressed, output written in: ./RESULTS_SMALL_REF/Influenza/RLZ_PRE_ref139327695_files/compressed_file
Parsing performed in 1 passes

0-th level report: 

Factors skipped      : 0
Factors 77'ed in ref : 709819
Factors RLZ          : 75698
Factors Total        : 785517
--------------------------------------------
Time SA          : 11.2331
Time LZ77 of ref : 2.66653
Time RLZ parse   : 0.707395
-------------------------------
Time RLZ total   : 14.607


Final Wtime(s)    : 14.6742
N Phrases         : 785517
Compression ratio : 0.0811859
seconds per MiB   : 0.0993937
seconds per GiB   : 101.779
MiB per second    : 10.061
GiB per second    : 0.0098252
	Command being timed: "./ext/RLZPP/rlz_plusplus /home/work/dvalenzu/data//Influenza -o ./RESULTS_SMALL_REF/Influenza/RLZ_PRE_ref139327695_files/compressed_file -s -l 139327695 -t16 -c32 -m30000 -d0 -v4"
	User time (seconds): 16.41
	System time (seconds): 0.32
	Percent of CPU this job got: 113%
	Elapsed (wall clock) time (h:mm:ss or m:ss): 0:14.68
	Average shared text size (kbytes): 0
	Average unshared data size (kbytes): 0
	Average stack size (kbytes): 0
	Average total size (kbytes): 0
	Maximum resident set size (kbytes): 1232392
	Average resident set size (kbytes): 0
	Major (requiring I/O) page faults: 0
	Minor (reclaiming a frame) page faults: 317157
	Voluntary context switches: 104
	Involuntary context switches: 362
	Swaps: 0
	File system inputs: 0
	File system outputs: 32496
	Socket messages sent: 0
	Socket messages received: 0
	Signals delivered: 0
	Page size (bytes): 4096
	Exit status: 0
