import os
import sys
import ntpath
from shutil import copyfile
from subprocess import call
from tools import *

def experiment_scalability(filename, working_folder, just_replot):
    normal_mem = 4000  ## 10GB
    time_budget = 3600*24*450  ## seconds
    
    file_len = os.path.getsize(filename)
    bitlen =  bitcount(file_len)
    #closest power of two.
    initial_len = 1 << (bitlen-1)
    
    #in increasing order...
    prefix_values = [initial_len/32, initial_len/16, initial_len/8, initial_len/4, initial_len/2, initial_len]
    # prune: anything smaller than 2^X may be too easy for Y MB of RAM
    
    total_len = sum(prefix_values)


    emlz_timedout = False;
    lzend_timedout = False;
    rlbwt_timedout = False;
    for prefix_len in prefix_values:
        n_methods = 3+6+3+3
        time_share = (prefix_len*time_budget)/(total_len*n_methods)
        if (just_replot):
            break
        print "experiment on " + str(prefix_len)
        #print len_to_human(prefix_len)
        current_file = ensure_prefix_exists(filename, prefix_len)
        current_folder = working_folder + "/"+str(prefix_len)
        ensure_dir(current_folder)
        runReLZ(current_file, current_folder, normal_mem,1,0, time_share)
        runReLZ(current_file, current_folder, normal_mem,1,99, time_share)
        runReLZ(current_file, current_folder, normal_mem,4,99, time_share)
        if not emlz_timedout:
            emlz_timedout = runEMLZscan(current_file, current_folder, normal_mem, 60*time_share)
        if not lzend_timedout:
            lzend_timedout = runLZend(current_file, current_folder, 3*time_share)
        if not rlbwt_timedout:
            rlbwt_timedout = runORLBWT_LZ77(current_file, current_folder, 3*time_share)
    
    for prefix_len in prefix_values:
        current_folder = working_folder + "/"+str(prefix_len)
        ensurePhrasesFile(current_folder)
    
    scale_summary_ReLZ(working_folder, prefix_values, normal_mem, 1, 0)
    scale_summary_ReLZ(working_folder, prefix_values, normal_mem, 1, 99)
    scale_summary_ReLZ(working_folder, prefix_values, normal_mem, 4, 99)
    scale_summary_EMLZ(working_folder, prefix_values) 
    scale_summary_LZend(working_folder, prefix_values) 
    scale_summary_ORLBWT_LZ77(working_folder, prefix_values) 
    doScalePlot(filename, working_folder,normal_mem)

def ensurePhrasesFile(folder):
    target_file = folder + "/nphrases.txt"
    emlz_file = folder + "/EMLZscan_files/nphrases.txt"
    if os.path.isfile(target_file) :
        real_z = open(target_file).readline().rstrip()
        if real_z != "1":
            return
    if os.path.isfile(emlz_file):
        print "Bringing it from EMLZ.."
        copyfile(emlz_file, target_file)
        return
    else :
        print "*************"
        print "SUPER WARNING: no file with LZ77 n phrases found, creating a phony one with z=1, but you need to put it manually and re-plot."
        print "the phrases plot will not be normalized"
        print "*************"
        out = open(target_file, 'w+')
        out.write("1")
        out.close
        return
    return


#SUMMARIES:
def scale_summary_ORLBWT_LZ77(working_folder, prefix_values):
    time_file_out = working_folder + "/time_ORLBWT_LZ77.txt"
    out = open(time_file_out, 'w+')
    for prefix_len in prefix_values:
        log_folder = working_folder + "/"+str(prefix_len) +"/ORLBWT_LZ77_files"
        log_file = log_folder + "/log.txt"
        if not is_valid_log_file(log_file):
            break
        # TODO: similar, allow following function to fail ?
        time = getCompressionTimeFromLog(log_file)  
        time_per_symbol = float(time)/float(prefix_len)
        line = str(log2(prefix_len)) + " " + str(prefix_len) + " " + str(time_per_symbol) +"\n" 
        out.write(line)
    out.close()
    return

def scale_summary_LZend(working_folder, prefix_values):
    file_out_time = working_folder + "/time_LZend.txt"
    out_time = open(file_out_time, 'w+')
    file_out_nphrases = working_folder + "/nphrases_LZend.txt"
    out_nphrases = open(file_out_nphrases, 'w+')
    for prefix_len in prefix_values:
        real_z_file = working_folder + "/"+str(prefix_len) + "/nphrases.txt"
        real_z = open(real_z_file).readline().rstrip()
        
        log_folder = working_folder + "/"+str(prefix_len) + "/LZend_files"
        log_file = log_folder + "/log.txt"
        if not is_valid_log_file(log_file):
            break
        
        time = getCompressionTimeFromLog(log_file)
        time_per_symbol = float(time)/float(prefix_len)
        line_time = str(log2(prefix_len)) + " " + str(prefix_len) + " " + str(time_per_symbol) +"\n" 
        out_time.write(line_time)
        
        nphrases = getNPHRASESfromLog(log_file)
        ratio = float(nphrases)/float(real_z)
        line_nphrases = str(log2(prefix_len)) + " " + str(prefix_len) + " " + str(ratio) +"\n" 
        out_nphrases.write(line_nphrases)
    out_time.close()
    return

def scale_summary_ReLZ(working_folder, prefix_values, mem_limit, threads, maxD):
    time_file_out = working_folder + "/time_ReLZ_MEM" + str(mem_limit) + "_THREADS_" + str(threads) + "_D_" + str(maxD) + ".txt"
    nphrases_file_out = working_folder + "/nphrases_ReLZ_MEM" + str(mem_limit) + "_THREADS_" + str(threads) + "_D_" + str(maxD) + ".txt"
    out_time = open(time_file_out, 'w+')
    out_nphrases = open(nphrases_file_out, 'w+')
    for prefix_len in prefix_values:
        real_z_file = working_folder + "/"+str(prefix_len) + "/nphrases.txt"
        real_z = open(real_z_file).readline().rstrip()
        
        log_folder = working_folder + "/"+str(prefix_len) + "/ReLZ_MEM" + str(mem_limit) + "_THREADS_" + str(threads) + "_D_" + str(maxD) + "_files"
        log_file = log_folder + "/log.txt"
        if not is_valid_log_file(log_file):
            break
        
        time = getCompressionTimeFromLog(log_file)
        time_per_symbol = float(time)/float(prefix_len)
        line_time = str(log2(prefix_len)) + " " + str(prefix_len) + " " + str(time_per_symbol) +"\n" 
        out_time.write(line_time)
        
        nphrases = getNPHRASESfromLog(log_file)
        ratio = float(nphrases)/float(real_z)
        line_nphrases = str(log2(prefix_len)) + " " + str(prefix_len) + " " + str(ratio) +"\n" 
        out_nphrases.write(line_nphrases)
    out_time.close()
    return

def scale_summary_EMLZ(working_folder, prefix_values):
    out = open(working_folder+'/time_EMLZ.txt', 'w+')
    for prefix_len in prefix_values:
        log_folder = working_folder + "/"+str(prefix_len) +"/EMLZscan_files"
        log_file = log_folder + "/log.txt"
        if not is_valid_log_file(log_file):
            break
        
        time_file = log_folder + "/time.txt"
        time = open(time_file).readline().rstrip()
        time_per_symbol = float(time)/float(prefix_len)
        line = str(log2(prefix_len)) + " " + str(prefix_len) + " " + str(time_per_symbol) +"\n" 
        out.write(line)
    out.close()
    return



def doScalePlot(input_filename, working_folder, normal_mem):
    #emlz_logfile = working_folder + "/EMLZscan_files/log.txt"
    #z = getNPHRASESfromLog(emlz_logfile)
    #max_y = 10*int(z);
    basename = os.path.basename(input_filename)
    for plotfile in os.listdir("./plotters/scale_plotters"):
        if plotfile.endswith(".p"):
            #print "Z is" + str(z)
            #print "max_y is " + str(max_y)
            sed_operation = " sed -i s/COLNAME/" + basename + "/ " + plotfile + " ;"
            sed_operation += " sed -i s/XNORMALMEMX/" + str(normal_mem) + "/ " + plotfile + " ;"

            #PLACEHOLDER
            command_plot = "cp ./plotters/scale_plotters/" + plotfile+ " "+ working_folder + ";";
            command_plot +="cd "+working_folder+"; " + sed_operation + " gnuplot "+ plotfile+ "; cd ..;"
            #print "calling: "+ command_plot 
            call([command_plot], shell=True)
        

